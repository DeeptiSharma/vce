program NSDesktop;



uses
  Forms,
  Windows,
  Messages,
  Dialogs,
  U_Main in 'U_Main.pas' {NaivesharkAgentMain_Form},
  U_Const in 'U_Const.pas',
  U_RSSReader in 'U_RSSReader.pas',
  U_URLs in 'U_URLs.pas',
  U_Settings in 'U_Settings.pas',
  U_Msg in 'U_Msg.pas',
  U_TrayIcon in 'U_TrayIcon.pas',
  U_Ver in 'U_Ver.pas',
  U_About in 'U_About.pas' {AboutBox},
  U_Online_Mode in 'U_Online_Mode.pas',
  U_Ini in 'U_Ini.pas',
  U_ShellOp in 'U_ShellOp.pas',
  U_CheckUpdate in 'U_CheckUpdate.pas',
  superobject in 'libs\superobject.pas',
  U_API_Wrapper in 'U_API_Wrapper.pas' {API_Wrapper_DataModule: TDataModule},
  U_Login_Frame in 'U_Login_Frame.pas' {Login_Frame: TFrame},
  U_Net_Const in 'U_Net_Const.pas';

{$R *.res}

var hPrevWindow : HWND;
    form_name : string;

    hMutex : integer;
begin
  (* �������� - �� ��� �� ��������� ��������� ������� �����? *)

  hMutex:=CreateMutex( nil, TRUE, 'NaivesharkAgent54781023h4e9x756Mutex' );
  (* ������ �������� - ������ ��� ������ *)
  if GetLastError <> 0
    then
      begin
        (* ������� ����� ���� ����� ����������� ����������. �������� ������ ����� ������ ��������� *)
        form_name := TNaivesharkAgentMain_Form.ClassName;

        (* ���� �� ������ ����. ������ �������� - caption - ����������, ������ �� ���� ����� ��� *)
        hPrevWindow := FindWindow( pchar( form_name ), nil );
        if hPrevWindow <> 0
          then
            begin
              PostMessage( hPrevWindow, WM_BACT2LIFENOTIFY, 0, 0 );
              ShowWindow( hPrevWindow, SW_SHOWNORMAL );
              ShowWindow( hPrevWindow, SW_RESTORE );
              SetForegroundWindow( hPrevWindow );
            end;
        (* ���� ��������� ��� �������, �� �������� �� ����� ���� ������ �� ����,
           ������ ������� ������� ���������. ��������, ���������� ��������� ��� ��
           �����������������, ��� ��� ����� ��������� (������ ������ ��������) *)
        Exit;
      end
    else
      begin
        Get_Version_From_Exe;

        (* ������� ���� �������� *)
        Ini_Init;
       
        Application.Initialize;
        Application.Title := program_title;
        Application.CreateForm(TNaivesharkAgentMain_Form, NaivesharkAgentMain_Form);
        Application.Run;

        (* ������� ���� �������� *)
        Ini_Done;

        ReleaseMutex( hMutex );
      end;
end.
