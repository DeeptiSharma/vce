About Version Control for engineers
===================================

Version Control for engineers (SVN edition) is a freeware Windows collaboration and revision control tool for engineers, scientist and designers.

Version Control for engineers (http://soft.postpdm.com) is graphical user interface for SVN (Apache Subversion, http://subversion.apache.org/ ) version control system. It give you the file revision control for your hardware, engineering and scientific projects, CAD/CAM files and technical visuals.

Program supports the file, http and https connection to SVN repositories, checkout, view modifications, update and commit operations, view log and conflict resolving.

REQUIREMENTS
============

1. Microsoft Windows 95 or later.
2. No local administrator rights are required!!!

CONTACT INFORMATION
===================
Web-site:     http://soft.postpdm.com

INSTALLATION
============

1. Download installer file and execute it. 
2. Walk through with installer master.

DEINSTALLATION
==============

1. Select and run Windows Start|VCE|uninstall

PORTABLE MODE
=============

1. Install the VCE at any place on your computer.
2. Move the whole VCE folder to your target path (to USB-drive, for example).
3. Place the empty vcs_cfg.ini file near the vcs_client.exe executable.
4. Run the vcs_client.exe from your USB-drive, go to Options dialog.
4.1 Check ON the option "Place all checkouts to executable Data path (useful for portable)".
4.2 Set "Temporary files path" to "Application executable path (if no access then system default will be used)".
5. Enjoy your portable version control!

For ease the portable installation - check for https://sourceforge.net/projects/portable.postpdm.p/ distributive.

HISTORY
=======

26 May 2017 - v0.22

New feature:

* Easy picker for path to local SVN repository, GitHub repository integration.

09 January 2017 - v0.21
Fixed bug:

* Fixed error in EPS (Encapsulated PostScript) files diff view

10 November 2016 - v0.20
New feature:

* Diff view for CDW (KOMPAS Graphic CAD) files (via KOMPAS, you need to have KOMPAS installed).

17 May 2016 - v0.19

New features:

* Configuration for external diff tool for each file type (by extension)
* Set the interface font with any size

15 Apr 2016 - v0.18

New features:

* First version of DWG and DXF visual diff (via MS Visio, you need to have MS Visio installed)
* Read the list of repo available from XML-catalog

Enhancements:

* Many little visual improvements in interface (in commit dialog, lists, logs...)

30 Mar 2016 - v0.17

Enhancements:

* More smooth interface with image toolbars.
* Filter by project path/url in main window.

11 Mar 2016 - v0.16

New features:

* List of known urls in drop-down list in New checkout dialog.
* Exporting of compared picture in Image Diff window.
* Test button for external diff tool now generate the temporary text file pair for running tool.
* Default commit message in Options.
* Support for portable mode.

Fixed bug:
* Checking for missed required comment in commit dialog. 

03 Feb 2016 - v0.15

New features:

* MS Visio (if installed) document comparison for .vsd .vdx and .vdw files in Diff and Resolve commands (with page synchronization)

13 Jan 2016 - v0.14

New features:

* MS Word (if installed) document comparison for .doc and .docx files in Diff and Resolve commands
* New "Copy full path to clipboard" command in file list

30 Dec 2015 - v0.13

Enhancements:
* New option in StartUp dialog - Add new project
* StartUp dialog could be switched back in Options dialog
* Visual diff and comparison support are tested for Photoshop Document PSD images.

Fixed bug:
* In diff operation for big files user can press diff for many times while View diff dialog is preparing - fixed.
* If calling the external diff program, based file is not protected - fixed.
* Can't visual compare the Truevision TGA and Portable PixMap PPM images - fixed.

02 Dec 2015 - v0.12

New features:

* Internal image diff and comparison for SVG (Scalable Vector Graphics) and JPEG2000 (jp2) formats

Enhancement:

* More stable working with URL and checkout paths with special and national characters.

Fixed bug:

* Disabled auto merging for text and XML files, which corrupt the file with merge markers

29 Sept 2015 - v0.11

Enhancement:

* New StartUp master with simple connection to free GitHub demo repository

24 Sept 2015 - v0.10

Enhancement:

* Conflict resolver improved - now preserve the extensions for revision files in case of update conflict.

21 Sept 2015 - v0.9

Fixed bug:

* Fixed CRITICAL bug - can't start svn functions if installed to path with space bars in file names.

New features:

* Image compare (for common image formats) in Diff and Resolve dialogs, highlight changes, diff view with scroll synchronization

16 Sept 2015 - v0.8

Enhancements:

* New installer with uninstaller option.
* Add (add file to control) button renamed to Stage
* Edit project credentials dialog improved

15 Sept 2015 - v0.7

Enhancement:

* New Edit credentials option for project working copy. Now you can set up the user name and password for any existing project.

Fixed bug:

* Fixed bug with server authorisation
* Screen position for Image diff dialog now saves for new session.
* If repo url contain / char, can't calculate destination folder for working copy.

11 Sept 2015 - v0.6

Enhancement:

* Revert operation now send modified file to Windows trash bin before replace it with server version.

10 Sept 2015 - v0.5

New features:

* Icons for indicating of file state (unversioned, missed, modified...) in project window
* View log function now work faster
* Dual overview of common image formats in Diff and Conflict resolver dialogs.

07 Sept 2015 - v0.4

Fixed bug:

* Fixed error with files with national characters in file name.

04 Sept 2015 - v0.3

New features:

* Call the external Diff\Diff3 utility in conflict resolver dialog.

02 Sept 2015 - v0.2

New features:
* Monitor repositories for new commits
* Context menu in file list.

26 Avg 2015 - v0.1

* First release.


LICENSE
=======

All copyrights to Version Control for engineers are exclusively owned by
PostPDM Software - http://soft.postpdm.com/.

Version Control for engineers is distributed as FREEWARE.
It means what you can:
1) Use this software free
2) Freely distribute the software, provided the 
   distribution package is not modified. No person or company 
   may charge a fee for the distribution of Version Control for engineers.

- Version Control for engineers IS DISTRIBUTED "AS IS".
  NO WARRANTY OF ANY KIND IS EXPRESSED OR IMPLIED. 
  YOU USE AT YOUR OWN RISK. THE AUTHOR WILL NOT BE LIABLE 
  FOR DATA LOSS, DAMAGES, LOSS OF PROFITS OR ANY OTHER KIND 
  OF LOSS WHILE USING OR MISISING THIS SOFTWARE.

- You may not use, copy, emulate, clone, rent, lease, sell, modify,
  decompile, disassemble, otherwise reverse engineer, or transfer the
  licensed program, or any subset of the licensed program, except as 
  provided for in this agreement.  Any such unauthorized use shall 
  result in immediate and automatic termination of this license and
  may result in criminal and/or civil prosecution.

  All rights not expressly granted here are reserved by
  Author.

- Installing and using Version Control for engineers signifies acceptance of 
  these terms and conditions of the license.

- If you do not agree with the terms of this license you must remove
  Version Control for engineers files from your storage devices 
  and cease to use the product.
