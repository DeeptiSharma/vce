About Version Control for engineers
===================================

Version Control for engineers (SVN edition) is a freeware Windows collaboration tool for engineers, scientist and designers.

Version Control for engineers is graphical user interface for SVN (Apache Subversion, http://subversion.apache.org/ ) version control system. It give you the file revision control for your hardware, engineering and scientific projects.

Program supports the file, http and https connection to SVN repositories, checkout, view modifications, update and commit operations, view log and conflict resolving.

REQUIREMENTS
============

1. Microsoft Windows 95 or later.
3. No local administrator rights are required!!!

CONTACT INFORMATION
===================
Web-site:     https://soft.postpdm.com

INSTALLATION
============

Download vcs_portable_v0_6 file and execute it. 
Select the destination folder to extract to.
Software will be extracted from archive.
When execute the vcs_client.exe.

HISTORY
=======

11 Sept 2015 - v0.6

Enhancement:

* Revert operation now send modified file to Windows trash bin before replace it with server version.

10 Sept 2015 - v0.5

New features:

* Icons for indicating of file state (unversioned, missed, modified...) in project window
* View log function now work faster
* Dual overview of common image formats in Diff and Conflict resolver dialogs.

07 Sept 2015 - v0.4

Fixed bug:

* Fixed error with files with national characters in file name.

04 Sept 2015 - v0.3

New features:

* Call the external Diff\Diff3 utility in conflict resolver dialog.

02 Sept 2015 - v0.2

New features:
* Monitor repositories for new commits
* Context menu in file list.

26 Avg 2015 - v0.1

* First release.


LICENSE
=======

All copyrights to Version Control for engineers are exclusively owned by
PostPDM Software - http://soft.postpdm.com/.

Version Control for engineers is distributed as FREEWARE.
It means what you can:
1) Use this software free
2) Freely distribute the software, provided the 
   distribution package is not modified. No person or company 
   may charge a fee for the distribution of Version Control for engineers.

- Version Control for engineers IS DISTRIBUTED "AS IS".
  NO WARRANTY OF ANY KIND IS EXPRESSED OR IMPLIED. 
  YOU USE AT YOUR OWN RISK. THE AUTHOR WILL NOT BE LIABLE 
  FOR DATA LOSS, DAMAGES, LOSS OF PROFITS OR ANY OTHER KIND 
  OF LOSS WHILE USING OR MISISING THIS SOFTWARE.

- You may not use, copy, emulate, clone, rent, lease, sell, modify,
  decompile, disassemble, otherwise reverse engineer, or transfer the
  licensed program, or any subset of the licensed program, except as 
  provided for in this agreement.  Any such unauthorized use shall 
  result in immediate and automatic termination of this license and
  may result in criminal and/or civil prosecution.

  All rights not expressly granted here are reserved by
  Author.

- Installing and using Version Control for engineers signifies acceptance of 
  these terms and conditions of the license.

- If you do not agree with the terms of this license you must remove
  Version Control for engineers files from your storage devices 
  and cease to use the product.
