;NSIS Modern User Interface
;Start Menu Folder Selection Example Script
;Written by Joost Verburg


; http://nsis.sourceforge.net/Docs/Chapter5.html#error
; надо проверять что exe тестовый и отстанавливать

SetCompressor /SOLID lzma

!define SCRIPT_ROOT "d:\delphi\Projects\DesktopClient\"
!define PROGNAME "Version Control for engineers"
!define SHORT_NAME "VCE"

!define VER_MAJOR 0
!define VER_MINOR 22

Caption "${PROGNAME} ver. ${VER_MAJOR}.${VER_MINOR} installer" 

!macro CheckMutex

System::Call 'kernel32::OpenMutex(i 0x100000, b 0, t "VCSClient7c4wqf8nwa9m2Mutex") i .R0'
IntCmp $R0 0 notRunning
    System::Call 'kernel32::CloseHandle(i $R0)'
    MessageBox MB_OK|MB_ICONEXCLAMATION "${PROGNAME} is running. Please close it first!" /SD IDOK
    Abort
notRunning:

!macroend

Function .onInit

!insertmacro CheckMutex

FunctionEnd

Function un.onInit

!insertmacro CheckMutex

FunctionEnd

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;General

  ;Name and file
  Name ${SHORT_NAME}
  OutFile "vce_setup_${VER_MAJOR}.${VER_MINOR}.exe"

  ;Default installation folder
  InstallDir "$LOCALAPPDATA\${PROGNAME}"
  
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "SOFTWARE\${PROGNAME}" ""

  ;Request application privileges for Windows Vista
  RequestExecutionLevel user
  
;--------------------------------
;Variables

  Var StartMenuFolder

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "${SCRIPT_ROOT}inst\vcs\readme.txt"
  ;!insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\${PROGNAME}" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
  
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "Dummy Section" SecDummy

  SetOutPath "$INSTDIR"
  
  ;ADD YOUR OWN FILES HERE...
  File "${SCRIPT_ROOT}\bin\vcs_client.exe"
  File "${SCRIPT_ROOT}\Inst\vcs\readme.txt"
  File "${SCRIPT_ROOT}\Inst\vcs\PostPDM WebSite.url"  
  
  SetOutPath "$INSTDIR\svn"
  File "${SCRIPT_ROOT}\bin\svn\"
  
  SetOutPath "$INSTDIR\imagemagick"
  File "${SCRIPT_ROOT}\bin\imagemagick\"
    
  SetOutPath "$INSTDIR\vcs_res"
  File "${SCRIPT_ROOT}\bin\vcs_res\"
  SetOutPath "$INSTDIR\vcs_res\actions"
  File "${SCRIPT_ROOT}\bin\vcs_res\actions\"

  ;Store installation folder
  WriteRegStr HKCU "Software\${PROGNAME}" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  Exec "$INSTDIR\vcs_client.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application

  ;create desktop shortcut
  CreateShortCut "$DESKTOP\${PROGNAME}.lnk" "$INSTDIR\vcs_client.exe" 
  
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\${PROGNAME}.lnk" "$INSTDIR\vcs_client.exe"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\readme.lnk" "$INSTDIR\readme.txt"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\PostPDM WebSite.lnk" "$INSTDIR\PostPDM WebSite.url"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_END
  ; добавить в автозапуск
  ;CreateShortCut "$SMSTARTUP\${PROGNAME}.lnk" "$INSTDIR\vcs_client.exe"
    
SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_SecDummy ${LANG_ENGLISH} "A test section."

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecDummy} $(DESC_SecDummy)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
 
;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...
  
  Delete "$INSTDIR\vcs_client.exe"
  Delete "$INSTDIR\readme.txt"
  Delete "$INSTDIR\PostPDM WebSite.url"
  
  Delete "$INSTDIR\svn\*.*"
  Delete "$INSTDIR\imagemagick\*.*"
  
  Delete "$INSTDIR\vcs_res\actions\*.*"
  Delete "$INSTDIR\vcs_res\*.*"
  RMDir "$INSTDIR\svn"
  RMDir "$INSTDIR\imagemagick"
  RMDir "$INSTDIR\vcs_res\actions"
  RMDir "$INSTDIR\vcs_res"
  
  Delete "$INSTDIR\Uninstall.exe"

  RMDir "$INSTDIR"

  ; удалить cfg
  ;Delete "$APPDATA\Naiveshark\vcs_cfg.ini.ini"
  ;RMDir "$APPDATA\Naiveshark"
  
  ; удалить из автозапуска
  ;Delete "$SMSTARTUP\${PROGNAME}.lnk"
  
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder    
    
  Delete "$DESKTOP\${PROGNAME}.lnk"
  
  ;Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\*.*"
  RMDir "$SMPROGRAMS\$StartMenuFolder"
  
  DeleteRegKey /ifempty HKCU "Software\${PROGNAME}"

SectionEnd