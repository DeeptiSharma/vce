;NSIS Modern User Interface
;Start Menu Folder Selection Example Script
;Written by Joost Verburg

SetCompressor /SOLID lzma

!define SCRIPT_ROOT "d:\p\delphi\Projects\Desktop\"
!define PROGNAME "Naiveshark agent"

!define VER_MAJOR 0
!define VER_MINOR 5

Caption "${PROGNAME} ver. ${VER_MAJOR}.${VER_MINOR} installer" 

!macro CheckMutex

System::Call 'kernel32::OpenMutex(i 0x100000, b 0, t "NaivesharkAgent54781023h4e9x756Mutex") i .R0'
IntCmp $R0 0 notRunning
    System::Call 'kernel32::CloseHandle(i $R0)'
    MessageBox MB_OK|MB_ICONEXCLAMATION "${PROGNAME} is running. Please close it first!" /SD IDOK
    Abort
notRunning:

!macroend

Function .onInit

!insertmacro CheckMutex

FunctionEnd

Function un.onInit

!insertmacro CheckMutex

FunctionEnd

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;General

  ;Name and file
  Name ${PROGNAME}
  OutFile "Out\nsasetup_${VER_MAJOR}.${VER_MINOR}.exe"

  ;Default installation folder
  InstallDir "$LOCALAPPDATA\${PROGNAME}"
  
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "SOFTWARE\${PROGNAME}" ""

  ;Request application privileges for Windows Vista
  RequestExecutionLevel user
  
;--------------------------------
;Variables

  Var StartMenuFolder

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "${SCRIPT_ROOT}\inst\readme.txt"
  ;!insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\${PROGNAME}" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
  
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "Dummy Section" SecDummy

  SetOutPath "$INSTDIR"
  
  ;ADD YOUR OWN FILES HERE...
  File "${SCRIPT_ROOT}\bin\NSDesktop.exe"
  File "${SCRIPT_ROOT}\Inst\readme.txt"
  File "${SCRIPT_ROOT}\Inst\Naiveshark WebSite.url"  
  
  ;Store installation folder
  WriteRegStr HKCU "Software\${PROGNAME}" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  Exec "$INSTDIR\NSDesktop.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\${PROGNAME}.lnk" "$INSTDIR\NSDesktop.exe"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\readme.lnk" "$INSTDIR\readme.txt"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Naiveshark WebSite.lnk" "$INSTDIR\Naiveshark WebSite.url"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_END
  ; добавить в автозапуск
  CreateShortCut "$SMSTARTUP\${PROGNAME}.lnk" "$INSTDIR\NSDesktop.exe"
    
SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_SecDummy ${LANG_ENGLISH} "A test section."

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecDummy} $(DESC_SecDummy)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
 
;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...
  
  Delete "$INSTDIR\NSDesktop.exe"
  Delete "$INSTDIR\readme.txt"
  Delete "$INSTDIR\Naiveshark WebSite.url"
  Delete "$INSTDIR\Uninstall.exe"

  RMDir "$INSTDIR"

  ; удалить cfg
  Delete "$APPDATA\Naiveshark\cfg.ini"
  RMDir "$APPDATA\Naiveshark"
  
  ; удалить из автозапуска
  Delete "$SMSTARTUP\${PROGNAME}.lnk"
  
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
    
  ;Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\*.*"
  RMDir "$SMPROGRAMS\$StartMenuFolder"
  
  DeleteRegKey /ifempty HKCU "Software\${PROGNAME}"

SectionEnd