CAD Diff tool

Visual comparison tool for difference in image and CAD files.

Version 1.4

Usage:

cad_diff.exe OldFile NewFile

or

cad_diff.exe

Support for 
* common image formats (BMP, GIF, JPG, PNG, PSD, SVG etc.),
* MS Visio drawings (need MS Visio to be installed)
* KOMPAS Graphic CDW drawings (need KOMPAS to be installed)
               
                
PostPDM, copyright 2016.
http://soft.postpdm.com/


History

Version 1.4
06 Dec 2016

+ Support for KOMPAS Graphic CDW drawings format (need KOMPAS to be installed)


Version 1.3
24 May 2016

+ Trim operation for image edges erasing.

Version 1.2
18 May 2016

+ Dual file select dialog (if no command line provided)

Version 1.1
25 Apr 2016

+ Support for related paths to compared files.

Version 1.0
19 Apr 2016

First release.