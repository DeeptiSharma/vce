unit U_CheckUpdate;

interface

uses Classes;
(* ���������� �������� *)
type t_cu_results = ( cur_Fail,         // ���� ��������
                      cur_NewDetected,  // ������� ����������
                      cur_UpToDate      // �� ������
                    );

(* ��������� ������� ���������� �� ��. �����. True ���� �� ����� ����� *)
function CheckUpdate( Owner : TComponent; out Out_New_Version_Cap : string ) : t_cu_results;

implementation

uses U_Ver, U_Msg, SysUtils,
     XMLDoc, XMLIntf, U_URLs;

function CheckUpdate;
var Version_XML: TXMLDocument;

    mainNode: IXMLNode;
    nodes: IXMLNodeList;

    validate_xml : string;
    xml_minor, xml_major : integer;

{$include directives.inc}
const
      {$ifdef Naiveshark_agent}
      xml_sign = 'u56h89p098';
      {$endif}
      {$ifdef VCS_Client}
      xml_sign = 'i94h79p013';
      {$endif}

begin
  Result := cur_Fail;

  Version_XML := TXMLDocument.Create( Owner );

  Version_XML.FileName := Get_Version_Xml;

  try
    Version_XML.Active := True;

    mainNode := Version_XML.DocumentElement;
    nodes := mainNode.ChildNodes;
    //ShowMsg( Version_XML.XML.Text );
    xml_major := StrToInt( nodes['MAJOR'].Text );
    xml_minor := StrToInt( nodes['MINOR'].Text );
    validate_xml := nodes['VALIDATE_XML'].Text;

    if validate_xml = xml_sign (* ��� ���� ���������, �������� � xml �����. ������������ ������ ��� ��������, ��� ������� ���������� ���� *)
      then
        begin
          if ( xml_major > Major_Ver ) or
             ( ( xml_major = Major_Ver ) and ( xml_minor > Minor_Ver ) )
            then
              begin
                Result := cur_NewDetected;
                Out_New_Version_Cap := Format( msg_NewVersion_detected_fmt, [ xml_major, xml_minor ] );
              end
            else
              Result := cur_UpToDate;
        end;
  except
  end;

  Version_XML.Active := False;

  Version_XML.Free;
end;

end.
