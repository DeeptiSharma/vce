unit U_Cad_Diff_File_Select;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFile_Select_F = class(TForm)
    OpenDialog1: TOpenDialog;
    Label1: TLabel;
    Left_E: TEdit;
    Label2: TLabel;
    Right_E: TEdit;
    OK_B: TButton;
    Cancel_B: TButton;
    Button1: TButton;
    Button2: TButton;
    procedure Left_EChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function File_Select( var Arg_FN1, Arg_FN2 : string ) : boolean;

implementation

{$R *.dfm}

function File_Select;
begin
  Result := False;

  with TFile_Select_F.Create( application ) do
    begin
      if Arg_FN1 <> ''
        then
          begin
            Left_E.Text := Arg_FN1;
            if Arg_FN2 <> ''
              then
                Right_E.Text := Arg_FN2;
          end;

      if ShowModal = mrOk
        then
          begin
            Arg_FN1 := Left_E.Text;
            Arg_FN2 := Right_E.Text;
            Result := True;
          end;
    end;
end;

procedure TFile_Select_F.Button1Click(Sender: TObject);
begin
  if Left_E.Text <> ''
    then
      OpenDialog1.FileName := Left_E.Text;

  if OpenDialog1.Execute
    then
      Left_E.Text := OpenDialog1.FileName;
end;

procedure TFile_Select_F.Button2Click(Sender: TObject);
begin
  if Right_E.Text <> ''
    then
      OpenDialog1.FileName := Right_E.Text;

  if OpenDialog1.Execute
    then
      Right_E.Text := OpenDialog1.FileName;
end;

procedure TFile_Select_F.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TFile_Select_F.Left_EChange(Sender: TObject);
begin
  OK_B.Enabled := ( Left_E.Text <> '' ) and ( FileExists( Left_E.Text ) )
                  and
                  ( Right_E.Text <> '' ) and ( FileExists( Right_E.Text ) );
  
end;

end.
