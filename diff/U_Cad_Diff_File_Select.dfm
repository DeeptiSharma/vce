object File_Select_F: TFile_Select_F
  Left = 0
  Top = 0
  Caption = 'CAD Diff - select files'
  ClientHeight = 166
  ClientWidth = 595
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  DesignSize = (
    595
    166)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 99
    Height = 13
    Caption = 'Select left (base) file'
  end
  object Label2: TLabel
    Left = 8
    Top = 64
    Width = 122
    Height = 13
    Caption = 'Select right (modified) file'
  end
  object Left_E: TEdit
    Left = 8
    Top = 35
    Width = 521
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    OnChange = Left_EChange
  end
  object Right_E: TEdit
    Left = 8
    Top = 83
    Width = 521
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    OnChange = Left_EChange
  end
  object OK_B: TButton
    Left = 160
    Top = 120
    Width = 137
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 4
  end
  object Cancel_B: TButton
    Left = 328
    Top = 120
    Width = 121
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 5
  end
  object Button1: TButton
    Left = 543
    Top = 33
    Width = 45
    Height = 25
    Caption = '...'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 543
    Top = 81
    Width = 45
    Height = 25
    Caption = '...'
    TabOrder = 3
    OnClick = Button2Click
  end
  object OpenDialog1: TOpenDialog
    Left = 512
    Top = 8
  end
end
