object About_F: TAbout_F
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'About'
  ClientHeight = 283
  ClientWidth = 378
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 11
    Top = 8
    Width = 339
    Height = 105
    BevelInner = bvRaised
    BevelOuter = bvLowered
    ParentColor = True
    TabOrder = 0
    object ProductName_L: TLabel
      Left = 16
      Top = 21
      Width = 67
      Height = 13
      Caption = 'Product Name'
      IsControl = True
    end
    object Version_L: TLabel
      Left = 16
      Top = 40
      Width = 46
      Height = 13
      Caption = 'Version_L'
      IsControl = True
    end
    object Copyright: TLabel
      Left = 16
      Top = 59
      Width = 155
      Height = 13
      Caption = 'Copyright: PostPDM, 2015-2016'
      IsControl = True
    end
    object www_main_l: TLabel
      Left = 16
      Top = 78
      Width = 60
      Height = 13
      Cursor = crHandPoint
      Caption = 'www_main_l'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = www_main_lClick
    end
  end
  object OK_B: TButton
    Left = 144
    Top = 251
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 1
    OnClick = OK_BClick
  end
  object Info_M: TMemo
    Left = 11
    Top = 119
    Width = 339
    Height = 126
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 2
  end
end
