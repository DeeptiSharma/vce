(* ������� ������ VCS - version control system. SVN *)
unit u_file_manager_VCS;

interface

uses Classes,
     u_file_manager_VCS_Base, u_file_manager_VCS_const;

type T_VCS_SVN_Wrapper = class( T_VCS_Wrapper_Base )
        protected
         function StateName2State( Arg_State_Name : string ) : T_File_VCS_States; override;
         function Scan( Arg_Name : string; var Arg_Files_Array : T_Files_Array; Arg_Folder : boolean ) : boolean; override;
         function Do_Checkout : boolean; override;

         function Do_Run_VCS_Tool( Arg : string; Arg_Auth_Need : boolean; Arg_Work_Folder_Env : T_Work_Folder_Env; out Out_Str : string; Arg_Log : boolean = False ) : boolean; override;
         procedure After_Create; override;
        public

         function Add_File( Arg_File_IDX : integer ) : boolean; override;
         function Commit : boolean; override;

         function Get_Path_Info( Arg_Path : string; Arg_Remote : boolean; out Out_Repo_Info : T_Repo_Info ) : T_Connect_Results; override;
         function Check_Working_Copy_Outdated( out out_Server_Rev, out_Working_Copy_Rev : t_revision_id_type ) : T_Connect_Results; override;

         function Update : T_Update_Results; override;

         function Revert( Arg_File_IDX : integer ) : boolean; override;

         function Resolve( Arg_File_IDX : integer ) : boolean; override;

         function Get_File_FullPath( Arg_File_Idx : integer; Arg_Canonical : boolean = False ): string; override;

         function Get_FileList_BeforeCommit( var Arg_File_List : T_Commited_Files_Array ) : T_Get_FileList_BeforeCommit_results; override;

         (* ��������� ��������� ������ ����� *)
         function Check_File_State( Arg_File : string; out out_conflicted_file_rev : t_revision_id_type ) : T_File_VCS_States; override;

         (* ���� ���� ��� � ������� - �������� ��� ���� ��� �������� diff *)
         function Get_Pristine_File_Path( Arg_File_IDX : integer ) : string; override;

         function Get_VCS_History( out Out_Commits_Log_A : T_Commits_Log_A; Arg_Path : string ) : boolean; override;

         (* ������� ����� �� ������� � ��������� ����� *)
         function Export_File_Revisioned( Arg_Local_Path, Arg_Dest_Path : string; Arg_Rev : t_revision_id_type ) : boolean; override;
      end;

(* ��� �����, ����������� �� ������ � ������� svn *)
function Extact_SVN_FileName( Arg_VCS_Path : string ) : string;

(* �������� ��� ����� �� ����������� svn *)
function SVN_Safe_FileName( Arg_VCS_Path : string ) : string;

function Prepare_SVN_bins : boolean;

type t_check_path_res = ( tcpr_None, tcpr_Ok, tcpr_Drive_Doesnt_Exist, tcpr_Path_Exist, tcpr_Path_Busy, tcpr_Nested, tcpr_ForbiddenChars );

(* ��������� ��������� ����� �� ������� ����, �������� �� ��� ��� �������� ������� �����. *)
function Check_Dest_For_VCS_Working_Copy( Arg_Dest_Path : string ) : t_check_path_res;

(* ��������� - ���� ��� svn ������? *)
function Check_Path_Is_SVN_Checkout( Arg_Path : string ) : boolean;

(* ��������� - ���� ��� ��������� svn repo? *)
function Check_Path_Is_Local_SVN_Repo( Arg_Path : string ) : boolean;

implementation

uses XMLDoc, xmldom, XMLIntf, msxmldom, SysUtils, Variants,
     U_Msg, U_ShellOp, U_Const, vcs_resolver_form, vcs_resolver_const,
    u_vcs_Ini, u_web_utils;

function XML_DT_2_Short( Arg_XML_DT : string ) : string;
var i : integer;
begin
  Result := Arg_XML_DT;

  i := pos( 'T', Result );
  if i > 0
    then
      begin
        Result[ i ] := ' ';

        i := Pos( '.', Result );

        if i > 0
          then
            Result := Copy( Result, 1, i - 1 );
      end;
end;

(* ��������� - ���� ��� svn ������? *)
function Check_Path_Is_SVN_Checkout;
begin
  Result := DirectoryExists( Arg_Path + '\.svn' );
end;

function Check_Path_Is_Local_SVN_Repo;
begin
  (* ��������� ���� ������ ��������� ���� ���� � ��� ����� *)
  Result := ( FileExists( Arg_Path + '\format' ) )
              and
            ( DirectoryExists( Arg_Path + '\conf' ) )
              and
            ( DirectoryExists( Arg_Path + '\db' ) );
end;

function Check_Dest_For_VCS_Working_Copy;
var i : integer;
    s : string; 
begin
  (* ���������, ����� �� ������ ���-��� *)
  (* ���� ���� ����� '\.svn', �� �������� ��� ������ *)
  Result := tcpr_None;

  (* ���� ������ ������������ *)
  if Drive_Exist( Arg_Dest_Path ) 
    then
      begin
        (* ����� ������ ���� ����� *)
        if DirectoryExists( Arg_Dest_Path )
          then
            Result := tcpr_Path_Exist
          else
            begin
              (* �������� - ���� �� ������ ��������� ����������� �������� *)
              if Find_Sub_Char( Arg_Dest_Path, MSWindows_Path_Restricted_Chars )
                then
                  begin
                    Result := tcpr_ForbiddenChars;
                    exit;
                  end;
              
              (* �������� - ��������� ���� �� ������ ������� � �����-������ ������� ����� �������� ������ *)
              s := Arg_Dest_Path;
              repeat
                i := LastDelimiter( '\', s );
                if i > 0
                  then
                    begin
                      s := Copy( s, 1, i - 1 );
                      if Check_Path_Is_SVN_Checkout( s )
                        then
                          Result := tcpr_Nested;
                    end;
              until ( Result = tcpr_Nested ) or ( i = 0 );
              if Result <> tcpr_Nested
                then
                  Result := tcpr_Ok;
            end;
      end
    else
      Result := tcpr_Drive_Doesnt_Exist;
end;

(* SVN ���� ���� � /, ���� �������� �� \ *)
function Replace_Slash_SVN2Dos( Arg_Path : string ) : string;
var i : integer;
begin
  Result := Arg_Path;

  for i := 1 to Length( Result ) do
    if Result[ i ] = '/'
      then
        Result[ i ] := '\';    
end;

function Extact_SVN_FileName;
var
  I: Integer;
  last_slash_pos : integer;
begin
  result := '';

  if Arg_VCS_Path = ''
    then
      Exit;
      
  (* ��� ����� ������������� � �� /
    https://bitbucket.org/yncoder/desktopclient/issues/89 *)
  repeat
    last_slash_pos := Length( Arg_VCS_Path );
    if ( Arg_VCS_Path[ last_slash_pos ] = '/' )
        or
       ( Arg_VCS_Path[ last_slash_pos ] = '\' )
      then
        Delete( Arg_VCS_Path, last_slash_pos, 1 )
      else
        last_slash_pos := -1;

  until last_slash_pos < 0;

  if Arg_VCS_Path <> ''
    then
      begin
        I := LastDelimiter( '/', Arg_VCS_Path );
        Result := Copy(Arg_VCS_Path, I + 1, MaxInt);
      end;
end;

(* �������� ��� ����� �� ����������� svn *)
function SVN_Safe_FileName( Arg_VCS_Path : string ) : string;
begin
  (* https://bitbucket.org/yncoder/desktopclient/issues/71 *)
  Result := Arg_VCS_Path;
  (* ��������, � ����� ����� ���� @. ������ ���� �������� @ *)
  if pos( '@', Result ) > 0
    then
      begin
        if Result[ Length( Result ) ] <> '@'
          then
            Result := Result + '@';
      end;
end;

(* ����� �� ����� ������������ ������� ������� �������� *)
const log_limit = 25;

var SVN_exe, text_merge_script : string;

{ T_VCS_Wrapper }

function T_VCS_SVN_Wrapper.Do_Checkout;
var Out_Str : string;
begin
  Result := True;
  (* ���������, ��� ����� ��� �������� �������� *)
  if not Check_Path_Is_SVN_Checkout( fVCS_Folder )
    then
      begin
        (* ���� ����� ��� - ����������� ������� *)
        if not DirectoryExists( fVCS_Folder )
          then
            begin
              if not ForceDirectories( fVCS_Folder )
                then
                  begin
                    Result := False;
                    Exit;
                  end;
            end;
        (* ���������� �������� ������� *)
        Result := Run_VCS_Tool( 'checkout "' + fVCS_URL + '"' + ' "' + Extact_SVN_FileName( fVCS_URL ) + '"', True, twfe_WorkFolder_Up, Out_Str, False )
      end;
end;

function T_VCS_SVN_Wrapper.Resolve;

function Get_Conflicted_File_Info( Arg_Conflicted_FN : string; var Arg_Your_File, Arg_Base_Revision_File, Arg_New_Revision_File : string;
                                   var Arg_Base_Revision_id, Arg_New_Revision_id : t_revision_id_type ) : boolean;

procedure Get_Rev_From_ConflictVersion( Arg_Version_ItemNode : IXMLNode );
begin
  if Assigned( Arg_Version_ItemNode )
    then
      begin
        if Arg_Version_ItemNode.Attributes['side'] = 'source-left'
          then
            Arg_Base_Revision_id := Arg_Version_ItemNode.Attributes['revision']
          else
            begin
              if Arg_Version_ItemNode.Attributes['side'] = 'source-right'
                then
                  Arg_New_Revision_id := Arg_Version_ItemNode.Attributes['revision']
            end;
      end;
end;

var Out_Str : string;
    fXMLDocument: TXMLDocument;
    Entry_ItemNode, Conflict_ItemNode, Version_ItemNode : IXMLNode;

begin
  Result := False;
  Arg_Your_File := '';
  Arg_Base_Revision_File := '';
  Arg_New_Revision_File := '';
  Arg_Base_Revision_id := revision_unknown;
  Arg_New_Revision_id := revision_unknown;

  fXMLDocument := nil;

  if ( Run_VCS_Tool( 'info "' + SVN_Safe_FileName( Arg_Conflicted_FN ) + '" --xml ', False, twfe_Default, Out_Str ) )
    then
      begin
        (* ������� xml *)
        try
          fXMLDocument := TXMLDocument.Create( Self );
          fXMLDocument.LoadFromXML( Out_Str );

          fXMLDocument.Active := True;
          (* ����� xml *)
          Entry_ItemNode := fXMLDocument.DocumentElement.ChildNodes.FindNode('entry');
          if Assigned( Entry_ItemNode )
            then
              begin
                Conflict_ItemNode := Entry_ItemNode.ChildNodes.FindNode('conflict');
                if Assigned( Conflict_ItemNode )
                  then
                    begin
                      (* ������ ������� *)
                      Version_ItemNode := Conflict_ItemNode.ChildNodes.FindNode( 'version' );
                      Get_Rev_From_ConflictVersion( Version_ItemNode );

                      Version_ItemNode := Version_ItemNode.NextSibling;
                      Get_Rev_From_ConflictVersion( Version_ItemNode );

                      if ( Conflict_ItemNode.ChildNodes.FindNode( 'prev-base-file' ) <> nil )
                        then
                          Arg_Base_Revision_File := Replace_Slash_SVN2Dos( Conflict_ItemNode.ChildNodes.FindNode( 'prev-base-file' ).Text );

                      if ( Conflict_ItemNode.ChildNodes.FindNode( 'prev-wc-file' ) <> nil )
                        then
                          Arg_Your_File := Replace_Slash_SVN2Dos( Conflict_ItemNode.ChildNodes.FindNode( 'prev-wc-file' ).Text );

                      if ( Conflict_ItemNode.ChildNodes.FindNode( 'cur-base-file' ) <> nil )
                        then
                          Arg_New_Revision_File := Replace_Slash_SVN2Dos( Conflict_ItemNode.ChildNodes.FindNode( 'cur-base-file' ).Text );
                    end;
              end;

        finally
          if Assigned( fXMLDocument )
            then
              begin
                if fXMLDocument.Active
                  then
                    fXMLDocument.Active := False;

                fXMLDocument.Free;
              end;
        end;
      end;
end;

var Out_Str : string;
    file_name : string;
    File_VCS_States : T_File_VCS_States;
    
    Conflicted_File_name,
    Conflicted_File, Your_File, Base_Revision_File, New_Revision_File : string;

    conflicted_file_rev : t_revision_id_type;

    Base_Revision_id, New_Revision_id : t_revision_id_type;

begin
  Result := False;
  Base_Revision_id := revision_unknown;
  New_Revision_id := revision_unknown;

  (* �������� ��������� ������, ������� ������ � ������� *)
  if fFolder_Files_Array[ Arg_File_Idx ].f_state in Resolvable_states
    then
      begin
        (* ��������� ������� ������ �����.
           ���� ������ ��������� ��������, ���� ��������� ������ ������ ����� *)
        file_name := Get_File_FullPath( Arg_File_Idx );

        File_VCS_States := Check_File_State( file_name, conflicted_file_rev );

        if File_VCS_States in Resolvable_states
          then
            begin
              (* ����������� ���� *)
              Conflicted_File_name := fFolder_Files_Array[ Arg_File_Idx ].f_file_name;
              Conflicted_File := Get_File_FullPath( Arg_File_Idx );

              Get_Conflicted_File_Info( file_name, Your_File, Base_Revision_File, New_Revision_File, Base_Revision_id, New_Revision_id );

{


            if New_Revision_File <> ''
              then
                New_Revision_File := fCurrent_Folder + Slash + New_Revision_File;
            if Base_Revision_File <> ''
              then
                Base_Revision_File := fCurrent_Folder + Slash + Base_Revision_File;
}
            (* ���������, ������� �� ���������� ����������� ����� *)
            if ( New_Revision_File <> '' ) or ( Base_Revision_File <> '' ) or ( Your_File <> '' )
              then
                begin
                  case Show_Resolver( Self, Conflicted_File, Your_File, Base_Revision_File, New_Revision_File, Base_Revision_id, New_Revision_id  ) of
                    //trd_Cancel, trd_None : ;
                    trd_Your, trd_Conflicted_Merged, trd_Conflicted_Yours :
                      begin
                        Result := Run_VCS_Tool( 'resolve --accept mine-full "' + SVN_Safe_FileName( Conflicted_File ) + '"', False, twfe_Default, Out_Str );
                      end;
                    trd_Their :
                      begin
                        Result := Run_VCS_Tool( 'resolve --accept theirs-full "' + SVN_Safe_FileName( Conflicted_File ) + '"', False, twfe_Default, Out_Str );
                      end;
                   end;
                end
              else
                ShowMsg( cap( cap_Cant_Resolve_Conflict ) );
            end;
      end;
end;

function T_VCS_SVN_Wrapper.Revert;
var Out_Str : string;
    file_name : string;
    File_VCS_States : T_File_VCS_States;
    conflicted_file_rev : t_revision_id_type;
begin
  Result := False;

  (* �������� ��������� ������, ������� ������ � ������� *)
  if fFolder_Files_Array[ Arg_File_Idx ].f_state in Revertable_states
    then
      begin
        (* ��������� ������� ������ �����.
           ���� ������ ��������� ��������, ���� ��������� ������ ������ ����� *)
        file_name := Get_File_FullPath( Arg_File_Idx );

        File_VCS_States := Check_File_State( file_name, conflicted_file_rev );
        if File_VCS_States in Revertable_states
          then
            begin
              Result := Run_VCS_Tool( 'revert "' + SVN_Safe_FileName( Get_File_FullPath( Arg_File_Idx ) ) + '"', False, twfe_Default, Out_Str );
            end;
      end;
end;

function T_VCS_SVN_Wrapper.do_Run_VCS_Tool;
var str, out_s, error_s : string;
    work_folder : string;
    extra_arg : string;
    i : integer;
begin
  Result := False;
  (* escape ��� ����� %, ������� ����� ����������� � ����� �����.

    https://bitbucket.org/yncoder/desktopclient/issues/65
   *)

  str := '"' + SVN_exe + '"' + ' ' + Escape_Command_Line( Arg );
  extra_arg := '';

  if Arg_Auth_Need
    then
      begin
        if fAuth_Params.f_username <> ''
          then
            begin
              str := str + ' --username ' + fAuth_Params.f_username;

              (* ���� ���� - ������, �.�. ��� �������� ����� ������ ������������ *)
              if fAuth_Params.f_password <> ''
                then
                  begin
                    (* ��� ������ ��������� ��� ��������, ��� �� �� �� ���������� � ����� *)
                    str := str + ' --password %1';
                    extra_arg := ' ' + fAuth_Params.f_password;
                  end;
            end;
      end;

  (* proxy. ������������ ������ ��� ��������-���������� *)

  if ( Pos( Extract_Protocol( fVCS_URL ), f_const_local_protocol_list ) = 0 ) and ( Ini_Strore.HTTP_Proxy_Host <> '' )
    then
      begin
        str := str + ' --config-option servers:global:http-proxy-host=' + Ini_Strore.HTTP_Proxy_Host;

        if Ini_Strore.HTTP_Proxy_Port <> 0
          then
            str := str + ' --config-option servers:global:http-proxy-port=' + IntToStr( Ini_Strore.HTTP_Proxy_Port );
      end;

  (* ������� ������� ����� ��� ������� *)
  work_folder := '';
  case Arg_Work_Folder_Env of
    twfe_Default         : ;
    twfe_WorkFolder      : if ( fVCS_Folder <> '' ) and ( fVCS_Project_State <> ps_None )
                            then
                              work_folder := fVCS_Folder;
    twfe_WorkFolder_Up   : if ( fVCS_Folder <> '' ) and ( fVCS_Project_State <> ps_None )
                            then
                              begin
                                (* �� ���� ������� *)
                                work_folder := fVCS_Folder;
                                i := LastDelimiter( '\', work_folder );
                                if i > 0
                                  then
                                    begin
                                      work_folder := Copy( work_folder, 1, i - 1 );
                                      //if ( DirectoryExists( s ) )
                                    end;
                              end;
   end;

  //  str := str + ' --config-dir ' + '' + 'd:\#PROFILES#\Nachitov.YuV\AppData\Roaming\Naiveshark\svn_cfg\' + '' ;
  // read user configuration files from directory ARG
  (* ��. https://bitbucket.org/yncoder/desktopclient/issues/65

     ���� �����, ��������� � �������� ������� ��������� ������ � ���������� ����� - ��� ��������� ���������� %1

     ��. ���� - ������������� ����� % � persent_posiion

     @ � ������ ����������� ��� ���������� ������ ����� ������� � stdout. ��� ������ �� ������ ��������� �� �������
   *)
  if Execute_To_String_Filed( '@' + str, extra_arg, work_folder, out_s, error_s, Arg_Log )
    then
      begin
        Result := True;
      end
    else
      begin
        ShowMsg( error_s );
      end;

  Out_Str := out_s;
end;

function T_VCS_SVN_Wrapper.Scan;
var cmd_str,
    Out_Str : string;
    fXMLDocument_status: TXMLDocument;

    StartItemNode : IXMLNode;
    ANode : IXMLNode;

    xml_revision : OleVariant;
    fn : string;

    New_FileRec : T_FileRec;
begin
  cmd_str := 'st ' + '"' + SVN_Safe_FileName( Arg_Name ) + '"'+ ' --verbose --xml';
  (* ��� ����� (� �.�. �������� - ����������� ������ 1 ������� ��������) *)
  if Arg_Folder
    then
      cmd_str := cmd_str + ' --depth immediates';

  Result := Run_VCS_Tool( cmd_str, False, twfe_Default, Out_Str );

  if not Result
    then
      Exit;

  (* ������� xml *)

  fXMLDocument_status := TXMLDocument.Create( Self );
  fXMLDocument_status.LoadFromXML( Out_Str );

  fXMLDocument_status.Active := True;

  (* ����� xml *)
  StartItemNode := fXMLDocument_status.DocumentElement.ChildNodes.First.ChildNodes.FindNode('entry');

  ANode := StartItemNode;
  while ANode <> nil do
    begin
      (* ���� ������ �����, �� ������ ��������� ����� ���� �����, � �� ���� ���������� *)
      if ( not Arg_Folder ) or ( ANode <> StartItemNode )
        then
          begin
            with New_FileRec do
              begin
                fn := ANode.Attributes['path'];
                f_file_name := ExtractFileName( fn );
                (* svn-������ *)
                f_state := StateName2State( ANode.ChildNodes.FindNode('wc-status').Attributes['item'] );
                try
                  xml_revision := ANode.ChildNodes.FindNode('wc-status').Attributes['revision'];

                  if not VarIsNull( xml_revision )
                    then
                      f_revision := xml_revision
                    else
                      f_revision := revision_unknown;
                except
                  f_revision := revision_unknown;
                end;

                (* �����? *)
                f_folder_flag := DirectoryExists( fn );
              end;
            Add2Files_Array( Arg_Files_Array, New_FileRec );
          end;

      ANode := ANode.NextSibling;
    end;

  if fXMLDocument_status.Active
    then
      fXMLDocument_status.Active := False;

  fXMLDocument_status.Free;
end;

function T_VCS_SVN_Wrapper.StateName2State;
(* �������, ��� ��� �������� �� svn st --xml
   http://svnbook.red-bean.com/en/1.8/svn.ref.svn.c.status.html
 *)
const SVN_XML_File_State_Names : array[T_File_VCS_States] of string =
  ( '',
    'normal',
    'added',
    'deleted',
    'modified',
    '',
    'unversioned',
    'missing',
    'conflicted'
    
  );
var File_VCS_States_loop : T_File_VCS_States;
begin
  Result := file_vcs_state_unknown;

  for File_VCS_States_loop := low(T_File_VCS_States) to high(T_File_VCS_States) do
    begin
      if Arg_State_Name = SVN_XML_File_State_Names[ File_VCS_States_loop ]
        then
          begin
            Result := File_VCS_States_loop;
            break;
          end;
    end;
end;

function T_VCS_SVN_Wrapper.Update;
const tag_Summary_of_conflicts = 'Summary of conflicts:';
      //tag_Text_conflict = 'Text conflicts: ';

var Out_Str : string;
    //pos_sum_conflict
    //, tag_Text_conflict
    //: integer;
begin
  Result := tur_Fail;
  
  (* ��������� ����������.
     �������� ���������, �� �� ������ ������
     ��������� ������������ ����������
  *)                                                                                       
  if not Run_VCS_Tool( 'update ' + '"' + SVN_Safe_FileName( fVCS_Folder ) + '" --accept postpone --config-option config:miscellany:preserved-conflict-file-exts=* '
  +
  (* ������-�������� ��� ���������� ������ ��������� ������ (� ��� ����� svg)
     https://bitbucket.org/yncoder/desktopclient/issues/112/merge-svg-xml
   *)
  ' --diff3-cmd="' + text_merge_script +  '" '

  , True, twfe_Default, Out_Str )
    then
      Exit;

  (* ��� ���������� ����� ���� ��������� - ����� �� ���������� *)
  if pos( tag_Summary_of_conflicts, Out_Str ) > 0
    then
      begin
        (* ������, ���� �������� ���������� *)
        Result := tur_Conflict;
        ShowMsg( Out_Str );
      end
    else
      Result := tur_Ok;
end;

function T_VCS_SVN_Wrapper.Add_File;
var Out_Str, file_name : string;
    File_VCS_States : T_File_VCS_States;
    conflicted_file_rev : t_revision_id_type;
begin
  Result := False;

  if fFolder_Files_Array[ Arg_File_Idx ].f_state in Addable_states
    then
      begin
        (* ��������� ������� ������ �����.
           ���� ������ ��������� ��������, ���� ��������� ������ ������ ����� *)
        file_name := Get_File_FullPath( Arg_File_Idx, True );

        File_VCS_States := Check_File_State( file_name, conflicted_file_rev );

        if File_VCS_States in Addable_states
          then
            begin
              Result := Run_VCS_Tool( 'add ' + '"' + file_name + '"', False, twfe_Default, Out_Str );
            end;
      end;
end;

procedure T_VCS_SVN_Wrapper.After_Create;
begin
  f_const_local_protocol_list := 'file';
  f_const_protocol_list := 'file;http;https;svn';
end;

function T_VCS_SVN_Wrapper.Check_File_State;
var FA : T_Files_Array;
begin
  Result := file_vcs_state_unknown;

  SetLength( FA, 0 );

  (* ����������� ���� ����. �������������� ������ ���� ������ ��������� 1 ������� *)
  if ( Scan( Arg_File, FA, False ) ) and ( Length( FA ) = 1 )
    then
      begin
        Result := FA[0].f_state;
        out_conflicted_file_rev := FA[0].f_revision;
      end;
  SetLength( FA, 0 );
end;

function T_VCS_SVN_Wrapper.Check_Working_Copy_Outdated;
var Server_Repo_Info, Working_Copy_Info : T_Repo_Info;
begin
  Result := Get_Path_Info( fVCS_URL, True, Server_Repo_Info );
  if Result = tcr_Ok
    then
      begin
        Result := Get_Path_Info( fVCS_Folder, False, Working_Copy_Info );
        if Result = tcr_Ok
          then
            begin
              out_Server_Rev := Server_Repo_Info.Rev;
              out_Working_Copy_Rev := Working_Copy_Info.Rev;
            end;
      end;
end;

function T_VCS_SVN_Wrapper.Commit;
var Out_Str : string;
    message_file_name : string;
begin
  message_file_name := Get_Commit_MSG_File_Name;
  result := Run_VCS_Tool( 'commit ' + '"' + SVN_Safe_FileName( fVCS_Folder ) + '"' + ' -F "' + Get_Commit_MSG_File_Name + '"', True, twfe_Default, Out_Str );
  if Result
    then
      DeleteFile( message_file_name );
end;

function T_VCS_SVN_Wrapper.Export_File_Revisioned;
var Out_Str : string;
begin
  (* ������� ������������ ������ �����
     --force �������� ������������ ��������� ���� � ������ �������
  *)
  result :=
    Run_VCS_Tool(
      'export ' + '"' + fVCS_Folder + Slash + SVN_Safe_FileName( Arg_Local_Path ) + '"' + ' -r ' + IntToStr( Arg_Rev ) + ' ' +
      '"' + SVN_Safe_FileName( Arg_Dest_Path ) + '"' + ' --force'
      , False, twfe_Default, Out_Str );
end;

function T_VCS_SVN_Wrapper.Get_FileList_BeforeCommit;
var Out_Str : string;
    fXMLDocument_status: TXMLDocument;

    StartItemNode : IXMLNode;
    ANode : IXMLNode;

    fn : string;
    item_state : T_File_VCS_States;
    i : integer;
begin
  Result := tgfl_bc_None;
  SetLength( Arg_File_List, 0 );

  if not Run_VCS_Tool( 'st ' + ' "'+ SVN_Safe_FileName( fVCS_Folder ) + '" '+ ' --xml ', False, twfe_Default, Out_Str )
    then
      begin
        Result := tgfl_bc_Error;
        Exit;
      end;

  (* ������� xml *)

  fXMLDocument_status := TXMLDocument.Create( Self );
  fXMLDocument_status.LoadFromXML( Out_Str );

  fXMLDocument_status.Active := True;

  (* ����� xml *)
  StartItemNode := fXMLDocument_status.DocumentElement.ChildNodes.First.ChildNodes.FindNode('entry');

  ANode := StartItemNode;
  i := 0;
  while ( ANode <> nil ) and (result <> tgfl_bc_Forbid_Conflict) do
    begin
      (* ���� ������ �����, �� ������ ��������� ����� ���� �����, � �� ���� ���������� *)
          begin
              begin
                fn := ANode.Attributes['path'];
                item_state := StateName2State( ANode.ChildNodes.FindNode('wc-status').Attributes['item'] );

                if item_state in Resolvable_states
                  then
                    begin
                      (* ��������, ��� ���� ��������, � ����� �� ����� *)
                      result := tgfl_bc_Forbid_Conflict;
                      //out_File_List := '';
                      break;
                    end
                  else
                    begin
                      if item_state in Commitable_states
                        then
                          begin
                            (* ��� ��������� � ������� ������� ����� ���������� ��������� *)
                            SetLength( Arg_File_List, i + 1 );
                            with Arg_File_List[ i ] do
                              begin
                                f_file_name := ExtractFileName( fn );
                                f_state     := item_state;
                              end;
                            inc( i );
                            result := tgfl_bc_Commit_Yes;
                          end;
                    end;
              end;
          end;

      ANode := ANode.NextSibling;
    end;

  if fXMLDocument_status.Active
    then
      fXMLDocument_status.Active := False;

  fXMLDocument_status.Free;

end;

function T_VCS_SVN_Wrapper.Get_File_FullPath;
begin
  Result := inherited Get_File_FullPath( Arg_File_Idx, Arg_Canonical );
  if Arg_Canonical
    then
      Result := SVN_Safe_FileName( Result );
end;

function T_VCS_SVN_Wrapper.Get_Path_Info;
var Out_Str : string;
    fXMLDocument: TXMLDocument;
    Entry_ItemNode, Repo_ItemNode : IXMLNode;
    uuid : string;
    revision_id : t_revision_id_type; 
begin
  Result := tcr_Fail;

  (* ������� �������� *)
  if Arg_Remote
    then
      begin
        if ( Arg_Path = '' ) or ( not Check_VCS_URL( Arg_Path ) )
          then
            begin
              Result := tcr_WrongURL;
              Exit;
            end;
      end
    else
      begin
        if not DirectoryExists( Arg_Path )
          then
            begin
              Result := tcr_WrongURL;
              Exit;
            end;
      end;

  fXMLDocument := nil;
  if ( Run_VCS_Tool( 'info "' + SVN_Safe_FileName( Arg_Path ) + '" --xml ', True, twfe_Default, Out_Str ) )
    then
      begin
        (* ������� xml *)
        try
          fXMLDocument := TXMLDocument.Create( Self );
          fXMLDocument.LoadFromXML( Out_Str );

          fXMLDocument.Active := True;
          (* ����� xml *)
          Entry_ItemNode := fXMLDocument.DocumentElement.ChildNodes.FindNode('entry');
          if Assigned( Entry_ItemNode )
            then
              begin
                Repo_ItemNode := Entry_ItemNode.ChildNodes.FindNode('repository');
                if Assigned( Repo_ItemNode )
                  then
                    begin
                      uuid := Repo_ItemNode.ChildNodes.FindNode( 'uuid' ).Text;
                      revision_id := Entry_ItemNode.Attributes['revision'];

                      (* uuid ����������, � revision_id ����� ���� ����� 0, ���� ��� �� ���� �� ������ ������ *)
                      if ( uuid <> '' ) and ( revision_id >= 0 )
                        then
                          begin
                            Out_Repo_Info.UUID := uuid;
                            Out_Repo_Info.Rev  := revision_id;
                            Result := tcr_Ok;
                          end;
                    end;
              end;

        finally
          if Assigned( fXMLDocument )
            then
              begin
                if fXMLDocument.Active
                  then
                    fXMLDocument.Active := False;

                fXMLDocument.Free;
              end;
        end;
      end;
end;

function T_VCS_SVN_Wrapper.Get_Pristine_File_Path;
var Out_Str : string;
    fXMLDocument: TXMLDocument;
    Entry_ItemNode, WC_ItemNode : IXMLNode;

    checksum : string;

    working_copy_filename, pristine_filename : string;
begin
  Result := '';
  fXMLDocument := nil;

  if fFolder_Files_Array[ Arg_File_Idx ].f_state in Diffable_states
    then
      begin
        working_copy_filename := Get_File_FullPath( Arg_File_Idx, True );

        if ( Run_VCS_Tool( 'info "' + SVN_Safe_FileName( working_copy_filename ) + '" --xml ', True, twfe_Default, Out_Str ) )
          then
            begin
              (* ������� xml *)
              try
                fXMLDocument := TXMLDocument.Create( Self );
                fXMLDocument.LoadFromXML( Out_Str );

                fXMLDocument.Active := True;
                (* ����� xml *)
                Entry_ItemNode := fXMLDocument.DocumentElement.ChildNodes.FindNode('entry');
                if Assigned( Entry_ItemNode )
                  then
                    begin
                      WC_ItemNode := Entry_ItemNode.ChildNodes.FindNode('wc-info');
                      if Assigned( WC_ItemNode )
                        then
                          begin
                            checksum := WC_ItemNode.ChildNodes.FindNode( 'checksum' ).Text;

                            if ( checksum <> '' )
                              then
                                begin
                                  { https://bitbucket.org/yncoder/desktopclient/issues/85 }
                                  {$message warn '��� ���'}
                                  pristine_filename := fVCS_Folder + '\.svn\pristine\' + copy( checksum, 1, 2 ) + '\' + checksum + '.svn-base';
                                  if FileExists( pristine_filename )
                                    then
                                      Result := pristine_filename;
                                end;
                          end;
                    end;

              finally
                if Assigned( fXMLDocument )
                  then
                    begin
                      if fXMLDocument.Active
                        then
                          fXMLDocument.Active := False;

                      fXMLDocument.Free;
                    end;
              end;
            end;
      end;
end;

function T_VCS_SVN_Wrapper.Get_VCS_History;
var Out_Str : string;

    fXMLDocument: TXMLDocument;

    i : integer;

    StartItemNode : IXMLNode;
    ANode, File_Node : IXMLNode;

begin
  Result := False;
  SetLength( Out_Commits_Log_A, 0 );

  (* ������� log ������ �� ��������� ����� . *)
  (* �� ������-�� ��������� ���������� � ��������.
     ��. https://bitbucket.org/yncoder/desktopclient/issue/27 *)

  (* http://stackoverflow.com/questions/2675749/how-do-i-see-the-last-10-commits-in-reverse-chronoligical-order-with-svn *)
  if not Run_VCS_Tool( 'log -v -l ' + inttostr( log_limit ) + ' "' + SVN_Safe_FileName( Arg_Path ) + '"' + ' --xml', True, twfe_Default, Out_Str )
    then
      Exit;              
  
  (* ������� xml *)
  fXMLDocument := TXMLDocument.Create( Self );
  fXMLDocument.LoadFromXML( Out_Str );

  fXMLDocument.Active := True;

  (* ����� xml *)
  StartItemNode := fXMLDocument.DocumentElement.ChildNodes.FindNode('logentry');

  ANode := StartItemNode;
  while ANode <> nil do
    begin
      i := length( Out_Commits_Log_A );
      SetLength( Out_Commits_Log_A, i + 1 );
      with Out_Commits_Log_A[ i ] do
        begin
          f_revision_id := ANode.Attributes['revision'];
          f_author := ANode.ChildNodes['author'].Text;
          f_comment := ANode.ChildNodes['msg'].Text;
          f_datetime := XML_DT_2_Short( ANode.ChildNodes['date'].Text );
          (* � ������ ������ *)
          f_file_list := '';

          File_Node := ANode.ChildNodes.FindNode('paths');
          File_Node := File_Node.ChildNodes.FindNode( 'path' );

          while File_Node <> nil do
            begin
              if f_file_list <> ''
                then
                  f_file_list := f_file_list + ret;
              
              f_file_list := f_file_list + File_Node.Text;

              File_Node := File_Node.NextSibling;
            end;
        end;

      ANode := ANode.NextSibling;
    end;

  if fXMLDocument.Active
    then
      fXMLDocument.Active := False;

  Result := True;

  fXMLDocument.Free;
end;

function Prepare_SVN_bins;
begin
  Result := False;
  (* ��������� ���� � ���������� svn *)
  SVN_exe := Appl_Path + 'svn\svn.exe';

  (* ��������� ���� � �������� ���������� ������� *)
  text_merge_script := Appl_Path + 'svn\text_merge.bat';

  if ( FileExists( SVN_exe ) ) and ( FileExists( text_merge_script ) )
    then
      Result := True;
end;

end.
