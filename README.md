# README #

"Version Control for engineers" (http://soft.postpdm.com/index.html) and "CAD Diff" (http://soft.postpdm.com/cad_diff.html) software.

## How to get binaries ##

Ready to install binaries are available here:

https://bitbucket.org/yncoder/naiveshark/downloads/

## How to build ##

Software developed with Turbo Delphi IDE. All used libraries are included.

Windows installer build with NSIS

## How to contribute ##

1. Post a task in issue list and discuss your idea or report a bug
2. Create a Fork and send a pull-request

## License ##

Apache License, Version 2.0, January 2004

http://www.apache.org/licenses/LICENSE-2.0