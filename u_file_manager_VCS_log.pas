unit u_file_manager_VCS_log;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls,
  u_file_manager_VCS_Base, u_file_manager_VCS_const, StdCtrls, ExtCtrls,
  u_file_manager_VCS_log_frame;

type
  TVCS_Log_F = class(TForm)
    VCS_Log_Frame1: TVCS_Log_Frame;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Show_VCS_Log( Arg_Commits_Log_A : T_Commits_Log_A; Arg_VCS_Wrapper : T_VCS_Wrapper_Base; Arg_Path : string );

implementation

uses U_Const, u_file_manager_VCS, u_vcs_Ini;

{$R *.dfm}

procedure Show_VCS_Log;
var view_log_form : TVCS_Log_F;
begin
  view_log_form := TVCS_Log_F.Create( nil );

  with view_log_form do
    begin
      Ini_Strore.Set_Font( Font );
      Ini_Strore.Set_Font( VCS_Log_Frame1.Font );

      VCS_Log_Frame1.Init_Data( Arg_Commits_Log_A, Arg_VCS_Wrapper );

      Caption := 'Log for ' + Arg_Path;

      ShowModal;
      Free;
    end;
end;

procedure TVCS_Log_F.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Ini_Strore.Save_Form_Sizes( Self );
end;

procedure TVCS_Log_F.FormCreate(Sender: TObject);
begin
  Ini_Strore.Load_Form_Sizes( Self );
end;

end.
