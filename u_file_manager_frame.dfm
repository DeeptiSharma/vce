object File_Manager_Frame: TFile_Manager_Frame
  Left = 0
  Top = 0
  Width = 688
  Height = 397
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 27
    Align = alTop
    AutoSize = True
    TabOrder = 0
    DesignSize = (
      688
      27)
    object Current_Path_E: TEdit
      Left = 1
      Top = 1
      Width = 664
      Height = 21
      Anchors = [akLeft, akTop, akRight, akBottom]
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 1
    end
    object Up_B: TButton
      Left = 646
      Top = 1
      Width = 41
      Height = 25
      Action = Up_folder_A
      Align = alRight
      TabOrder = 0
    end
  end
  object Files_ListView: TListView
    Left = 0
    Top = 50
    Width = 688
    Height = 347
    Align = alClient
    Columns = <
      item
        AutoSize = True
        Caption = 'File name'
      end
      item
        AutoSize = True
        Caption = 'Status'
      end>
    ColumnClick = False
    HideSelection = False
    ReadOnly = True
    PopupMenu = PopupMenu1
    TabOrder = 1
    ViewStyle = vsReport
    OnChange = Files_ListViewChange
    OnDblClick = Files_ListViewDblClick
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 27
    Width = 688
    Height = 23
    AutoSize = True
    ButtonHeight = 21
    ButtonWidth = 87
    Caption = 'ToolBar1'
    Flat = False
    ParentShowHint = False
    ShowCaptions = True
    ShowHint = True
    TabOrder = 2
    object ToolButton2: TToolButton
      Left = 0
      Top = 2
      Action = Open_in_explorer_A
      AutoSize = True
    end
    object ToolButton11: TToolButton
      Left = 91
      Top = 2
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 99
      Top = 2
      Action = Edit_Project_A
      AutoSize = True
    end
    object ToolButton12: TToolButton
      Left = 165
      Top = 2
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 173
      Top = 2
      Action = Stage_A
      AutoSize = True
    end
    object ToolButton5: TToolButton
      Left = 212
      Top = 2
      Action = Revert_A
      AutoSize = True
    end
    object ToolButton3: TToolButton
      Left = 256
      Top = 2
      Action = Resolve_A
      AutoSize = True
    end
    object ToolButton4: TToolButton
      Left = 305
      Top = 2
      Action = Diff_A
      AutoSize = True
    end
    object ToolButton8: TToolButton
      Left = 357
      Top = 2
      Action = View_Log_A
      AutoSize = True
    end
    object ToolButton10: TToolButton
      Left = 407
      Top = 2
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 415
      Top = 2
      Action = Update_A
      AutoSize = True
    end
    object ToolButton1: TToolButton
      Left = 461
      Top = 2
      Action = Commit_A
      AutoSize = True
    end
    object ToolButton13: TToolButton
      Left = 507
      Top = 2
      Width = 8
      Caption = 'ToolButton13'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object ToolButton14: TToolButton
      Left = 515
      Top = 2
      Action = Close_A
      AutoSize = True
    end
  end
  object vcs_AL: TActionList
    Left = 504
    Top = 144
    object Stage_A: TAction
      Caption = 'Stage'
      OnExecute = Stage_AExecute
    end
    object Revert_A: TAction
      Caption = 'Revert'
      OnExecute = Revert_AExecute
    end
    object Commit_A: TAction
      Caption = 'Commit'
      OnExecute = Commit_AExecute
    end
    object Update_A: TAction
      Caption = 'Update'
      OnExecute = Update_AExecute
    end
    object View_Log_A: TAction
      Caption = 'View log'
      OnExecute = View_Log_Execute
    end
    object Open_in_explorer_A: TAction
      Caption = 'Open in Explorer'
      OnExecute = Open_in_explorer_AExecute
    end
    object Up_folder_A: TAction
      Caption = 'Up'
      OnExecute = Up_folder_AExecute
    end
    object Resolve_A: TAction
      Caption = 'Resolve'
      OnExecute = Resolve_AExecute
    end
    object View_Item_Log_A: TAction
      Caption = 'View item log'
      OnExecute = View_Item_Log_AExecute
    end
    object Show_Item_In_Explorer_A: TAction
      Caption = 'Show item in Explorer'
      OnExecute = Show_Item_In_Explorer_AExecute
    end
    object Drill_Down_Folder_A: TAction
      Caption = 'Enter folder'
      OnExecute = Drill_Down_Folder_AExecute
    end
    object Diff_A: TAction
      Caption = 'View diff'
      OnExecute = Diff_AExecute
    end
    object Edit_Project_A: TAction
      Caption = 'Edit project'
      OnExecute = Edit_Project_AExecute
    end
    object Copy_Path2Clipboard_A: TAction
      Caption = 'Copy path to clipboard'
      OnExecute = Copy_Path2Clipboard_AExecute
    end
    object Close_A: TAction
      Caption = 'Close'
      OnExecute = Close_AExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 328
    Top = 264
    object DrillDownFolderA1: TMenuItem
      Action = Drill_Down_Folder_A
    end
    object Diff1: TMenuItem
      Action = Diff_A
    end
    object Showiteminexplorer1: TMenuItem
      Action = Show_Item_In_Explorer_A
    end
    object Copypathtoclipboard1: TMenuItem
      Action = Copy_Path2Clipboard_A
    end
    object Viewitemlog1: TMenuItem
      Action = View_Item_Log_A
    end
  end
end
