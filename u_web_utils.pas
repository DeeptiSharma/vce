unit u_web_utils;

interface

function Extract_Protocol( Arg_URL : string ) : string;

function Check_URL( Arg_URL : string; Arg_protocol_list : string = '' ) : boolean;

(* ������������� �������� ���� � URL *)
function Convert_Path2URL( Arg_Path : string ) : string;

implementation

uses SysUtils, StrUtils, ShLwApi, IdURI;

function Extract_Protocol;
var IdURI : TIdURI;
begin
  IdURI := TIdURI.Create( Arg_URL );
  Result := LowerCase( IdURI.Protocol );
  IdURI.Free;
end;

(* ���� ������ �������, �� ���������. ���������� �������� �� ������. *)
function Occurrences(const Text: string; const Substring : Char ): integer;
var i : integer;
begin
  Result := 0;
  for i := 1 to Length( Text ) do
    if ( Text[ i ] = Substring )
      then
        Inc( Result );    
end;

function Check_URL;
var protocol : string;
begin
  Result := False;

  if Pos( '\', Arg_URL ) > 0
    then
      Exit;

  if Pos( '"', Arg_URL ) > 0
    then
      Exit;

  if not( Occurrences( Arg_URL, ':' ) in [1,2] ) (* ���� : ������������ ����� � ��� ��������� �����, ��� ��� : ����� ���� 1 ��� 2 ���� *)
    then
      Exit;

  if Length( Arg_URL ) < 2
    then
      Exit;

  if PathIsURL( PChar( Arg_URL ) )
    then
      begin
        protocol := Extract_Protocol( Arg_URL );

        if Arg_protocol_list = ''
          then
            Result := protocol <> ''
          else
            begin
              if pos( protocol, Arg_protocol_list ) > 0
                then
                  Result := True;
            end;
    end;
end;

function Convert_Path2URL;
begin
  Result := 'file:///' + ReplaceText( Arg_Path, '\', '/' );
end;

end.
