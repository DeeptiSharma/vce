unit U_URLs;

interface

{$include directives.inc}
const
      {$ifdef Naiveshark_agent }
      url_main_site_root = 'http://www.naiveshark.com';
      url_agent_page = 'http://soft.naiveshark.com/';
      url_update_page = url_agent_page;

      {$endif}
      {$ifdef CAD_Diff_App)}
      url_main_site_root = 'http://soft.postpdm.com';
      url_update_page = url_main_site_root;
      {$endif}

      {$ifdef VCS_Client}
      url_main_site_root = 'http://soft.postpdm.com';
      url_update_page = url_main_site_root;

      (* github ����� ��������� ����������� �� ������ � svn - ��� ������ ������ ������ � trunc ��� branch.
         ��� ������ �� svn ������ ������ ���� �� ���� (����� ����� ��� ���������������� ������ readme ����� ��� ��������) *)
      url_github_demo = 'https://github.com/postpdm/VCE_PostPDM_demo';

      url_where2host = 'http://soft.postpdm.com/docs.html#Where2host';
      {$endif}

      url_doc_site = 'http://naivesharkcomdoc.readthedocs.org/en/latest/agent.html';

      url_reg_page = 'http://www.naiveshark.com/account/signup/';

{$ifdef Naiveshark_agent}
function Get_Curr_Site : string;

function GetFeedURL : string;

function Get_Api_Version_URL : string;
function Get_My_Profile_URL : string;
function Get_MyProjects_URL : string;
function Get_Notifications_URL : string;

procedure OpenAgentPage;

procedure OpenAgentDoc;

{$endif}

{$ifdef VCS_Client}

procedure Open_Where2Host_Page;

{$endif}

{$if defined(Naiveshark_agent) or defined(VCS_Client) }
function Get_Version_Xml : string;
{$ifend}

procedure OpenMainSite;

procedure Open_DownloadPage;

procedure OpenOnlineRegPage;

implementation

uses {$ifdef Naiveshark_agent}
       U_Settings,
     {$endif}
     U_Const;

{$ifdef Naiveshark_agent}
const DebugHostURL = 'http://127.0.0.1:8000';
      ProdHostURL = 'http://www.naiveshark.com';

const url_version_xml = url_agent_page + 'agent/agentversion.xml';

function Get_Curr_Site : string;
begin
  if Debug_Mode
    then
      Result := DebugHostURL
    else
      Result := ProdHostURL;
end;

function GetFeedURL;
begin
  Result := Get_Curr_Site + '/data_extraction/latest_product/feed/';
end;

function Get_Version_Xml;
begin
  if Debug_Mode
    then
      Result := Appl_Path + 'agentversion.xml'
    else
      Result := url_version_xml;
end;

function Get_Api_Version_URL;
begin
  Result := Get_Curr_Site + '/webapi/get_api_ver/?format=json';
end;

function Get_My_Profile_URL;
begin
  Result := Get_Curr_Site + '/webapi/get_my_profile/?format=json';
end;

function Get_MyProjects_URL;
begin
  Result := Get_Curr_Site + '/webapi/my_projects/?format=json';
end;

function Get_Notifications_URL : string;
begin
  Result := Get_Curr_Site + '/webapi/notifications/?format=json'
end;

procedure OpenAgentPage;
begin
  OpenURL( url_agent_page );
end;

procedure OpenAgentDoc;
begin
  OpenURL( url_doc_site );
end;

     {$endif}

{$ifdef VCS_Client}
function Get_Version_Xml;
begin
  Result := url_main_site_root + '/pad/vcs_version.xml';
end;

procedure Open_Where2Host_Page;
begin
  OpenURL( url_where2host );
end;

{$endif}

procedure OpenMainSite;
begin
  OpenURL( url_main_site_root );
end;

procedure Open_DownloadPage;
begin
  OpenURL( url_update_page );
end;

procedure OpenOnlineRegPage;
begin
  OpenURL( url_reg_page );
end;

end.
