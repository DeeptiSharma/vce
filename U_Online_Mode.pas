unit U_Online_Mode;

interface

uses Classes;

type T_Online_Mode = class
      protected
       (* ������� ��������� *)
       fOnline_Flag : boolean;
       (* ������� ��� ������ ��� ����������� *)
       fJust_Started : boolean;
       (* ������� ��� ������������ ����������� *)
       fLogged_Flag : boolean;

       (* ��������� �� ����� ��������� *)
       fOnlineChangedEvent: TNotifyEvent;
       procedure SetOnline_Flag( Arg_Value : boolean );
       procedure SetLogged_Flag( Arg_Value : boolean );
      public
       property Online_Flag : boolean read fOnline_Flag write SetOnline_Flag;
       property Just_Started : boolean read fJust_Started;
       property Logged_Flag : boolean read fLogged_Flag write SetLogged_Flag;
       constructor Create;
       property OnlineChangedEvent: TNotifyEvent read fOnlineChangedEvent write fOnlineChangedEvent;
      end;

implementation

{ T_Online_Mode }

constructor T_Online_Mode.Create;
begin
  fOnline_Flag := False;
  fJust_Started := True;
  fLogged_Flag := False;
end;

procedure T_Online_Mode.SetLogged_Flag;
begin
  if fLogged_Flag <> Arg_Value
    then
      fLogged_Flag := Arg_Value;
end;

procedure T_Online_Mode.SetOnline_Flag;
begin
  if fOnline_Flag <> Arg_Value
    then
      begin
        fOnline_Flag := Arg_Value;
        if Assigned( fOnlineChangedEvent ) and ( not fJust_Started )
          then
            fOnlineChangedEvent( Self );
      end;
  if fJust_Started then fJust_Started := False;
end;

end.
