object Project_Form: TProject_Form
  Left = 0
  Top = 0
  Caption = 'Project_Form'
  ClientHeight = 402
  ClientWidth = 604
  Color = clBtnFace
  Constraints.MinHeight = 440
  Constraints.MinWidth = 620
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inline File_Manager_Frame1: TFile_Manager_Frame
    Left = 0
    Top = 0
    Width = 604
    Height = 402
    Align = alClient
    TabOrder = 1
    ExplicitWidth = 604
    ExplicitHeight = 402
    inherited Panel1: TPanel
      Width = 604
      ExplicitWidth = 604
      inherited Current_Path_E: TEdit
        Width = 580
        ExplicitWidth = 580
      end
      inherited Up_B: TButton
        Left = 562
        ExplicitLeft = 562
      end
    end
    inherited Files_ListView: TListView
      Width = 604
      Height = 352
      ExplicitWidth = 604
      ExplicitHeight = 352
    end
    inherited ToolBar1: TToolBar
      Width = 604
      ExplicitWidth = 604
      inherited ToolButton2: TToolButton
        ExplicitWidth = 91
      end
      inherited ToolButton6: TToolButton
        ExplicitWidth = 66
      end
      inherited ToolButton7: TToolButton
        ExplicitWidth = 39
      end
      inherited ToolButton5: TToolButton
        ExplicitWidth = 44
      end
      inherited ToolButton3: TToolButton
        ExplicitWidth = 49
      end
      inherited ToolButton4: TToolButton
        ExplicitWidth = 52
      end
      inherited ToolButton8: TToolButton
        ExplicitWidth = 50
      end
      inherited ToolButton9: TToolButton
        ExplicitWidth = 46
      end
      inherited ToolButton1: TToolButton
        ExplicitWidth = 46
      end
      inherited ToolButton14: TToolButton
        ExplicitWidth = 37
      end
    end
  end
  object Project_Cfg_P: TPanel
    Left = 0
    Top = 0
    Width = 604
    Height = 402
    Align = alClient
    TabOrder = 0
    DesignSize = (
      604
      402)
    object Server_Url_L: TLabel
      Left = 9
      Top = 80
      Width = 245
      Height = 13
      Caption = 'Server repository URL, Test it then press Checkout'
    end
    object URL_Examples_C_L: TLabel
      Left = 9
      Top = 126
      Width = 67
      Height = 13
      Caption = 'URL examples'
    end
    object Username_L: TLabel
      Left = 9
      Top = 149
      Width = 51
      Height = 13
      Caption = 'User name'
    end
    object Password_L: TLabel
      Left = 317
      Top = 149
      Width = 46
      Height = 13
      Caption = 'Password'
    end
    object New_working_copy_path_L: TLabel
      Left = 9
      Top = 320
      Width = 112
      Height = 13
      Caption = 'New working copy path'
    end
    object Calculated_Checkout_path_E: TEdit
      Left = 7
      Top = 339
      Width = 586
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 7
    end
    object ToolBar1: TToolBar
      Left = 1
      Top = 378
      Width = 602
      Height = 23
      Align = alBottom
      AutoSize = True
      ButtonHeight = 21
      ButtonWidth = 190
      Caption = 'ToolBar1'
      Flat = False
      ShowCaptions = True
      TabOrder = 8
      object Checkout_B: TToolButton
        Left = 0
        Top = 2
        Action = Checkout_A
        AutoSize = True
      end
      object Close_B: TToolButton
        Left = 194
        Top = 2
        Action = Close_A
        AutoSize = True
      end
      object ToolButton2: TToolButton
        Left = 231
        Top = 2
        Width = 8
        Caption = 'ToolButton2'
        ImageIndex = 1
        Style = tbsSeparator
      end
    end
    object Info_P: TPanel
      Left = 1
      Top = 1
      Width = 602
      Height = 73
      Align = alTop
      TabOrder = 9
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 314
        Height = 13
        Caption = 
          'Type or select Server repository URL, test it then press Checkou' +
          't'
      end
      object IKnowUrl_B: TButton
        Left = 8
        Top = 32
        Width = 156
        Height = 25
        Caption = 'I know repository URL'
        TabOrder = 0
        OnClick = IKnowUrl_BClick
      end
      object IWantGitHub_B: TButton
        Left = 180
        Top = 32
        Width = 197
        Height = 25
        Caption = 'I want to checkout GitHub repository'
        TabOrder = 1
        OnClick = IWantGitHub_BClick
      end
      object IHaveLocalRepo_B: TButton
        Left = 383
        Top = 32
        Width = 168
        Height = 25
        Caption = 'I have local SVN repository'
        TabOrder = 2
        OnClick = IHaveLocalRepo_BClick
      end
    end
    object Server_Url_E: TComboBox
      Left = 9
      Top = 99
      Width = 591
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ItemHeight = 13
      TabOrder = 0
      OnChange = Server_Url_EChange
    end
    object URL_Examples_L: TEdit
      Left = 82
      Top = 126
      Width = 513
      Height = 21
      TabStop = False
      Anchors = [akLeft, akTop, akRight]
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 10
    end
    object Username_E: TEdit
      Left = 82
      Top = 146
      Width = 223
      Height = 21
      TabOrder = 1
      OnChange = Username_EChange
    end
    object Password_E: TEdit
      Left = 384
      Top = 146
      Width = 201
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
      OnChange = Password_EChange
    end
    object Repo_Test_View_B: TButton
      Left = 54
      Top = 185
      Width = 303
      Height = 40
      Caption = 'Test repo'
      Enabled = False
      TabOrder = 3
      OnClick = Repo_Test_View_BClick
    end
    object Local_path_CB: TCheckBox
      Left = 9
      Top = 231
      Width = 631
      Height = 39
      Caption = 'Set manual root for working copy'
      TabOrder = 4
      OnClick = Local_path_CBClick
    end
    object Working_Copy_Path_E: TEdit
      Left = 9
      Top = 276
      Width = 490
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Enabled = False
      TabOrder = 5
      OnChange = Working_Copy_Path_EChange
    end
    object Select_B: TButton
      Left = 520
      Top = 274
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Select'
      Enabled = False
      TabOrder = 6
      OnClick = Select_BClick
    end
  end
  object ActionList1: TActionList
    Left = 496
    Top = 152
    object Checkout_A: TAction
      Caption = 'Checkout tested repo to working copy'
      OnExecute = Checkout_BClick
    end
    object Close_A: TAction
      Caption = 'Close'
      OnExecute = Close_AExecute
    end
  end
end
