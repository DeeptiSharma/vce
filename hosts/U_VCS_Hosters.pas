(* ������ � API ��������� ��������� *)
unit U_VCS_Hosters;

interface

type T_Hosted_Repo_Rec = record
       fName,
       fSVN_Url : string;
       fFork : boolean;
      end;

type T_Hosted_Repo_A = array of T_Hosted_Repo_Rec;

(* ���������� ��������� � GitHub *)
type T_GitHub_Res_Type = ( ghrt_Unknown, ghrt_No_Connection, ghrt_NoSuchUser, ghrt_Ok );

function Get_GitHub_User_Repos( Arg_UserName : string; var Arg_Hosted_Repo_A : T_Hosted_Repo_A ) : T_GitHub_Res_Type;

implementation

uses UrlMon, superobject, Classes, sysutils, U_Const;

function Get_GitHub_User_Repos;
const cGitHubUserRepos = 'https://api.github.com/users/' + '%s' + '/repos';
      cGitHubAPIRoot = 'https://api.github.com';

var obj: ISuperObject;
    sl : TStringList;
    file_name : string;
    i : integer;
    url : string;
    download_res : HResult;

begin
  Result := ghrt_Unknown;
  
  SetLength( Arg_Hosted_Repo_A, 0 );

  file_name := Get_Rnd_Temp_File_Name( True ) + '.json';
  if FileExists( file_name ) then DeleteFile(file_name );

  url := format( cGitHubUserRepos, [ Arg_UserName ] );

  (* https://msdn.microsoft.com/en-us/library/ms775123(v=vs.85).aspx *)
  download_res := URLDownloadToFile( nil, pansichar( url ), PChar( file_name ), 0, nil );
  case download_res of
    S_OK :
      begin
        (* ParseFile *)
        if FileExists( file_name )
          then
            begin
              sl := TStringList.Create;
              sl.LoadFromFile( file_name );
              obj := SO( sl.Text );
              sl.Free;

              SetLength( Arg_Hosted_Repo_A, obj.AsArray.Length );

              for i := 0 to obj.AsArray.Length - 1 do
                with Arg_Hosted_Repo_A[ i ] do
                  begin
                    fName    := obj.AsArray[i].S['name'];
                    fSVN_Url := obj.AsArray[i].S['svn_url'];
                    fFork    := obj.AsArray[i].B['fork'];
                  end;
            end;
        Result := ghrt_Ok;
      end;
    else
    //E_OUTOFMEMORY : ;
    //INET_E_DOWNLOAD_FAILURE : ;
      begin
        (* ��� 2 �������� - ��� ����� � github (��������, ��� ���������, ��� ������������ ������ ���� ����),
           ��� ��� ������ ����� *)
        (* ���������, ���� �� ������ ����� � API *)

        file_name := Get_Rnd_Temp_File_Name( True ) + '.json';
        if FileExists( file_name ) then DeleteFile(file_name );

        if URLDownloadToFile( nil, pansichar( cGitHubAPIRoot ), PChar( file_name ), 0, nil ) = S_OK
          then
            Result := ghrt_NoSuchUser (* ����� ���� - ����� �����, ��� ���� � ���������� ����� *)
          else
            Result := ghrt_No_Connection; (* ��� ����� ������ � API *)
      end;
  end;
end;

end.
