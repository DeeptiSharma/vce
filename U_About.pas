unit U_About;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, jpeg, ComCtrls;

type
  TAboutBox = class(TForm)
    OKButton: TButton;
    PageControl1: TPageControl;
    Credits_TSh: TTabSheet;
    Inforrmation_TSh: TTabSheet;
    Ini_Path_L: TLabel;
    Ini_Path_E: TEdit;
    Panel1: TPanel;
    ProgramIcon: TImage;
    ProductName_L: TLabel;
    Version_L: TLabel;
    Copyright: TLabel;
    www_main_l: TLabel;
    www_agent_l: TLabel;
    www_doc_l: TLabel;
    New_Version_L: TLabel;
    Check_Status_L: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure www_main_lClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure www_agent_lClick(Sender: TObject);
    procedure www_doc_lClick(Sender: TObject);
    procedure New_Version_LClick(Sender: TObject);
  private
    { Private declarations }
    Singleton_Flag : boolean;
  public
    { Public declarations }
    procedure Check_Update;
  end;

procedure Run_About_Singleton;

procedure Close_About;

implementation

uses U_Msg, U_Ver, U_URLs, U_Ini, U_CheckUpdate;

{$R *.dfm}

var AboutBox_Singleton : TAboutBox;

procedure Run_About_Singleton;
begin
  if not Assigned( AboutBox_Singleton )
    then
      begin
        AboutBox_Singleton := TAboutBox.Create(Application);
        AboutBox_Singleton.Singleton_Flag := True;
      end;
  AboutBox_Singleton.Show;
  (* срабатывает после отображения Show *)
  AboutBox_Singleton.Check_Update;
end;

procedure Close_About;
begin
  if Assigned( AboutBox_Singleton )
    then
      AboutBox_Singleton.Close;
end;

procedure TAboutBox.Check_Update;
var new_ver_cap : string;
begin
  case CheckUpdate( Self, new_ver_cap ) of
   cur_Fail :
      begin
        Check_Status_L.Caption := msg_ConnectFail;
        Check_Status_L.Visible := True;
        New_Version_L.Visible := False;
      end;
   cur_NewDetected :
      begin
        New_Version_L.Caption := new_ver_cap;
        New_Version_L.Width := 210;
        New_Version_L.Visible := True;
        Check_Status_L.Visible := False;
      end;
    cur_UpToDate :
      begin
        New_Version_L.Visible := False;
        Check_Status_L.Caption := msg_Version_UpToDate;
        Check_Status_L.Visible := True;
      end;
  end;
end;

procedure TAboutBox.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  AboutBox_Singleton := nil;
end;

procedure TAboutBox.FormCreate(Sender: TObject);
begin
  ProductName_L.Caption := program_title;
  Version_L.Caption := 'Version: ' + Full_Version_Name;
  www_main_l.Caption := url_main_site_root;
  www_agent_l.Caption := url_agent_page;
  www_doc_l.Caption := url_doc_site;

  Ini_Path_E.Text := Ini_Strore.Get_Ini_Path;
end;

procedure TAboutBox.FormShow(Sender: TObject);
begin
  if not Singleton_Flag
    then
      Raise Exception.Create('Singleton error happened');
end;

procedure TAboutBox.New_Version_LClick(Sender: TObject);
begin
  OpenAgentDownloadPage;
end;

procedure TAboutBox.OKButtonClick(Sender: TObject);
begin
  Close;
end;

procedure TAboutBox.www_agent_lClick(Sender: TObject);
begin
  OpenAgentPage;
end;

procedure TAboutBox.www_doc_lClick(Sender: TObject);
begin
  OpenAgentDoc;
end;

procedure TAboutBox.www_main_lClick(Sender: TObject);
begin
  OpenMainSite;
end;

initialization

end.
