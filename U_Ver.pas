unit U_Ver;

interface

var Major_Ver,
    Minor_Ver : integer;
    
    Full_Version_Name : string;
    form_caption : string;
    Build_Name : string;
    Build_N : integer;

(* �������������� ������ API *)    
const Web_Api_Version = 1;    

(* ���������� ������ �� ���������� �� EXE *)
procedure Get_Version_From_Exe;

implementation

uses Windows, Forms, Sysutils;

(* ���������� ������ �� ���������� �� EXE *)
procedure Get_Version_From_Exe;
var
  FileName: string;
  InfoSize, Wnd: DWORD;
  VerBuf: Pointer;
  FI: PVSFixedFileInfo;
  VerSize: DWORD;
  Major, Minor, Release, Build : word;
begin

    FileName := Application.ExeName;
    InfoSize := GetFileVersionInfoSize(PChar(FileName), Wnd);
    if InfoSize <> 0 then
    begin
      GetMem(VerBuf, InfoSize);
      try
        if GetFileVersionInfo(PChar(FileName), Wnd, InfoSize, VerBuf) then
          if VerQueryValue(VerBuf, '\', Pointer(FI), VerSize) then
            begin
              Major   := FI.dwFileVersionMS shr 16;
              Minor   := FI.dwFileVersionMS ;
              Release := FI.dwFileVersionLS shr 16;
              Build   := FI.dwFileVersionLS;

              Major_Ver := Major;
              Minor_Ver := Minor;

              Build_N    := Build;
              Build_Name := IntToStr( Build );

              Full_Version_Name := inttostr( Major_Ver ) + '.' + inttostr( Minor_Ver );
            end;
      finally
        FreeMem(VerBuf);
      end;
    end;
end;

end.
