unit U_Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, U_RSSReader, Menus, ActnList, U_TrayIcon,
  U_API_Wrapper, U_Login_Frame, StdActns;

(* ��������� � ������� � systray *)
const WM_BACT2LIFENOTIFY = WM_USER + 11;

type
  TNaivesharkAgentMain_Form = class(TForm)
    Main_Timer: TTimer;
    Panel1: TPanel;
    Status_L: TStaticText;
    TrayPopupMenu: TPopupMenu;
    ActionList1: TActionList;
    Restore_A: TAction;
    Exit_A: TAction;
    RestoreA1: TMenuItem;
    CloseA1: TMenuItem;
    N1: TMenuItem;
    PageControl1: TPageControl;
    MyProjects_TS: TTabSheet;
    Products_Feed_TS: TTabSheet;
    RSS_LV: TListView;
    MyProjects_LV: TListView;
    Login_Frame1: TLogin_Frame;
    Notifications_LV: TListView;
    Splitter1: TSplitter;
    User_Name_L: TLabel;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    About_A: TAction;
    Help1: TMenuItem;
    About1: TMenuItem;
    Logout_A: TAction;
    Logout1: TMenuItem;
    Full_Exit_A: TAction;
    FullExitA1: TMenuItem;
    Minimize_A: TAction;
    Minimize1: TMenuItem;
    N2: TMenuItem;
    procedure Main_TimerTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
    procedure Exit_AExecute(Sender: TObject);
    procedure Restore_AExecute(Sender: TObject);
    procedure RSS_LVClick(Sender: TObject);
    (* ��������� �� ������ � SysTray *)
    procedure WMBACT2LIFENOTIFY(var Msg: TMessage); message WM_BACT2LIFENOTIFY;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MyProjects_LVClick(Sender: TObject);
    procedure Login_Frame1Login_BClick(Sender: TObject);
    procedure Notifications_LVClick(Sender: TObject);
    procedure About_AExecute(Sender: TObject);
    procedure Logout_AExecute(Sender: TObject);
    procedure Full_Exit_AExecute(Sender: TObject);
    procedure Minimize_AExecute(Sender: TObject);
  private
    { Private declarations }
    RSS_Reader : T_RSS_Reader;

    API_Wrapper_DataModule: TAPI_Wrapper_DataModule;

    Wrong_Login_Flag : boolean;

    procedure Refresh_Mode;
    procedure SetCaptions;
    procedure ShowTrayMsg( Arg_Msg : string; Arg_Flag : TBalloonFlags = bfNone );

    procedure OnMinimize(Sender:TObject);
    procedure OnlineHandler(Sender:TObject);
    procedure RestoreApp;

    procedure Refresh_RSS;
    procedure Refresh_MyData;
    procedure Refresh_Logged_Status;
  public
    { Public declarations }
    TrayIcon1 : TExtTrayIcon;

    procedure Logout;
  end;

var
  NaivesharkAgentMain_Form: TNaivesharkAgentMain_Form;

implementation

{$R *.dfm}

uses U_URLs, U_Settings, U_Const, U_Msg, U_About, U_Ini, U_Net_Const;

procedure TNaivesharkAgentMain_Form.About_AExecute(Sender: TObject);
begin
  Run_About_Singleton;
end;

procedure TNaivesharkAgentMain_Form.Exit_AExecute(Sender: TObject);
begin
  OnCloseQuery := nil;
  Close;
end;

procedure TNaivesharkAgentMain_Form.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Ini_Strore.Save_Form_Sizes( Self );
end;

procedure TNaivesharkAgentMain_Form.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := False;
  Application.Minimize;
end;

procedure TNaivesharkAgentMain_Form.FormCreate(Sender: TObject);
begin  
  TrayIcon1 := TExtTrayIcon.Create( Self );
  TrayIcon1.OnClick := TrayIcon1Click;
  TrayIcon1.PopupMenu := TrayPopupMenu;
  TrayIcon1.OnBalloonClick := TrayIcon1Click;
  TrayIcon1.Visible := True;

  Wrong_Login_Flag := False;
  
  Login_Frame1.Set_Labels;

  Application.OnMinimize := OnMinimize;
  Online_Mode.OnlineChangedEvent := OnlineHandler;

  if ( ParamCount > 0 ) and ( ParamStr(1) = 'Debug' )
    then
      Debug_Mode := True
    else
      Debug_Mode := False;

  Full_Exit_A.Visible := Debug_Mode;

  SetCaptions;

  RSS_Reader := T_RSS_Reader.Create( GetFeedURL );

  API_Wrapper_DataModule := TAPI_Wrapper_DataModule.Create( Self );

  Main_Timer.Interval := Get_Refresh_Interval;
  Main_Timer.Enabled := True;
  Main_TimerTimer( Sender );

  Ini_Strore.Load_Form_Sizes( Self );

  Refresh_Logged_Status;
end;

procedure TNaivesharkAgentMain_Form.FormDestroy(Sender: TObject);
begin
  Main_Timer.Enabled := False;

  TrayIcon1.Free;
  RSS_Reader.Free;
  API_Wrapper_DataModule.Free;
  API_Wrapper_DataModule := nil;
end;

procedure TNaivesharkAgentMain_Form.Full_Exit_AExecute(Sender: TObject);
begin
  Exit_A.Execute;
end;

procedure TNaivesharkAgentMain_Form.Login_Frame1Login_BClick(
  Sender: TObject);
begin
  Wrong_Login_Flag := False;
  
  Login_Frame1.Save_Prev;

  API_Wrapper_DataModule.SetLogin_PW( Login_Frame1.Username_E.Text, Login_Frame1.Password_E.Text );
  Refresh_MyData;
end;

procedure TNaivesharkAgentMain_Form.Logout;
begin
  Wrong_Login_Flag := False;

  Login_Frame1.Clear_Login;

  API_Wrapper_DataModule.Logout;
  Refresh_MyData;
end;

procedure TNaivesharkAgentMain_Form.Logout_AExecute(Sender: TObject);
begin
  if Show_Q( cap(cap_Logout), false )
    then
      begin
        Logout;
      end;
end;

procedure TNaivesharkAgentMain_Form.RSS_LVClick(Sender: TObject);
var Selected : TListItem;
    url : string;
begin
  Selected := RSS_LV.Selected;
  if Selected <> nil
     then
       begin
         url := RSS_Reader.Get_Url( selected.Index );
         if url <> ''
           then
             OpenURL( url );
       end;
end;

procedure TNaivesharkAgentMain_Form.SetCaptions;
var debug_title : string;
begin
  if Debug_Mode
    then
      debug_title := ' [DEBUG]';

  Caption := program_title + debug_title + ' ' + Get_Curr_Site;
  Application.Title := Caption;
  TrayIcon1.Hint := Caption;
end;

procedure TNaivesharkAgentMain_Form.ShowTrayMsg;
begin
{  if TrayIcon1.Visible
    then }
      begin
        TrayIcon1.BalloonHint := Arg_Msg;
        TrayIcon1.BalloonFlags := Arg_Flag;
        TrayIcon1.BalloonTitle := program_title;

        TrayIcon1.ShowBalloonHint;
      end;
end;

procedure TNaivesharkAgentMain_Form.TrayIcon1Click(Sender: TObject);
begin
  if Visible
    then
      Application.Minimize
    else
      begin
        RestoreApp;
      end;
end;

procedure TNaivesharkAgentMain_Form.WMBACT2LIFENOTIFY;
begin
  RestoreApp;
end;

procedure TNaivesharkAgentMain_Form.Refresh_Logged_Status;
begin
  Splitter1.Visible := False;

  if Online_Mode.Logged_Flag
    then
      begin
        MyProjects_LV.Visible := False; //True;

        Notifications_LV.Visible := True;
        Login_Frame1.Visible := False;

        User_Name_L.Caption := Cap( cap_Login ) + API_Wrapper_DataModule.Get_Login;
        User_Name_L.Visible := True;
        Logout_A.Enabled := True;
      end
    else
      begin
        Notifications_LV.Visible := False;
        MyProjects_LV.Visible := False;
        Login_Frame1.Visible := True;

        User_Name_L.Caption := '';
        User_Name_L.Visible := False;
        Logout_A.Enabled := False;
      end;
end;

procedure TNaivesharkAgentMain_Form.Refresh_Mode;
var Refresh_Interval : integer;
    s : string;
begin
  Refresh_Interval := Get_Refresh_Interval;
  s := inttostr( Refresh_Interval div 1000 ) + ' sec. refresh) ' + TimeToStr( time );
  if Online_Mode.Online_Flag
    then
      begin
        Status_L.Caption := 'Online (' + s;
        Status_L.Color := clBtnFace;
      end
    else
      begin
        Status_L.Caption := 'Offline (' + s;
        Status_L.Color := clRed;
      end;

  Main_Timer.Interval := Refresh_Interval;
end;

procedure TNaivesharkAgentMain_Form.Refresh_MyData;

procedure Fill_MyProjects_LV;
var //MyProjects : T_Project_Array;
    i : integer;
begin
  {MyProjects := API_Wrapper_DataModule.Get_My_Projects;
  MyProjects_LV.Items.Clear;

  for i := 0 to Length( MyProjects ) - 1 do
    with MyProjects_LV.Items.Add do
      begin
        Caption := MyProjects[ i ].fullname;
        Data    := pointer( MyProjects[ i ].id );
        SubItems.Add( MyProjects[ i ].url );
      end;}
end;

procedure Fill_Notifications_LV;
var Notifications : T_Notification_Array;
    i : integer;
begin
  Notifications := API_Wrapper_DataModule.Get_Notifications;
  Notifications_LV.Items.Clear;

  for i := 0 to Length( Notifications ) - 1 do
    with Notifications_LV.Items.Add do
      begin
        Caption := Notifications[ i ].msg_txt;
        Data    := pointer( Notifications[ i ].id );
        SubItems.Add( Notifications[ i ].created_at );
        SubItems.Add( Notifications[ i ].sender_user );
      end;
end;

begin
  if Wrong_Login_Flag
    then
      Exit;

  Login_Frame1.Switch_Wrong_Login_MSG( False );

  case API_Wrapper_DataModule.Refresh of
    nc_res_Auth_Empty :
      begin
        Online_Mode.Logged_Flag := False;
        Online_Mode.Online_Flag := True;
        //Login_Frame1.Switch_Wrong_Login_MSG( True );
      end;
    nc_res_Auth_Fail :
      begin
        Online_Mode.Logged_Flag := False;
        Online_Mode.Online_Flag := True;
        Login_Frame1.Switch_Wrong_Login_MSG( True );
        Wrong_Login_Flag := True;
      end;
    nc_res_DataNoChange :
      begin
        (* ������ �� ���������� *)
      end;
    nc_res_NewData :
      begin
        Online_Mode.Logged_Flag := True;
        Online_Mode.Online_Flag := True;

        //Fill_MyProjects_LV;
        Fill_Notifications_LV;

        ShowTrayMsg( msg_New_Notification_Data );
      end;
    nc_res_Fail_but_Cash :
      begin
        Online_Mode.Logged_Flag := True;
        Online_Mode.Online_Flag := False;

        //Fill_MyProjects_LV;
        Fill_Notifications_LV;
      end;
   end;
  Refresh_Logged_Status;   
end;

procedure TNaivesharkAgentMain_Form.Refresh_RSS;
var RSS_Array : T_RSS_Array;
    i : integer;
begin
  case RSS_Reader.Refresh of
    nc_res_NewData :
      begin
        RSS_LV.Clear;
        RSS_Array := RSS_Reader.Get_RSS_Array;
        for i := 0 to Length( RSS_Array ) - 1 do
          with RSS_LV.Items.Add do
            begin
              Caption := RSS_Array[i].fTitle;
              SubItems.Add(RSS_Array[i].fDT);
              SubItems.Add(RSS_Array[i].fDesc);
            end;

        if not Online_Mode.Just_Started
          then
            ShowTrayMsg( msg_New_Feed_Data );
        Online_Mode.Online_Flag := True;
      end;
    nc_res_DataNoChange:
      begin
        (* rss �� ��������� *)
        Online_Mode.Online_Flag := True;
      end;
    nc_res_Fail :
      begin
        Online_Mode.Online_Flag := False;
      end;
   end;
  Refresh_Mode;
end;

procedure TNaivesharkAgentMain_Form.RestoreApp;
begin
  Show;
  Application.BringToFront;
  Application.Restore;
  //TrayIcon1.Visible := False;
end;

procedure TNaivesharkAgentMain_Form.Restore_AExecute(Sender: TObject);
begin
  TrayIcon1Click( Sender );
end;

procedure TNaivesharkAgentMain_Form.Main_TimerTimer(Sender: TObject);
begin
  Refresh_RSS;
  Refresh_MyData;
end;

procedure TNaivesharkAgentMain_Form.Minimize_AExecute(Sender: TObject);
var CanClose: Boolean;
begin
  FormCloseQuery( Sender, CanClose );
end;

procedure TNaivesharkAgentMain_Form.MyProjects_LVClick(Sender: TObject);
var Selected : TListItem;
begin
{  Selected := MyProjects_LV.Selected;
  if Selected <> nil
     then
       begin
         View_Project( API_Wrapper_DataModule.Get_My_Projects[ Selected.Index ] );
       end;}
end;

procedure TNaivesharkAgentMain_Form.Notifications_LVClick(Sender: TObject);
var Selected : TListItem;
    url : string;
begin
  Selected := Notifications_LV.Selected;
  if Selected <> nil
     then
       begin
         url := API_Wrapper_DataModule.Get_Notifications[ Selected.Index ].absolute_url;
         if url <> ''
           then
             begin
               OpenURL( url );
             end;
       end;
end;

procedure TNaivesharkAgentMain_Form.OnlineHandler(Sender: TObject);
begin
  if Online_Mode.Online_Flag
    then
      ShowTrayMsg( 'Online' )
    else
      ShowTrayMsg( 'Offline', bfWarning );
end;

procedure TNaivesharkAgentMain_Form.OnMinimize(Sender: TObject);
begin
  //TrayIcon1.Visible := True;
  Close_About;
  Hide;
end;

end.
