object NaivesharkAgentMain_Form: TNaivesharkAgentMain_Form
  Left = 0
  Top = 0
  Caption = 'Main'
  ClientHeight = 439
  ClientWidth = 578
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDefault
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 413
    Width = 578
    Height = 26
    Align = alBottom
    BevelKind = bkSoft
    TabOrder = 0
    object User_Name_L: TLabel
      Left = 259
      Top = 1
      Width = 79
      Height = 16
      Caption = 'User_Name_L'
      Visible = False
    end
    object Status_L: TStaticText
      Left = 14
      Top = 1
      Width = 53
      Height = 20
      Caption = 'Status_L'
      TabOrder = 0
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 578
    Height = 413
    ActivePage = MyProjects_TS
    Align = alClient
    TabOrder = 1
    object MyProjects_TS: TTabSheet
      Caption = 'Notifications'
      object Splitter1: TSplitter
        Left = 0
        Top = 282
        Width = 570
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitTop = 124
        ExplicitWidth = 124
      end
      object MyProjects_LV: TListView
        Left = 0
        Top = 285
        Width = 570
        Height = 97
        Align = alBottom
        Columns = <
          item
            AutoSize = True
            Caption = 'Fullname'
          end
          item
            AutoSize = True
            Caption = 'URL'
          end>
        ColumnClick = False
        HotTrackStyles = [htHandPoint]
        ReadOnly = True
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
        OnClick = MyProjects_LVClick
      end
      inline Login_Frame1: TLogin_Frame
        Left = 0
        Top = 0
        Width = 570
        Height = 117
        Align = alTop
        TabOrder = 1
        Visible = False
        ExplicitWidth = 570
        ExplicitHeight = 117
        inherited Label1: TLabel
          Width = 67
          Height = 16
          ExplicitWidth = 67
          ExplicitHeight = 16
        end
        inherited Label2: TLabel
          Width = 60
          Height = 16
          ExplicitWidth = 60
          ExplicitHeight = 16
        end
        inherited Warning_L: TLabel
          Width = 48
          Height = 16
          ExplicitWidth = 48
          ExplicitHeight = 16
        end
        inherited Username_E: TEdit
          Height = 24
          ExplicitHeight = 24
        end
        inherited Password_E: TEdit
          Height = 24
          ExplicitHeight = 24
        end
        inherited Login_B: TButton
          OnClick = Login_Frame1Login_BClick
        end
      end
      object Notifications_LV: TListView
        Left = 0
        Top = 117
        Width = 570
        Height = 165
        Align = alClient
        Columns = <
          item
            AutoSize = True
            Caption = 'Message'
          end
          item
            Caption = 'Date-time'
            Width = 150
          end
          item
            Caption = 'Sender'
            Width = 100
          end>
        ColumnClick = False
        HotTrackStyles = [htHandPoint]
        ReadOnly = True
        RowSelect = True
        TabOrder = 2
        ViewStyle = vsReport
        OnClick = Notifications_LVClick
      end
    end
    object Products_Feed_TS: TTabSheet
      Caption = 'Products feed'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object RSS_LV: TListView
        Left = 0
        Top = 0
        Width = 570
        Height = 382
        Align = alClient
        Columns = <
          item
            AutoSize = True
            Caption = 'New/modified product'
          end
          item
            AutoSize = True
            Caption = 'Date/time'
          end
          item
            AutoSize = True
            Caption = 'Description'
          end>
        ColumnClick = False
        HotTrackStyles = [htHandPoint]
        ReadOnly = True
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
        OnClick = RSS_LVClick
      end
    end
  end
  object Main_Timer: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = Main_TimerTimer
    Left = 512
    Top = 8
  end
  object TrayPopupMenu: TPopupMenu
    Left = 544
    Top = 16
    object RestoreA1: TMenuItem
      Action = Restore_A
      Default = True
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CloseA1: TMenuItem
      Action = Exit_A
    end
  end
  object ActionList1: TActionList
    Left = 456
    Top = 16
    object Restore_A: TAction
      Category = 'Tray'
      Caption = 'Show agent'
      OnExecute = Restore_AExecute
    end
    object Exit_A: TAction
      Category = 'File'
      Caption = 'Exit'
      OnExecute = Exit_AExecute
    end
    object About_A: TAction
      Category = 'Help'
      Caption = 'About'
      OnExecute = About_AExecute
    end
    object Logout_A: TAction
      Category = 'File'
      Caption = 'Logout'
      OnExecute = Logout_AExecute
    end
    object Full_Exit_A: TAction
      Category = 'File'
      Caption = 'Full_Exit_A'
      OnExecute = Full_Exit_AExecute
    end
    object Minimize_A: TAction
      Category = 'File'
      Caption = 'Minimize'
      OnExecute = Minimize_AExecute
    end
  end
  object MainMenu1: TMainMenu
    Left = 408
    Top = 8
    object File1: TMenuItem
      Caption = '&File'
      object Minimize1: TMenuItem
        Action = Minimize_A
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Logout1: TMenuItem
        Action = Logout_A
      end
      object FullExitA1: TMenuItem
        Action = Full_Exit_A
      end
    end
    object Help1: TMenuItem
      Caption = '&Help'
      object About1: TMenuItem
        Action = About_A
      end
    end
  end
end
