unit u_file_manager_VCS_log_frame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls,
  u_file_manager_VCS_Base, u_file_manager_VCS_const, StdCtrls, ExtCtrls;

type
  TVCS_Log_Frame = class(TFrame)
    LV: TListView;
    Splitter1: TSplitter;
    Panel1: TPanel;
    Panel2: TPanel;
    SaveAs_B: TButton;
    File_List_LB: TListBox;
    SaveDialog1: TSaveDialog;
    Comment_M: TMemo;
    Splitter2: TSplitter;
    procedure LVChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure SaveAs_BClick(Sender: TObject);
    procedure File_List_LBClick(Sender: TObject);
    procedure File_List_LBExit(Sender: TObject);
  private
    { Private declarations }
    fVCS_Wrapper : T_VCS_Wrapper_Base;
    fCommits_Log_A : T_Commits_Log_A;
    procedure Fill_LV;
    procedure Set_Save_Enabled;
    function File_For_Export_Selected( out Out_FN : string ) : boolean;
  public
    { Public declarations }
    procedure Init_Data( Arg_Commits_Log_A : T_Commits_Log_A; Arg_VCS_Wrapper : T_VCS_Wrapper_Base );
    procedure Highlight_Revision( Arg_Revision_id : t_revision_id_type );
  end;

implementation

uses U_Const, u_file_manager_VCS, u_vcs_Ini, U_ShellOp;

{$R *.dfm}

function TVCS_Log_Frame.File_For_Export_Selected;
var i : integer;
    s : string;
begin
  Out_FN := '';
  Result := False;

  i := File_List_LB.ItemIndex;
  if ( i >= 0 )
    then
      begin
        s := File_List_LB.Items[i];
        if ( s <> '' ) and ( s <> '/' )
          then
            begin
              Out_FN := s;
              Result := True;
            end;
      end;
end;

procedure TVCS_Log_Frame.File_List_LBClick(Sender: TObject);
begin
  Set_Save_Enabled;
end;

procedure TVCS_Log_Frame.File_List_LBExit(Sender: TObject);
begin
  Set_Save_Enabled;
end;

procedure TVCS_Log_Frame.Fill_LV;
var i : integer;
begin
  lv.Items.BeginUpdate;

  for i := 0 to high( fCommits_Log_A ) do
    with lv.Items.Add do
      begin
        Caption := inttostr( fCommits_Log_A[i].f_revision_id );
        SubItems.Add( fCommits_Log_A[i].f_author );
        SubItems.Add( fCommits_Log_A[i].f_datetime );
        SubItems.Add( fCommits_Log_A[i].f_comment );
        Data := pointer( fCommits_Log_A[i].f_revision_id );
      end;
  lv.Items.EndUpdate;      
end;

procedure TVCS_Log_Frame.Highlight_Revision;
var i : integer;
begin
  for i := 0 to high( fCommits_Log_A ) do
    if fCommits_Log_A[ i ].f_revision_id = Arg_Revision_id
      then
        begin
          LV.Items[ i ].Selected := True;
          break;                                  
        end;
end;

procedure TVCS_Log_Frame.Init_Data;
begin
  fCommits_Log_A := Arg_Commits_Log_A;
  fVCS_Wrapper   := Arg_VCS_Wrapper;

  Fill_LV;
  Set_Save_Enabled;
end;

procedure TVCS_Log_Frame.LVChange;
begin
  if LV.ItemIndex >=0
    then
      begin
        Comment_M.Lines.Text := fCommits_Log_A[ LV.ItemIndex ].f_comment;

        File_List_LB.Clear;
        File_List_LB.Items.Text := fCommits_Log_A[ LV.ItemIndex ].f_file_list;
        Set_Save_Enabled;
      end;
end;

procedure TVCS_Log_Frame.SaveAs_BClick(Sender: TObject);
var s, fn : string;
    rev : t_revision_id_type;
begin
  if File_For_Export_Selected( s )
    then
      begin
        fn := Extact_SVN_FileName( s );
        if s[1] = '/' then delete( s, 1, 1 );

        SaveDialog1.FileName := fn;
        if SaveDialog1.Execute
          then
            begin
              rev := StrToInt( lv.ItemFocused.Caption );

              if fVCS_Wrapper.Export_File_Revisioned( s, SaveDialog1.FileName, rev )
                then
                  begin
                    Show_in_Explorer( SaveDialog1.FileName );
                  end;
            end;
      end;
end;

procedure TVCS_Log_Frame.Set_Save_Enabled;
var s : string;
begin
  SaveAs_B.Enabled := File_For_Export_Selected( s );
end;

end.
