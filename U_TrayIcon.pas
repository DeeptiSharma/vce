unit U_TrayIcon;

(* ���������� ������������ TrayIcon
http://www.experts-exchange.com/Programming/Languages/Pascal/Delphi/Q_22812287.html
*)

interface

uses ExtCtrls, messages, Classes, ShellApi;

type TExtTrayIcon = class(TTrayIcon)
      private
        (* ��� ���������� ����� �� ��������� *)
        FOnBalloonClick: TNotifyEvent;
      protected
        procedure WindowProc(var Message: TMessage); override;
      public
      published
        property OnBalloonClick: TNotifyEvent read FOnBalloonClick write FOnBalloonClick;
  end;

implementation

{ TExtTrayIcon }

procedure TExtTrayIcon.WindowProc(var Message: TMessage);
begin
  case Message.Msg of
    WM_SYSTEM_TRAY_MESSAGE:
      begin
         case Message.lParam of
           NIN_BALLOONUSERCLICK:
             begin
               if Assigned(FOnBalloonClick)
                then
                  FOnBalloonClick(Self);
             end;
         else
           inherited;
         end;
      end;
    else
      Inherited;
  end;
end;

end.
