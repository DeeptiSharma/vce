(* RSS-Reader *)
unit U_RSSReader;

interface

uses XMLDoc, Classes,
     U_Net_Const;

type T_RSS_Record = record
       fTitle, fDesc, fLink : string;
       fDT : string;
      end;
     T_RSS_Array = array of T_RSS_Record;

type T_RSS_Reader = class(TComponent)
       private
         fXMLDocument: TXMLDocument;
         fFeedURL : string;
         fRSS_Array : T_RSS_Array;
         (* ������ � ����-�������� ��������� �������� rss *)
         f_last_pub_date_str : string;

         (* �������� ������ *)
         procedure RSS_Array_Set_Nil;
         (* �������� ������ � ������ *)
         function RSS_Array_Inc : integer;
       public
         property FeedURL : string read fFeedURL;

         (* Arg_FeedURL - URL ��� RSS-������ *)
         constructor Create( Arg_FeedURL : string ); reintroduce; virtual;

         (* ��������. ��������� ������. *)
         function Refresh : T_Net_Call_Results;

         function Get_RSS_Array : T_RSS_Array;

         (* ������� url �� ������ �� ������ *)
         function Get_Url( Arg_IDX : integer ) : string;

         destructor Destroy; override;
      end;

implementation

uses dialogs, sysutils,


     xmldom, XMLIntf, msxmldom, U_Const;

{ T_RSS_Reader }

constructor T_RSS_Reader.Create;
begin
  fXMLDocument := TXMLDocument.Create( Self );
  fFeedURL := Arg_FeedURL;
  fXMLDocument.FileName := fFeedURL;
  RSS_Array_Set_Nil;
  f_last_pub_date_str := '';

  inherited Create( nil );
end;

destructor T_RSS_Reader.Destroy;
begin
  fXMLDocument.Free;
  fFeedURL := '';
  RSS_Array_Set_Nil;

  inherited;
end;

function T_RSS_Reader.Get_RSS_Array: T_RSS_Array;
begin
  Result := fRSS_Array;
end;

function T_RSS_Reader.Get_Url;
begin
  Result := '';
  try
    if Arg_IDX in [0..length(fRSS_Array)-1]
      then
        Result := fRSS_Array[Arg_IDX].fLink;
  except
  end;
end;

function T_RSS_Reader.Refresh;
var StartItemNode : IXMLNode;
    ANode : IXMLNode;
    i : integer;
    (* ���� ���������� �������� rss-������ �� ������ ��� ����������� xml *)
    current_pub_date_str : string;
begin
  try
    fXMLDocument.Active := True;

    //fXMLDocument.XML.SaveToFile( GetNowAsFileName + '.xml' );

    StartItemNode := fXMLDocument.DocumentElement.ChildNodes.First.ChildNodes.FindNode('item');
    ANode := StartItemNode;
    if ANode = nil
      then
        current_pub_date_str := ''
      else
        current_pub_date_str := ANode.ChildNodes['pubDate'].Text;
    if current_pub_date_str <> f_last_pub_date_str
      then
        begin
          (* ������� ����� ����� - �.�. ���� ���������� �������� �������� ���������� �� ����� ����������� *)
          f_last_pub_date_str := current_pub_date_str;

          RSS_Array_Set_Nil;
          repeat
            i := RSS_Array_Inc;
            fRSS_Array[ i ].fTitle := ANode.ChildNodes['title'].Text;
            fRSS_Array[ i ].fDT := ANode.ChildNodes['pubDate'].Text;
            fRSS_Array[ i ].fLink := ANode.ChildNodes['link'].Text;
            fRSS_Array[ i ].fDesc := ANode.ChildNodes['description'].Text;
            ANode := ANode.NextSibling;
          until ANode = nil;
          Result := nc_res_NewData;

        end
      else
        Result := nc_res_DataNoChange;
  except
    Result := nc_res_Fail;
  end;
  if fXMLDocument.Active
    then
      fXMLDocument.Active := False;  
end;

function T_RSS_Reader.RSS_Array_Inc;
var i : integer;
begin
  i := Length( fRSS_Array );
  inc( i );
  SetLength( fRSS_Array, i );
  Result := i - 1;
end;

procedure T_RSS_Reader.RSS_Array_Set_Nil;
begin
  SetLength( fRSS_Array, 0 );
end;

end.
