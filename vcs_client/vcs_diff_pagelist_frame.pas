unit vcs_diff_pagelist_frame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  StdCtrls, u_multi_page_const, ComCtrls;

type T_OnPage_Change = procedure( const Arg_Page_IDX : integer ) of object;

type
  TDiff_Pagelist_Frame = class(TFrame)
    TabControl1: TTabControl;
    procedure Page_ListBoxClick(Sender: TObject);
  private
    { Private declarations }
    fSelected_Page : integer;
    f_Page_List_A : T_Page_List_A;
  public
    { Public declarations }
    OnPage_Change : T_OnPage_Change;
    procedure Init_Pagelist( Arg_Page_List_A : T_Page_List_A );
    procedure Show_Page( Arg_Page_IDX : integer );
    procedure Show_Page_by_ID( Arg_Page_ID : integer );
  end;

implementation

{$R *.dfm}

{ TDiff_Pagelist_Frame }

procedure TDiff_Pagelist_Frame.Init_Pagelist;
var i : integer;
begin
  Visible := True;

  for i := 0 to high( Arg_Page_List_A ) do
    TabControl1.Tabs.Add( Arg_Page_List_A[i].Caption );

  fSelected_Page := 0;
  f_Page_List_A := Arg_Page_List_A;
  TabControl1.TabIndex := 0;
end;

procedure TDiff_Pagelist_Frame.Page_ListBoxClick(Sender: TObject);
begin
  if fSelected_Page <> TabControl1.TabIndex
    then
      begin
        fSelected_Page := TabControl1.TabIndex;
        if Assigned( OnPage_Change )
          then
            OnPage_Change( TabControl1.TabIndex );
      end;
end;

procedure TDiff_Pagelist_Frame.Show_Page(Arg_Page_IDX: integer);
begin
  if ( TabControl1.TabIndex <> Arg_Page_IDX ) and ( Arg_Page_IDX >= 0 ) and ( Arg_Page_IDX < TabControl1.Tabs.Count )
    then
      begin
        TabControl1.TabIndex := Arg_Page_IDX;
        Page_ListBoxClick( self );
      end;
end;

procedure TDiff_Pagelist_Frame.Show_Page_by_ID( Arg_Page_ID : integer );
var i : integer;
begin
  for i := 0 to high( f_Page_List_A ) do
    if f_Page_List_A[ i ].ID = Arg_Page_ID
      then
        Show_Page( i );
end;

end.
