(* ������ � ������ portable *)
unit u_vcs_portable_const;

interface

var Portable_Mode : boolean;

    Ini_File_Path : string;

procedure Prepare_Paths;

implementation

uses Sysutils,
     U_Const, U_ShellOp, u_vcs_Ini;

procedure Prepare_Paths;
var Cool_INI, Base_INI, current_ini : string;
begin
  (* ������ ���� ����� *)

  (* ���������, portable �� ������ *)
  (* portable - ������� ��������� � ����� exe *)
  Base_INI := Appl_Path + Get_ini_file_name;

{  ����� �������� ������� �� ������
  ���� � ����� exe ������� ��� - ������������� � ����� ��-�����������

  ���� ������� ��� � ���� - ��������� ����������

  � ������ - ���� �������� �������
  - ���������� ��������� ������� ����� � ����� exe
  - ��� ����� ������������ exe ����������� ��������� ����� � ������? ���?
}
  (* ����������, portable �� ���� ������� *)



  {$message warn '������ - ��, ��� ��� ������� � ���������� Data - �������� �� ��� ��� �� �������� portable �����? � ���� �� ��������� - ��� �� ���� �������� �����?'}

  Portable_Mode := False;
  if FileExists( Base_INI )
      and
     Test_WriteAccess_INI( Base_INI )
    then
      begin
        (* ���� ��� ���� � ����� � exe. ������������, ��� ��� ������� portable *)
        Ini_File_Path := Base_INI;
        AppData_Path := Test_Appl_Path_Data_Folder_Write_Access;
        Portable_Mode := True;
        if AppData_Path = ''
          then
            AppData_Path := AppData_Folder + Slash + AppData_SubFolderName + Slash;
      end
    else
      begin
        Portable_Mode := False;
        (* ������������ MS - ������� ��������� � ����� ������������ *)
        AppData_Path := AppData_Folder + Slash + AppData_SubFolderName + Slash;
        Cool_INI := AppData_Path + Get_ini_file_name;
        Ini_File_Path := Cool_INI;
      end;
    
  if not DirectoryExists( AppData_Path )
    then
      begin (* ���� �� Force *)
        if ForceDirectories( AppData_Path )
          then
          else
            begin
              (* Show_Err( Err( err_Cant_Write_On_Disk ) );
              Application.Terminate;
              Exit; *)
            end;
      end;
end;

end.
