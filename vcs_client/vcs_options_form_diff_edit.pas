unit vcs_options_form_diff_edit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,
  u_vcs_diff_options_const, StdCtrls;

type
  TOption_Edit_Diff_F = class(TForm)
    File_Ext_E: TEdit;
    Ok_B: TButton;
    Cancel_B: TButton;
    Select_B: TButton;
    File_Ext_L: TLabel;
    External_Diff_Path_L: TLabel;
    Clear_B: TButton;
    Test_Text_B: TButton;
    Check_Extension_L: TLabel;
    External_Diff_Path_CB: TComboBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Select_BClick(Sender: TObject);
    procedure External_Diff_Path_CBChange(Sender: TObject);
    procedure File_Ext_EChange(Sender: TObject);
    procedure Clear_BClick(Sender: TObject);
    procedure Test_Text_BClick(Sender: TObject);
  private
    { Private declarations }
    fModified : boolean; (* ���� ���-�� ������� *)
    fExtension_Correct : boolean; (* ���������� ��������� *)
    f_List_Of_Known_Ext : string;
    oroginal_Diff_Action_desc : T_Diff_Action_desc;
    procedure Set_Enabled;
    procedure Check_Extension_Correct;
  public
    { Public declarations }
  end;

function Show_Diff_Option_Edit( var Arg_Diff_Action_desc : T_Diff_Action_desc; Arg_List_Of_Tools : string; Arg_List_Of_Ext : string ) : boolean;

implementation

uses u_vcs_diff_wrapper, U_ShellOp;

{$R *.dfm}

function Show_Diff_Option_Edit;
begin
  Result := False;

  with TOption_Edit_Diff_F.Create( Application ) do
    begin
      File_Ext_E.Text := Arg_Diff_Action_desc.f_extention;
      External_Diff_Path_CB.Text := Arg_Diff_Action_desc.f_external_tool_path;
      External_Diff_Path_CB.Items.Text := Arg_List_Of_Tools;
      oroginal_Diff_Action_desc := Arg_Diff_Action_desc;

      f_List_Of_Known_Ext := Arg_List_Of_Ext;

      fModified := False;
      Check_Extension_Correct;
      
      Set_Enabled;
      if ShowModal = idOk
        then
          begin
            Arg_Diff_Action_desc.f_extention := File_Ext_E.Text;
            Arg_Diff_Action_desc.f_external_tool_path := External_Diff_Path_CB.Text;
            Result := True;
          end;
    end;
end;

procedure TOption_Edit_Diff_F.Test_Text_BClick(Sender: TObject);
begin
  if External_Diff_Path_CB.Text <> ''
    then
      Test_External_Diff( External_Diff_Path_CB.Text );
end;

procedure TOption_Edit_Diff_F.Check_Extension_Correct;
var ext : string;
begin
  fExtension_Correct := False;
  ext := File_Ext_E.Text;

  if ext = ''
    then
      begin
        Check_Extension_L.Caption := 'Extension can''t be empty';
      end
    else
      begin
        if Find_Sub_Char( ext, MSWindows_File_Restricted_Chars )
          then
            Check_Extension_L.Caption := 'Extension can''t contain the Windows restricted chars: ' + MSWindows_File_Restricted_Chars
          else
            begin
              (* ���������, ��� ������ ��� �� ���� *)
              if ( oroginal_Diff_Action_desc.f_extention <> ext )
                 and
                 ( pos( Ext_List_Delimiter + ext + Ext_List_Delimiter, f_List_Of_Known_Ext ) > 0 )
                then
                  Check_Extension_L.Caption := 'This extension is already in the list'
                else
                  begin
                    Check_Extension_L.Caption := 'OK';
                    fExtension_Correct := True;
                  end;
            end;
      end;
end;

procedure TOption_Edit_Diff_F.Clear_BClick(Sender: TObject);
begin
  if External_Diff_Path_CB.Text <> ''
    then
      External_Diff_Path_CB.Clear; (* ��� �� ��� �� ������� ����������� ������ *)

  fModified := True;
  Set_Enabled;
end;

procedure TOption_Edit_Diff_F.External_Diff_Path_CBChange(Sender: TObject);
begin
  fModified := True;
  Set_Enabled;
end;

procedure TOption_Edit_Diff_F.File_Ext_EChange(Sender: TObject);
begin
  fModified := True;
  Check_Extension_Correct;
  Set_Enabled;
end;

procedure TOption_Edit_Diff_F.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TOption_Edit_Diff_F.Select_BClick(Sender: TObject);
var s : string;
begin
  s := Select_Util_OpenDialog( External_Diff_Path_CB.Text );
  if s <> ''
    then
      begin
        External_Diff_Path_CB.Text := s;
        fModified := True;
        Set_Enabled;
      end;
end;

procedure TOption_Edit_Diff_F.Set_Enabled;
begin
  Ok_B.Enabled := fModified and fExtension_Correct;
  Test_Text_B.Enabled := External_Diff_Path_CB.Text <> '';
end;

end.
