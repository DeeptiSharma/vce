unit vcs_password_form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TCredential_F = class(TForm)
    GridPanel1: TGridPanel;
    Label1: TLabel;
    User_Name_E: TEdit;
    Label2: TLabel;
    Password_E: TEdit;
    Ok_B: TButton;
    Cancel_B: TButton;
    Panel1: TPanel;
    procedure User_Name_EChange(Sender: TObject);
    procedure Password_EChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    fOld_User_Name, fOld_PW : string;
    function Something_Changed_And_Correct : boolean;
    procedure Set_Command_Enabled;
  public
    { Public declarations }
  end;

function Show_Credentials( Arg_VCS_URL : string ; var Arg_User_Name, Arg_Password : string ) : boolean;

implementation

uses u_vcs_Ini, U_Msg;

{$R *.dfm}

function Show_Credentials;
begin
  Result := False;

  with TCredential_F.Create( Application ) do
    begin
      Caption := 'Credentials for server URL: ' + Arg_VCS_URL;
      Ini_Strore.Set_Font( Font );

      User_Name_E.Text := Arg_User_Name;
      Password_E.Text := Arg_Password;
      fOld_User_Name  := Arg_User_Name;
      fOld_PW         := Arg_Password;
      Set_Command_Enabled;

      if ShowModal = mrOk
        then
          begin
            Arg_User_Name := User_Name_E.Text;
            Arg_Password := Password_E.Text;
            Result := True;
          end;
      Free;
    end;
end;

{ TCredential_F }

procedure TCredential_F.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := True;

  if ( ModalResult = mrCancel )
      and
     ( Something_Changed_And_Correct )
    then
      begin
        case Show_YNC( cap( cap_Save_Changes_q ) ) of
          ID_YES    : ModalResult := mrOk;
          ID_NO     : ;
          ID_CANCEL : CanClose := False;
         end;
      end;
end;

procedure TCredential_F.Password_EChange(Sender: TObject);
begin
  Set_Command_Enabled;
end;

procedure TCredential_F.Set_Command_Enabled;
begin
  (* �� �������� ������ � ������ ��������� ������, �� ����������� �������� "����� ������, �� ��� ������" *)
  Ok_B.Enabled := Something_Changed_And_Correct;
end;

function TCredential_F.Something_Changed_And_Correct;
begin
  Result := ( not ( ( Password_E.Text <> '' ) and ( User_Name_E.Text = '' ) ) )
              and
            ( ( Password_E.Text <> fOld_PW ) or ( User_Name_E.Text <> fOld_User_Name ) );
end;

procedure TCredential_F.User_Name_EChange(Sender: TObject);
begin
  Set_Command_Enabled;
end;

end.
