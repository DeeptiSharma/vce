unit u_vcs_start_up_master_f;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList;

type
  TStartUp_Master_Form = class(TForm)
    Label1: TLabel;
    Goto_Options_B: TButton;
    Connect2demo_github_B: TButton;
    Do_Not_Show_Again_CB: TCheckBox;
    Close_B: TButton;
    GitHub_L: TLabel;
    Open_Web_Site_B: TButton;
    Where2Host_Page_B: TButton;
    Add_New_Checkout_B: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Close_BClick(Sender: TObject);
    procedure Goto_Options_BClick(Sender: TObject);
    procedure Connect2demo_github_BClick(Sender: TObject);
    procedure Open_Web_Site_BClick(Sender: TObject);
    procedure Where2Host_Page_BClick(Sender: TObject);
    procedure Add_New_Checkout_BClick(Sender: TObject);
  private
    { Private declarations }
    fAdd_Project_A,
    fOptions_A, // fCreate_Demo_Repo_And_Add_A
    fConnect2demo_github_A
     : TAction;
  public
    { Public declarations }
  end;

procedure Show_StartUp_Master( Arg_Add_Project_A, Arg_Options_A, Arg_Connect2demo_github_A : TAction );

implementation

uses u_vcs_Ini, U_Msg, U_URLs;

{$R *.dfm}

procedure Show_StartUp_Master;
begin
  with TStartUp_Master_Form.Create( Application ) do
    begin
      fAdd_Project_A := Arg_Add_Project_A;
      fOptions_A := Arg_Options_A;
      //fCreate_Demo_Repo_And_Add_A := Arg_Create_Demo_Repo_And_Add_A;

      GitHub_L.Caption :=
      'GitHub provide the free online GIT repositories with SVN bridge.' + ret +
      'You can connect to our demo project for overview of VCE functionality.' + ret +
      'Internet connection is needed!' + ret +
      'Project will be read-only (you can change it local, but can''t commit to GitHub).';
      fConnect2demo_github_A := Arg_Connect2demo_github_A;
      Show;
    end;
end;

procedure TStartUp_Master_Form.Where2Host_Page_BClick(Sender: TObject);
begin
  Open_Where2Host_Page;
end;

procedure TStartUp_Master_Form.Add_New_Checkout_BClick(Sender: TObject);
begin
  close;

  if Assigned( fAdd_Project_A )
    then
      fAdd_Project_A.Execute;
end;

procedure TStartUp_Master_Form.Close_BClick(Sender: TObject);
begin
  if Do_Not_Show_Again_CB.Checked
    then
      Ini_Strore.Boo[ ii_DoNot_Show_StartUp ] := True;

  Close;
end;

procedure TStartUp_Master_Form.Connect2demo_github_BClick(
  Sender: TObject);
begin
  close;

  if Assigned( fConnect2demo_github_A )
    then
      fConnect2demo_github_A.Execute;
end;

procedure TStartUp_Master_Form.FormClose;
begin
  Action := caFree;
end;

procedure TStartUp_Master_Form.Goto_Options_BClick(Sender: TObject);
begin
  close;

  if Assigned( fOptions_A )
    then
      fOptions_A.Execute;
end;

procedure TStartUp_Master_Form.Open_Web_Site_BClick(Sender: TObject);
begin
  OpenMainSite;
end;

end.
