unit vcs_diff_form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, u_vcs_diff_wrapper;

type
  TDiff_F = class(TForm)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    f_Use_Ini : boolean;
    procedure fDiff_Close;
  public
    { Public declarations }
  end;

procedure Show_Diff( Arg_Use_Ini_Flag : boolean; (* ���� False - �� ������ �� ������ �� ini *)
                     Arg_L, Arg_L_Caption, Arg_R, Arg_R_Caption : string;
                     Arg_Image_Convert : T_Convertable_Formats = cf_None;
                     Arg_Multipage : T_Multipage_Formats = mf_None );

implementation

uses vcs_diff_frame, u_vcs_Ini, vcs_view_image_const;

{$R *.dfm}

procedure Show_Diff;
var Diff_F : TDiff_F;
begin
  Diff_F := TDiff_F.Create( Application );

  with Diff_F do
    begin
      f_Use_Ini := Arg_Use_Ini_Flag;
      
      if f_Use_Ini
        then
          Ini_Strore.Set_Font( Font );

      with TView_diff_frame.Create( Diff_F, Arg_L, Arg_L_Caption, Arg_R, Arg_R_Caption, fDiff_Close, Arg_Image_Convert, Arg_Multipage ) do
        begin
          Align := alClient;
          if View_Mode = vm_None
            then
              begin
                Close;
              end
            else
              begin
                if f_Use_Ini
                  then
                    Ini_Strore.Load_Form_Sizes( Diff_F )
                  else
                    WindowState := wsMaximized;
                ShowModal;
              end;
        end;
    end;
end;

procedure TDiff_F.fDiff_Close;
begin
  Close;
end;

procedure TDiff_F.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if f_Use_Ini and Visible
    then
      Ini_Strore.Save_Form_Sizes( Self );

  Action := caFree;
end;

end.
