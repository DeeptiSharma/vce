unit u_vcs_diff_wrapper;

interface

uses u_vcs_diff_options_const;

(* ���������� ������� - ��� ������������� diff *)
type T_Diff_Viewer_Kind = ( dvk_MSWord,
                            dvk_Multipage_Convert,
                            dvk_Internal_Image,
                            dvk_Internal_Image_Convert,
                            dvk_External,

                            dvk_None (* ���������� �� ��������, ������� ��� ������� ���� ���� ������� ��������� - �� �������� *)
                            );

(* �������, ��� ������� ��������� ����������� *)
type T_Convertable_Formats = ( cf_None, cf_SVG, cf_Jpg2000, cf_PPM, cf_EPS );
(* ���������� ��������, ��� ������� ��������� ����������� *)

const convertable_format_ext_arr : array[1..5] of record Convertable_Formats : T_Convertable_Formats ; extention : string end =
 (
   ( Convertable_Formats : cf_SVG;
     extention : 'svg';
    ),

   ( Convertable_Formats : cf_Jpg2000;
     extention : 'j2k';
    ),
   ( Convertable_Formats : cf_Jpg2000;
     extention : 'jp2';
    ),
   ( Convertable_Formats : cf_PPM;
     extention : 'ppm';
    ),
   ( Convertable_Formats : cf_EPS;
     extention : 'eps';
    )

  );

(* �������� ��� �������������� ��������� ������� ��� ImageMagick  *)
(* http://stackoverflow.com/questions/33305416/how-to-define-the-input-image-format-for-imagemagick-convert *)
const imagemagick_format_prefix : array[T_Convertable_Formats] of string = ( '', 'svg', 'j2k', 'ppm', 'eps' );

(* �������, ��� ������� ��������� ��������� ����������������� *)
type T_Multipage_Formats = ( mf_None, mf_MSVisio, mf_Kompas );

(* ��������� ���� - ������������ visio ��� ��������� dxf *)
const DXF_by_MSVisio = true;

function Check_Diff_Viewer_Kind( Arg_FN : string;
                                 out Out_Convertable_Formats : T_Convertable_Formats;
                                 out Out_Multipage_Formats : T_Multipage_Formats;
                                 out Out_Diff_Action_desc : T_Diff_Action_desc // ������ � ������� �����������
                                  ) : T_Diff_Viewer_Kind;

function Test_External_Diff( Arg_Exe : string ) : boolean;

(* ������� ���������� ����� ��� ������������ ������������� �� �������� ����������� *)
procedure Protect_File( var Arg_Protected_File_Name : string; Arg_Name : string );

type t_rediff_result = ( rediff_Ok, rediff_NotConfigured, rediff_NotFound, rediff_NoDiff3, rediff_Fail );

(* Arg_Protect_N_with_Name - ���� �� �����, �� ���� ���������� (���������� � temp) � ��������� ������ *)
function Run_External_Diff( const Arg_Diff_Action_desc : T_Diff_Action_desc;
                            Arg_File1 : string; Arg_Protect_1_with_Name : string;
                            Arg_File2 : string; Arg_Protect_2_with_Name : string;
                            Arg_File3 : string = ''; Arg_Protect_3_with_Name : string = ''
                          ) : t_rediff_result;

procedure Run_MSWord_Compare( Arg_Left_File, Arg_Right_File : string );

procedure Show_No_Diff;

procedure Show_Diff_Not_Found( Arg_Exe : string );

var v_Show_Diff_Options_Event : procedure;

implementation

uses SysUtils, Classes, Forms, windows,
     GraphicEx,
     U_ShellOp, U_Msg, u_vcs_Ini, U_Const, u_ms_word_compare;

function Check_Diff_Viewer_Kind;
var ext : string;
    List: TStringList;
    i : integer;
begin
  Result := dvk_None;
  (* ������������. �������� ������ ����������� ������ ���� Result = dvk_Internal_Image_Convert *)
  Out_Convertable_Formats := cf_None;
  (* ������������. �������� ������ ����������� ������ ���� Result = dvk_Multipage_Convert *)
  Out_Multipage_Formats := mf_None;

  {$i test.inc}
  {$ifdef disable_internal_diff}
  Exit;
  {$endif}  

  ext := LowerCase( ExtractFileExt( Arg_FN ) );

  if ext = ''
    then
      begin
        Exit;
      end
    else
      begin
        if ext[1] = '.' then Delete( ext, 1, 1 );
        (* �.�. �������� �����, ���� ���������, ��� �������� *)
        if ext <> ''
          then
            begin
              if ( ext = 'doc' )
                  or
                 ( ext = 'docx' )
                then
                  Result := dvk_MSWord
                else
                  begin
                    if ( ext = 'vsd' ) // �������� Visio
                        or
                       ( ext = 'vdw' ) // ��� �������� Visio
                        or
                       ( ext = 'vdx' ) // XML �������� Visio
                      then
                        begin
                          Result := dvk_Multipage_Convert;
                          Out_Multipage_Formats := mf_MSVisio;
                        end
                      else
                        begin
                          (* ������� ������ Dwg Dxf ����� Visio *)
                          if ( ( ext = 'dwg' ) or ( ext = 'dxf' ) ) and ( DXF_by_MSVisio )
                            then
                              begin
                                Result := dvk_Multipage_Convert;
                                Out_Multipage_Formats := mf_MSVisio;
                              end
                            else
                              begin
                                if ( ( ext = 'cdw' ) ) (* ������� ������ *)
                                  then
                                    begin
                                      Result := dvk_Multipage_Convert;
                                      Out_Multipage_Formats := mf_Kompas;

                                    end
                                  else
                                    begin
                                      List := TStringList.Create;

                                      FileFormatList.GetExtensionList( List );

                                      if List.IndexOf( ext ) >= 0
                                        then
                                          Result := dvk_Internal_Image
                                        else
                                          begin
                                            (* ���������� ����� �� �����. �����, ������� �����������? *)
                                            for i := low( convertable_format_ext_arr ) to high( convertable_format_ext_arr) do
                                              begin
                                                if ext = convertable_format_ext_arr[i].extention
                                                  then
                                                    begin
                                                      Result := dvk_Internal_Image_Convert;
                                                      Out_Convertable_Formats := convertable_format_ext_arr[i].Convertable_Formats;
                                                      break;
                                                    end;
                                              end;
                                          end;

                                      List.free;
                                    end;
                              end;
                        end;
                  end;
               (* ���������� �� ������� *)
               if ( Result = dvk_None ) and Assigned( Ini_Strore )
                 then
                   begin
                     for i := 0 to high( Ini_Strore.f_Diff_Action_arr ) do
                       begin
                         if ( ext = LowerCase( Ini_Strore.f_Diff_Action_arr[i].f_extention ) ) and ( Ini_Strore.f_Diff_Action_arr[i].f_external_tool_path <> '' )
                           then
                             begin
                               (* ������ ������������ ���������� ��� ������� ���������� *)
                               Out_Diff_Action_desc := Ini_Strore.f_Diff_Action_arr[i];
                               Result := dvk_External;
                               break;
                             end;
                       end;
                     (* ������������ ������������ ��� � �� ������� *)
                     if Result = dvk_None
                       then
                         begin
                           (* ����� ���� ���������? *)
                           if Ini_Strore.Str[ ii_External_Diff_Exe ] <> ''
                             then
                               begin
                                 Result := dvk_External;
                                 Out_Diff_Action_desc.f_external_tool_path := Ini_Strore.Str[ ii_External_Diff_Exe ];
                                 if Ini_Strore.Boo[ ii_External_Diff3_Flag ]
                                   then
                                     Out_Diff_Action_desc.f_diff3_flag := True;
                               end;
                         end;
                   end;
            end;
      end;
end;

function Test_External_Diff;
var sl : tstringlist;
    lf, rf : string;
    i : integer;
begin
  Result := False;
  
  if FileExists( Arg_Exe )
    then
      begin
        //ExecNewProcess( Arg_Exe + ' "' + readme_full_path + '" "' + readme_full_path + '"', False, False )
        (* ������������� 2 ���� ������ ��������� ����� *)
        sl := tstringlist.Create;

        sl.Text := 'Temp text file for diff tesing' + ret + ret;

        for i := 0 to 25 do
          sl.Add( chr( 65 + i ) );

        Randomize();

        lf := Get_Rnd_Temp_File_Name( True ) + '__left_base.txt';
        sl.SaveToFile( lf );
        sl.Delete( 3 );
        sl.Delete( 7 );
        sl.Insert( 11, 'new line' );
        sl[18] := sl[18] + ' new text';

        rf := Get_Rnd_Temp_File_Name( True ) + '__right_modified.txt';
        sl.SaveToFile( rf );

        sl.Free;

        ExecNewProcess( Arg_Exe + ' "' + lf + '" "' + rf + '"', False, False );
        Result := True;
      end
    else
      ShowMsg( cap( cap_Diff_Tool_Not_Found ) );
end;

procedure Protect_File;
var temp_fn : string;
    dwFileAttributes: DWORD;
begin
  temp_fn := Temp_Path + Arg_Name;
  if Copy_File( Arg_Protected_File_Name, temp_fn )
    then
      begin
        dwFileAttributes := FILE_ATTRIBUTE_ARCHIVE + FILE_ATTRIBUTE_READONLY;
        SetFileAttributes( pansichar( temp_fn ), dwFileAttributes );
        Arg_Protected_File_Name := temp_fn;
      end;
end;

function Run_External_Diff;
var diff_exe, s : string;
begin
  diff_exe := Arg_Diff_Action_desc.f_external_tool_path;

  if diff_exe = ''
    then
      begin
        Result := rediff_NotConfigured
      end
    else
      begin
        if ( FileExists( diff_exe ) )
          then
            begin
              (* �������� - �.�. �������� ����� �� ��������� ����� *)
              if Arg_Protect_1_with_Name <> ''
                then
                  Protect_File( Arg_File1, Arg_Protect_1_with_Name );
              if Arg_Protect_2_with_Name <> ''
                then
                  Protect_File( Arg_File2, Arg_Protect_2_with_Name );
              if Arg_Protect_3_with_Name <> ''
                then
                  Protect_File( Arg_File3, Arg_Protect_3_with_Name );


              s := '"' + diff_exe + '" ' +
                   '"' + Arg_File1 + '" ' +
                   '"'+ Arg_File2 + '"';

              if Arg_File3 <> ''
                then
                  begin
                    if Arg_Diff_Action_desc.f_diff3_flag 
                      then
                        s := s + ' "' + Arg_File3 + '"'
                      else
                        begin
                          //ShowMsg( cap( cap_No_Diff3_Tool_Configured ) + ret + cap( cap_Use_Options ) );
                          Result := rediff_NoDiff3;
                          exit;
                        end;  
                  end;

              if ExecNewProcess( s, False, False )
                then
                  Result := rediff_Ok
                else
                  Result := rediff_Fail;
            end
          else
            begin
              Result := rediff_NotFound;
            end;
      end;
end;

procedure Run_MSWord_Compare;
var OLE_Error : string;
begin
  Protect_File( Arg_Left_File, ExtractFileName( Arg_Right_File ) );

  case MS_Word_Compare( Arg_Left_File, Arg_Right_File, OLE_Error ) of
    tmswcr_None                    : ;
    tmswcr_LF_Not_Found            : ShowMsg( 'Left ' + Arg_Left_File + ' file not found' );
    tmswcr_RF_Not_Found            : ShowMsg( 'Right ' + Arg_Right_File + ' file not found' );
    tmswcr_OLE_MSWord_Not_Found    : ShowMsg( cap( cap_MSWord_Not_Found ) );
    tmswcr_OK                      : ;//ShowMessage( 'Ok' );
    tmswcr_LF_CantOpen, tmswcr_RF_CantOpen : ShowMsg( cap( cap_Cant_Open_File) + ret + OLE_Error );
    tmswcr_OLE_MSWord_Compare_Fail : ShowMsg( 'MS Word can''t compare file!' + ret + OLE_Error );
  end;
end;

procedure Show_No_Diff;
begin
  if Show_Q( cap( cap_No_Diff_Tool_Configured ) + ret + cap( cap_Open_Diff_Options_Confirm ), True )
    then
      v_Show_Diff_Options_Event;
end;

procedure Show_Diff_Not_Found( Arg_Exe : string );
begin
  if Show_Q( cap( cap_Diff_Tool_Not_Found ) + ret + ret + Arg_Exe + ret + ret + cap( cap_Open_Diff_Options_Confirm ), True )
    then
      v_Show_Diff_Options_Event;
end;

end.
