object VCS_Resolver_f: TVCS_Resolver_f
  Left = 0
  Top = 0
  Caption = 'Resolver'
  ClientHeight = 573
  ClientWidth = 592
  Color = clBtnFace
  Constraints.MinHeight = 600
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 532
    Width = 592
    Height = 41
    Align = alBottom
    TabOrder = 0
    object Cancel_B: TButton
      Left = 24
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 0
    end
    object Diff_B: TButton
      Left = 159
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Diff'
      Enabled = False
      TabOrder = 1
      OnClick = Diff_BClick
    end
    object Diff3_B: TButton
      Left = 240
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Diff3'
      Enabled = False
      TabOrder = 2
      OnClick = Diff3_BClick
    end
    object Diff_Options_B: TButton
      Left = 321
      Top = 6
      Width = 258
      Height = 25
      Caption = 'Diff_Options_B'
      TabOrder = 3
      Visible = False
      OnClick = Diff_Options_BClick
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 592
    Height = 532
    ActivePage = Conflict_Info_TS
    Align = alClient
    TabOrder = 1
    object Conflict_Info_TS: TTabSheet
      Caption = 'Conflict info'
      object Log_P: TPanel
        Left = -96
        Top = -129
        Width = 377
        Height = 294
        TabOrder = 0
        inline VCS_Log_Frame1: TVCS_Log_Frame
          Left = 1
          Top = 41
          Width = 375
          Height = 252
          Align = alClient
          Constraints.MinHeight = 240
          Constraints.MinWidth = 320
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitLeft = 1
          ExplicitTop = 41
          ExplicitWidth = 375
          ExplicitHeight = 252
          inherited Splitter1: TSplitter
            Top = 134
            Width = 375
            ExplicitTop = 142
            ExplicitWidth = 375
          end
          inherited LV: TListView
            Width = 375
            Height = 134
            ExplicitWidth = 375
            ExplicitHeight = 134
          end
          inherited Panel1: TPanel
            Top = 137
            Width = 375
            ExplicitTop = 137
            ExplicitWidth = 375
            inherited Splitter2: TSplitter
              Left = 72
              ExplicitLeft = 72
            end
            inherited Panel2: TPanel
              Left = 267
              ExplicitLeft = 267
            end
            inherited File_List_LB: TListBox
              Left = 75
              ExplicitLeft = 75
            end
            inherited Comment_M: TMemo
              Width = 71
              ExplicitWidth = 71
            end
          end
        end
        object Panel3: TPanel
          Left = 1
          Top = 1
          Width = 375
          Height = 40
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object File_Log_L: TLabel
            Left = 8
            Top = 20
            Width = 50
            Height = 13
            Caption = 'File_Log_L'
          end
        end
      end
    end
    object Internal_Image_Viewer_TS: TTabSheet
      Caption = 'Internal image viewer'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 484
      ExplicitHeight = 0
    end
  end
end
