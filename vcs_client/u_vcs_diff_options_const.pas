unit u_vcs_diff_options_const;

interface

(* �������� diff-�������� � ��������� ����� ������ *)
type T_Diff_Action_desc =
       record
         f_extention : string;
         f_external_tool_path : string;
         f_diff3_flag : boolean;
        end;

(* ������ �������� *)
type T_Diff_Action_arr = array of T_Diff_Action_desc;

function Select_Util_OpenDialog( Arg_fn : string ) : string;

(* ����������� � ������-������ ���������� *)
const Ext_List_Delimiter = '|';

implementation

uses Dialogs;

function Select_Util_OpenDialog;
var OpenDialog : TOpenDialog;
begin
  Result := '';
  OpenDialog := TOpenDialog.Create( nil );

  with OpenDialog do
    begin
      FileName := Arg_fn;
      Filter := 'Executable files|*.exe;*.com;*.bat;*.cmd|All files|*.*';
      Options := [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing];
      if Execute
        then
          Result := FileName;
      Free;
    end;
end;

end.
