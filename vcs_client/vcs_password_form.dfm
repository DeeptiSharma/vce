object Credential_F: TCredential_F
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'Credentials'
  ClientHeight = 133
  ClientWidth = 576
  Color = clBtnFace
  Constraints.MinHeight = 150
  Constraints.MinWidth = 300
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object GridPanel1: TGridPanel
    Left = 0
    Top = 0
    Width = 576
    Height = 92
    Align = alClient
    ColumnCollection = <
      item
        Value = 34.426229508196720000
      end
      item
        Value = 65.573770491803270000
      end>
    ControlCollection = <
      item
        Column = 0
        Control = Label1
        Row = 0
      end
      item
        Column = 1
        Control = User_Name_E
        Row = 0
      end
      item
        Column = 0
        Control = Label2
        Row = 1
      end
      item
        Column = 1
        Control = Password_E
        Row = 1
      end
      item
        Column = 0
        Control = Ok_B
        Row = 2
      end
      item
        Column = 1
        Control = Cancel_B
        Row = 2
      end>
    RowCollection = <
      item
        SizeStyle = ssAuto
        Value = 45.454545454545450000
      end
      item
        SizeStyle = ssAuto
        Value = 83.333333333333330000
      end
      item
        SizeStyle = ssAuto
        Value = 100.000000000000000000
      end>
    TabOrder = 0
    ExplicitLeft = 32
    ExplicitTop = 40
    ExplicitWidth = 641
    ExplicitHeight = 249
    DesignSize = (
      576
      92)
    object Label1: TLabel
      Left = 74
      Top = 5
      Width = 51
      Height = 13
      Anchors = []
      Caption = 'User name'
      ExplicitLeft = 138
      ExplicitTop = 56
    end
    object User_Name_E: TEdit
      Left = 198
      Top = 1
      Width = 377
      Height = 21
      Align = alClient
      TabOrder = 0
      OnChange = User_Name_EChange
      ExplicitLeft = 520
      ExplicitTop = 2
      ExplicitWidth = 222
    end
    object Label2: TLabel
      Left = 76
      Top = 26
      Width = 46
      Height = 13
      Anchors = []
      Caption = 'Password'
      ExplicitLeft = 140
      ExplicitTop = 179
    end
    object Password_E: TEdit
      Left = 198
      Top = 22
      Width = 377
      Height = 21
      Align = alClient
      PasswordChar = '*'
      TabOrder = 1
      OnChange = Password_EChange
      ExplicitLeft = 419
      ExplicitTop = 175
      ExplicitWidth = 222
    end
    object Ok_B: TButton
      Left = 18
      Top = 43
      Width = 162
      Height = 23
      Anchors = []
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 2
      ExplicitLeft = 64
    end
    object Cancel_B: TButton
      Left = 312
      Top = 43
      Width = 148
      Height = 23
      Anchors = []
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 3
      ExplicitLeft = 557
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 576
    Height = 41
    Align = alBottom
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = 
      'Empty user name mean anonymous access or usage of Windows user n' +
      'ame'
    TabOrder = 1
    ExplicitLeft = 448
    ExplicitTop = 480
    ExplicitWidth = 185
  end
end
