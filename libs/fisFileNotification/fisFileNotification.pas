{------------------------------------------------------------------------------
  Unit     : fisFileNotifaction.pas
  Purpose  : File notification component
  Status   :
  Copyright: �2000 First Internet Software House, http://www.fishouse.com
  Contact  : support@fishouse.com
-------------------------------------------------------------------------------

  History:

  Date                By      Comments
  ----                ----    --------
  30 July 2000        ME      Created, converted from C++ Builder

}

unit fisFileNotification;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  extctrls;

type

  TDirThread = class(TThread)
  private
    prDirectory: String;
    prKillEvent: THandle;
    prSubTree: Boolean;
    prNotifyFilter: DWORD;
    prTimer: TTimer;
  protected
    procedure Execute; override;
  public
    constructor Create(aDirectory: String; aSubtree: Boolean;
                        aNotifyFilter: DWORD; aTimer: TTimer);
    destructor Destroy; override;
  end;

  TNotifyOption = (noFilename, noDirname, noAttributes, noSize, noLastWrite, noLastAccess, noCreation);
  TNotifyOptions = set of TNotifyOption;

  TfisFileNotification = class(TComponent)
  private
    { Private declarations }
    prStarted: Boolean;
    FSubtree: Boolean;
    FOptions: TNotifyOptions;
    FDirectory: String;
    FOnDirectoryChanged: TNotifyEvent;
    thrDirThread: TDirThread;
    prTimer: TTimer;
  protected
    { Protected declarations }
    procedure Timer(Sender: TObject);
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Start;
    procedure Stop;
  published
    { Published declarations }
    property OnDirectoryChanged: TNotifyEvent read FOnDirectoryChanged write FOnDirectoryChanged;
    property Subtree: Boolean read FSubtree write FSubtree;
    property Directory: String read FDirectory write FDirectory;
    property NotificationType: TNotifyOptions read FOptions write FOptions default [noLastWrite];
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('FISH', [TfisFileNotification]);
end;

{ TDirThread }

constructor TDirThread.Create(aDirectory: String; aSubtree: Boolean;
  aNotifyFilter: DWORD; aTimer: TTimer);
begin
  inherited Create(false);
  prKillEvent := CreateEvent(nil, false, false, nil);
  prDirectory := aDirectory;
  prSubTree := aSubtree;
  prNotifyFilter := aNotifyFilter;
  prTimer := aTimer;
end;

destructor TDirThread.Destroy;
begin
  SetEvent(prKillEvent);
  CloseHandle(prKillEvent);
  inherited;
end;

procedure TDirThread.Execute;
var
  lObjList: array[0..1] of THandle;
  NotifyRes: THandle;
begin
  lObjList[0] := prKillEvent;
  NotifyRes := FindFirstChangeNotification(Pchar(prDirectory), prSubTree,
        prNotifyFilter);
  lObjList[1] := NotifyRes;
  if (NotifyRes <> INVALID_HANDLE_VALUE) then
  begin
    repeat
      if(WaitForMultipleObjects(2, @lObjList, false, INFINITE) = WAIT_OBJECT_0) then
      begin
          break;
      end;
      prTimer.Enabled := true;
    until not FindNextChangeNotification(lObjList[1]);
    FindCloseChangeNotification(lObjList[1]);
  end;
end;

{ TfisFileNotification }

constructor TfisFileNotification.Create(aOwner: TComponent);
begin
  inherited;
  FSubTree := False;
  prStarted := False;
  FDirectory := '';
  FOptions := FOptions + [noLastWrite];
  prTimer := TTimer.Create(Self);
  prTimer.Enabled := False;
  prTimer.Interval := 10;
  prTimer.OnTimer := Timer;
end;

destructor TfisFileNotification.Destroy;
begin
  if not (csDesigning in ComponentState) then
  begin
    Stop;
  end;
  inherited;
end;

procedure TfisFileNotification.Start;
var
  lNotifyFilter: DWORD;
  lItem: Integer;
begin
  if (not prStarted) then
  begin
    lNotifyFilter := 0;
    lItem := Integer(noFilename);
    // Make file notification mask out of our enumerated type
    while lItem <= integer(noCreation) do
    begin
      if TNotifyOption(lItem) in FOptions then
      begin
        lNotifyFilter := LNotifyFilter + (1 shl lItem);
      end;
      inc(lItem);
    end;
    thrDirThread := TDirThread.Create(FDirectory, FSubTree, lNotifyFilter, prTimer);
    prStarted := True;
  end;
end;

procedure TfisFileNotification.Stop;
begin
  if prstarted then
  begin
    thrDirThread.free;
    prStarted := False;
  end;
end;

procedure TfisFileNotification.Timer(Sender: TObject);
begin
  prTimer.Enabled := false;
  if assigned(FOnDirectoryChanged) then
  begin
    FOnDirectoryChanged(self);
  end;
end;

end.
