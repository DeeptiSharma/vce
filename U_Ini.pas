(* Ini- ����. ���������� �������� *)
unit U_Ini;

interface

uses IniFiles, Classes, Comctrls, Forms;

(* ������ *)
type TINI_Sections =
     ( is_Options_Section,
       is_Curr_user_Section,
       is_View_Section
     );


(* ������ *)
type TINI_Idents   = (
       ii_Login,
       ii_PW,
       ii_Nickname,
       ii_email

 );

type TIni_Strore = object
       private
         function Get_Int( INI_Idents   : TINI_Idents
                         ) : integer;
         procedure Set_Int( INI_Idents   : TINI_Idents;
                            Value : integer );

         function Get_Boo( INI_Idents   : TINI_Idents
                         ) : boolean;
         procedure Set_Boo( INI_Idents   : TINI_Idents;
                            Value : boolean );

         function Get_Str( INI_Idents   : TINI_Idents
                         ) : string;
         procedure Set_Str( INI_Idents   : TINI_Idents;
                            Value : string );
       public
         property Int[ INI_Idents : TINI_Idents ]  : integer read Get_Int write Set_Int;

         property Boo[ INI_Idents : TINI_Idents ]  : boolean read Get_Boo write Set_Boo;

         property Str[ INI_Idents : TINI_Idents ]  : string  read Get_Str write Set_Str;

         (* ������-������ �������� ����� �� ini ����� *)
         (* ������ �������� ����� � ini ���� *)
         procedure Save_Form_Sizes( Arg_Form : TForm );

         (* ������ �������� ����� �� ini ����� *)
         procedure Load_Form_Sizes( Arg_Form : TForm );

         (* �������� �������� �� ��������� *)
         function Get_Def( INI_Idents : TINI_Idents ) : string;

         (* �������� �������� ���� � ini *)
         function Get_Ini_Path : string;

         (* ������� ������ *)
         procedure Del_Section( Arg_INI_Sections : TINI_Sections );
      end;

var Ini_Strore : TIni_Strore;

(* ������� ���� �������� *)
procedure Ini_Init;

(* ������� ���� �������� *)
procedure Ini_Done;

(* �������� �������� ������ �� ���� *)
function Get_Section_Name( Arg_INI_Sections : TINI_Sections ) : string;

(* �������� �������� �������� �� ���� *)
function Get_Idents_Name( Arg_INI_Idents : TINI_Idents ) : string;

implementation

uses Sysutils, Graphics, U_Const, U_ShellOp, U_Msg;

(* ������ *)
const INI_Sections_Name : array[TINI_Sections] of string =
        ( 'Options',
          'Curr_user',
          'View'
        );

(* ������ *)

const INI_Idents_Name : array[ TINI_Idents ] of
        record
          INI_Sections : TINI_Sections;
          Idents_Name  : string;
          Def          : string
        end
      =
      (
        ( INI_Sections : is_Curr_user_Section;
          Idents_Name  : 'Login';
          Def          : '' ),
        ( INI_Sections : is_Curr_user_Section;
          Idents_Name  : 'PW';
          Def          : '' ),

        ( INI_Sections : is_Curr_user_Section;
          Idents_Name  : 'Nickname';
          Def          : '' ),
        ( INI_Sections : is_Curr_user_Section;
          Idents_Name  : 'Email';
          Def          : '' )

      );

var Ini : TIniFile;

(* ������� ���� �������� *)
procedure Ini_Init;
var Cool_INI, Base_INI, s : string;
begin
  Cool_INI := AppData_Path + Get_ini_file_name;
  Base_INI := Appl_Path + Get_ini_file_name;

  s := Base_INI;
  (* ���� ���� ����������� - ��������� ini ���� � App Data �����.
     �� �� ����������. *)
  if ( AppData_Path <> '' )
     and
     ( DirectoryExists( AppData_Path ) )
    then
      begin
        s := Cool_INI;
      end;

  Ini := TIniFile.Create( s );
end;

(* ������� ���� �������� *)
procedure Ini_Done;
begin
  Ini.Free;
end;

procedure TIni_Strore.Del_Section;
begin
  Ini.EraseSection( INI_Sections_Name[ Arg_INI_Sections ] );
end;

function TIni_Strore.Get_Boo;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      Result := Ini.ReadBool( INI_Sections_Name[INI_Sections], Idents_Name, ( Def = '1' ) );
    end;
end;

function TIni_Strore.Get_Ini_Path: string;
begin
  Result := '';
  if Assigned( Ini )
    then
      Result := Ini.FileName;
end;

function TIni_Strore.Get_Int;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      Result := Ini.ReadInteger( INI_Sections_Name[INI_Sections], Idents_Name, StrToInt( Def ) );
    end;
end;

function TIni_Strore.Get_Str;
var s : string;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      s := Ini.ReadString( INI_Sections_Name[INI_Sections], Idents_Name, Def );
      if INI_Idents = ii_PW
        then
          begin
            s := Decrypt_B64( s );
          end;

      Result := s;
    end;
end;

procedure TIni_Strore.Set_Str;
var s : string;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      s := Value;

      if INI_Idents = ii_PW
        then
          begin
            s := Encrypt_B64( s );
          end;

      Ini.WriteString( INI_Sections_Name[INI_Sections], Idents_Name, s );
    end;
end;

procedure TIni_Strore.Load_Form_Sizes;
var Max : boolean;
    Section : string;
    Top : integer;
begin
  Section := Arg_Form.ClassName;
  Max := ( Arg_Form.WindowState = wsMaximized );

  (* ��������� �� ��������� �������� ������� ��������. ����� ����������! *)
  with Ini do
    begin
      Max := ReadBool( Section, 'Maximized', Max );

      (* ���� ������� "���������� �����", �� ������� ����� �� ���������� *)
      if Max
        then
          Arg_Form.WindowState := wsMaximized
        else
          begin
            Top := ReadInteger( Section, 'Top', 0 );
            if Top = 0
              then
                begin
                  Arg_Form.Position := poDesktopCenter;
                  Arg_Form.width  := Arg_Form.Constraints.MinWidth * 2;
                  Arg_Form.height := Arg_Form.Constraints.MinHeight;
                end
              else
                begin
                  Arg_Form.top := ReadInteger( Section, 'Top', Arg_Form.Top);
                  Arg_Form.left := ReadInteger( Section, 'Left', Arg_Form.Left);

                  if ( Arg_Form.BorderStyle = bsSizeable )
                    then
                      begin
                        Arg_Form.width  := ReadInteger( Section, 'Width', Arg_Form.Constraints.MinWidth );
                        Arg_Form.height := ReadInteger( Section, 'Height', Arg_Form.Constraints.MinHeight );
                      end;
                end;
          end;
    end;
end;

procedure TIni_Strore.Save_Form_Sizes;
var Max : boolean;
    Section : string;
begin
  Section := Arg_Form.ClassName;
  Max := ( Arg_Form.WindowState = wsMaximized );

  with Ini do
    begin
      WriteInteger( Section, 'Top', Arg_Form.Top );
      WriteInteger( Section, 'Left', Arg_Form.Left );
      WriteInteger( Section, 'Width', Arg_Form.Width );
      WriteInteger( Section, 'Height', Arg_Form.Height );
      WriteBool( Section, 'Maximized', Max );
    end;
end;

procedure TIni_Strore.Set_Boo;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      Ini.WriteBool( INI_Sections_Name[INI_Sections], Idents_Name, Value );
    end;
end;

procedure TIni_Strore.Set_Int;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      Ini.WriteInteger( INI_Sections_Name[INI_Sections], Idents_Name, Value );
    end;
end;

function TIni_Strore.Get_Def;
begin
  with INI_Idents_Name[INI_Idents] do
    begin
      Result := Def;
    end;
end;

(* �������� �������� ������ �� ���� *)
function Get_Section_Name;
begin
  Result := INI_Sections_Name[ Arg_INI_Sections ];
end;

(* �������� �������� �������� �� ���� *)
function Get_Idents_Name;
begin
  Result := INI_Idents_Name[ Arg_INI_Idents ].Idents_Name;
end;

end.
