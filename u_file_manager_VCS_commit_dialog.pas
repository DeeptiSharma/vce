unit u_file_manager_VCS_commit_dialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, u_file_manager_VCS_const, ActnList,
  ToolWin;

type
  TCommit_Dialog_Form = class(TForm)
    Splitter1: TSplitter;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Commit_Msg_M: TMemo;
    Files_LV: TListView;
    ActionList1: TActionList;
    OK_A: TAction;
    Cancel_A: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Commit_Msg_MChange(Sender: TObject);
    procedure OK_AExecute(Sender: TObject);
    procedure Cancel_AExecute(Sender: TObject);
  private
    { Private declarations }
    procedure Set_Enabled;
  public
    { Public declarations }
  end;

function Request_Commit_Confirm( Arg_File_List : T_Commited_Files_Array; var Out_Commit_Msg : string; out Out_MSG_Changed : boolean ) : boolean;

implementation

uses u_vcs_Ini, u_vcs_ico_const;

{$R *.dfm}

function Request_Commit_Confirm;
var Commit_Dialog_Form : TCommit_Dialog_Form;
    i : integer;
begin
  Result := False;
  Out_MSG_Changed := False;
  Commit_Dialog_Form := TCommit_Dialog_Form.Create( Application );

  with Commit_Dialog_Form do
    begin
      OK_A.ImageIndex := integer( iii_Commit );
      Cancel_A.ImageIndex := integer( iii_Close );
      Set_ToolBar( ToolBar1, True );

      (* ��������� ������ ������ *)
      Files_LV.Items.BeginUpdate;

      for i := 0 to high( Arg_File_List ) do
        with Files_LV.Items.Add do
          begin
            Caption := Arg_File_List[i].f_file_name;
            SubItems.Add( T_File_VCS_State_Titles[ Arg_File_List[i].f_state ] );
          end;
      Files_LV.Items.EndUpdate;

      (* ����� ����������� ��������� *)
      Commit_Msg_M.Lines.Text := Out_Commit_Msg;

      Set_Enabled;

      ShowModal;

      if Commit_Msg_M.Lines.Text <> Out_Commit_Msg
        then
          begin
            Out_Commit_Msg := Commit_Msg_M.Lines.Text;
            Out_MSG_Changed := True;
          end;

      if ModalResult = mrOk
        then
          Result := True;

      Free;
    end;
end;

procedure TCommit_Dialog_Form.Cancel_AExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TCommit_Dialog_Form.Commit_Msg_MChange(Sender: TObject);
begin
  Set_Enabled;
end;

procedure TCommit_Dialog_Form.FormClose;
begin
  Ini_Strore.Save_Form_Sizes( Self );
end;

procedure TCommit_Dialog_Form.FormCreate(Sender: TObject);
begin
  Ini_Strore.Set_Font( Font );
  Ini_Strore.Load_Form_Sizes( Self );
end;

procedure TCommit_Dialog_Form.OK_AExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TCommit_Dialog_Form.Set_Enabled;
var msg_text : boolean;
begin
  msg_text := Commit_Msg_M.Text <> '';
  Ok_A.Enabled := msg_text;

  if msg_text
    then
      Cancel_A.Caption := 'Cancel (your comment will be stored)'
    else
      Cancel_A.Caption := 'Cancel';
end;

end.
