unit u_ms_word_compare_form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses u_ms_word_compare;

{$R *.dfm}


procedure TForm1.Button1Click(Sender: TObject);

const {LF = 'd:\p\delphi\Projects\test\ms\word\����������� ���������������.doc';
      RF = 'd:\p\delphi\Projects\test\ms\word\����������� ���������������_2.doc';}

      {LF = 'd:\p\delphi\Projects\test\ms\word\1_corrupted.docx';
      RF = 'd:\p\delphi\Projects\test\ms\word\2.docx';}

      LF = 'd:\p\delphi\Projects\test\ms\word\1.docx';
      //LF = 'd:\p\delphi\Projects\test\ms\word\1_corrupted.docx';
      RF = 'd:\p\delphi\Projects\test\ms\word\2.docx';
begin
  case MS_Word_Compare( LF, RF ) of
    tmswcr_None                    : ;
    tmswcr_LF_Not_Found            : ShowMessage( 'Left ' + LF + ' file not found' );
    tmswcr_RF_Not_Found            : ShowMessage( 'Right ' + RF + ' file not found' );

    tmswcr_LF_CantOpen             : ShowMessage( 'Left ' + LF + ' can''t be open, maybe corrupted' );
    tmswcr_RF_CantOpen             : ShowMessage( 'Right ' + RF + ' can''t be open, maybe corrupted' );

    tmswcr_OLE_MSWord_Not_Found    : ShowMessage( 'Word not found' );
    tmswcr_OLE_MSWord_Compare_Fail :  ShowMessage( 'Compare fail' );
    tmswcr_OK                      : {ShowMessage( 'Ok' )};
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  close;
end;

end.
