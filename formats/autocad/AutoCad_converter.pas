unit autocad_converter;

interface

{$i test.inc}

uses u_multi_page_const;

type T_AutoCad_Multipage_Wrapper = class( T_Base_Multipage_Wrapper )
       protected
        AutoCad_App : variant;
        f_New_Inst : boolean;
        function do_Close_Doc( const Arg_Doc_IDX : integer ) : boolean; override;
        function do_Init_App : boolean; override;
        function do_Close_App : boolean; override;
       public
        function Open_Invisible_File( Arg_FN : string; out Out_Doc_IDX : integer; out Out_MSG : string ) : t_open_results; override;
        function Export_File( const Arg_Doc_IDX : integer; const Arg_Dest_Path : string;
                              const Arg_Trust_Cash : boolean;
                              out Out_Error_MSG : string;
                              const Arg_Page_N : integer = -1 ) : boolean; override;
      end;

implementation

uses Variants, ActiveX, ComObj, SysUtils;

{ T_AutoCad_Multipage_Wrapper }

function T_AutoCad_Multipage_Wrapper.do_Close_App: boolean;
begin
  Result := False;

  {$message warn '���� ��������� - ��������� �� �� ��� ������������ � ��������'}
  
  (* �����, �� �� ��� ������� ���������������? *)
  try
    if ( not VarIsClear( AutoCad_App ) ) and ( f_New_Inst )
      then
        begin
          AutoCad_App.Quit;
          AutoCad_App := varNull;
          Result := True;
        end
      else
        Result := True;
  except
  end;
end;

function T_AutoCad_Multipage_Wrapper.do_Close_Doc(
  const Arg_Doc_IDX: integer): boolean;
begin
  Result := False;
  if ( not Is_Doc_IDX_Correct( Arg_Doc_IDX ) )
      or
     ( not fDoc_List[ Arg_Doc_IDX ].fOpen )
    then
      Exit;

  try
    fDoc_List[ Arg_Doc_IDX ].fDoc_Var.Close;  {$message warn '���� ��������� - ��������� �� �� ��� ������������ � ��������'}
    Result := True;
  except
  end
end;

function T_AutoCad_Multipage_Wrapper.do_Init_App: boolean;
const AutoCad_class_Name = 'AutoCAD.Application';

var Except_MSG : string;
    fn : string;
    pc, i : integer;

    ext : string;
begin
  Result := False;

  if VarIsClear( AutoCad_App )
    then
      begin
        {$message warn 'Try_CreateOleObject' }

        AutoCad_App := GetActiveOleObject('AutoCAD.Application');
        if not VarIsClear( AutoCad_App )
          then
            begin
              //Kompas_App.Visible := true;
              Result := True;
              f_New_Inst := False;
            end
          else
            begin
              Except_MSG := '';
              if Assigned( f_OLE_Exception_Handler )
                then
                  f_OLE_Exception_Handler( Except_MSG );
            end;
      end;
end;

function T_AutoCad_Multipage_Wrapper.Export_File;

procedure Export_One_Page( Arg_idx : integer );
var page_dest_name : string;
    saved_name : string;
    sset : variant;
    doc : variant;
begin
  page_dest_name := Arg_Dest_Path + '_page_' + IntToStr( Arg_idx ){ + '.png'};

  {$ifdef test_wrong_autocad_export}
  page_dest_name := 'zzz:/\/\/\' + page_dest_name;
  {$endif}

  if Arg_Trust_Cash
    then
      begin
        (* �������, ����� ��� ���������, � ���� ��� �����
           �����, ��� �� ���������� Saved2File ������ ������ ������� � ������, �.�.
           ������ ���� ������� ������-�� ����� �� ����� ����������, ���� �� ������� �� ������� �������
           ��� ������ �������� ������
         *)
        saved_name := fDoc_List[ Arg_Doc_IDX ].fPage_List.fPage_List_A[ Arg_idx ].Saved2File;

        if ( saved_name <> '' ) and ( FileExists( saved_name ) )
          then
            Exit;
      end;

  //fDoc_List[ Arg_Doc_IDX ].fDoc_Var.Pages.Item( Arg_idx + 1 ).Export( page_dest_name );
        doc := (fDoc_List[ Arg_Doc_IDX ].fDoc_Var);

        //��� ������ - ���-�� ���������� ������
        sset := doc.ActiveSelectionSet;

        (* http://help.autodesk.com/view/ACD/2015/ENU/?guid=GUID-893F1711-3591-4DDC-8D27-DF91052F5E5A *)
        doc.Export( page_dest_name, 'eps', sset );

  (* ���� ��������� *)
  fDoc_List[ Arg_Doc_IDX ].fPage_List.fPage_List_A[ Arg_idx ].Saved2File := page_dest_name + '.eps';
end;


var page_idx : integer;

begin
  Result := False;

  if Is_Doc_IDX_Correct( Arg_Doc_IDX )
    then
      begin
        try
          {$ifdef test_raise_kompas_export}
          raise EOleError.Create( 'AutoCad Export Test Fail' );
          {$endif}

          if ( fDoc_List[ Arg_Doc_IDX ].fOpen )
            then
              begin
                (* ���� ���������� �������� �� �������� - ������������ ��� *)
                if Arg_Page_N = -1
                  then
                    for page_idx := 0 to fDoc_List[ Arg_Doc_IDX ].fDoc_Var.Pages.Count - 1 do
                      Export_One_Page( page_idx )
                  else
                    begin
                      if ( Arg_Page_N >= 0 ) and ( Arg_Page_N < Length( fDoc_List[ Arg_Doc_IDX ].fPage_List.fPage_List_A ) )
                        then
                          Export_One_Page( Arg_Page_N )
                        else
                          Exit;
                    end;
                Result := True;
              end;
        except
          (* ���� ��������
             �������� ������ - ���� �������� �� ��������, ��� ������� � ������� �����... *)
          on E: EOleError do
            begin
              Out_Error_MSG := E.Message;
            end;
        end;
      end;
end;

function T_AutoCad_Multipage_Wrapper.Open_Invisible_File(Arg_FN: string;
  out Out_Doc_IDX: integer; out Out_MSG: string): t_open_results;
var i : integer;
    page_count, page_idx : integer;
    doc : variant;

    pc : integer;

    ext : string;

begin
  if FileExists( Arg_FN )
    then
      begin
        try
          (* ������� �������� *)
          {$ifdef test_wrong_kompas_open_doc}
          raise EOleError.Create( 'Kompas Open Test Fail' );
          {$endif}

      doc := IDispatch( AutoCad_App.Documents.Open( Arg_FN, True ) ); (* True - read only *)
      if not VarIsClear( doc )
        then
          begin
            i := Length( fDoc_List );
            SetLength( fDoc_List, i + 1 );
            fDoc_List[ i ].fDoc_Var := doc;
        end;


          fDoc_List[ i ].fOpen := True;



          (* ��������� ������ ������� *)
          page_count := 1;
        
          SetLength( fDoc_List[ i ].fPage_List.fPage_List_A, page_count );

          for page_idx := 0 to page_count -1 do
            begin
              //fVisio_Page := fVisio_Doc.Pages.Item( page_idx + 1 );
              with fDoc_List[ i ].fPage_List.fPage_List_A[ page_idx ] do
                begin
                  Caption := IntToStr( page_idx );
                  ID      := page_idx;
                end;
            end;
            
          Out_Doc_IDX := i;

          Result := tor_Ok;
        except
          on E: EOleError do
            begin
              (* ���� ����� ���� ���������, � ����� � ������������ *)
              Out_MSG := E.Message;
              Result := tor_Ole_Error_Open_Doc;
            end;
        end;
      end
    else
      Result := tor_File_Not_Found;
end;

end.
