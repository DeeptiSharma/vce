unit visio_converter;

interface

{$i test.inc}

uses u_multi_page_const;

type t_convert_results = ( tcr_None, tcr_Ok, tcr_File_Not_Found, tcr_Ole_Not_Found, tcr_Ole_Error_Open_Doc, tcr_Ole_Error_Export, tcr_Some_Ole_Error );

function Convert_Visio2Image( Arg_VSD_File : string;
                              Arg_One_Page_Complete : T_One_Page_Complete_NotifyEvent;
                              Arg_OLE_Exception_Handler : T_OLE_Exception_Handler ) : t_convert_results;

(* ������ ��������, ��� ����� ��������� ���� � 0, � ������ Visio.Doc.Pages.Item - � ������� *)
type T_Visio_Multipage_Wrapper = class( T_Base_Multipage_Wrapper )
       protected
        fVisio_App : Variant;
        function do_Close_Doc( const Arg_Doc_IDX : integer ) : boolean; override;
        function do_Init_App : boolean; override;
        function do_Close_App : boolean; override;
       public
        function Open_Invisible_File( Arg_FN : string; out Out_Doc_IDX : integer; out Out_MSG : string ) : t_open_results; override;
        function Export_File( const Arg_Doc_IDX : integer; const Arg_Dest_Path : string;
                              const Arg_Trust_Cash : boolean;
                              out Out_Error_MSG : string;
                              const Arg_Page_N : integer = -1 ) : boolean; override;
      end;

implementation

uses Variants, comobj, SysUtils,
     u_ms_ole_common;

Const visOpenRO = 2;
Const visOpenMinimized = 16;
Const visOpenHidden = 64;
Const visOpenMacrosDisabled = 128;
Const visOpenNoWorkspace = 256;

(* https://msdn.microsoft.com/en-us/library/office/aa215114%28v=office.11%29.aspx *)
const vb_IDNO = 7;

function Convert_Visio2Image;
var fVisio_Dispatch : IDispatch;
    fVisio_App, fVisio_Doc : Variant;
    page_idx : integer;
    page_dest_name : string;

    flag_vsd_opened,
    flag_image_exported : boolean;

    msg : string;
begin
  Result := tcr_None;
  flag_vsd_opened := False;
  flag_image_exported := False;

  if not FileExists( Arg_VSD_File )
    then
      begin
        Result := tcr_File_Not_Found;
        Exit;
      end;

  //fVisio_App := CreateOleObject(Visio_Com);
  if Try_CreateOleObject( Visio_Com, fVisio_Dispatch, msg )
    then
      (* ������������� �� IDispatch � Variant, �� �������� ��� ���� � �������� ������������� CreateOleObject *)
      fVisio_App := fVisio_Dispatch
    else
      begin
        Result := tcr_Ole_Not_Found;
        Exit;
      end;

  fVisio_App.Visible := False;
  fVisio_App.AlertResponse := vb_IDNO;

  (* https://msdn.microsoft.com/en-us/library/ff767229%28v=office.14%29.aspx *)
  try
    fVisio_Doc := fVisio_App.Documents.openex( Arg_VSD_File, visOpenRO + visOpenMinimized + visOpenHidden + visOpenMacrosDisabled + visOpenNoWorkspace );

    flag_vsd_opened := True;

    (* https://msdn.microsoft.com/en-us/library/ff768567%28v=office.14%29.aspx *)
    for page_idx := 1 to fVisio_Doc.Pages.Count do
      begin
        page_dest_name := Arg_VSD_File + '_page_' + IntToStr( page_idx ) + '.png';
        (* https://msdn.microsoft.com/en-us/library/ff767035%28v=office.14%29.aspx *)
        (* https://msdn.microsoft.com/en-us/library/ff767110%28v=office.14%29.aspx *)
        fVisio_Doc.Pages.Item( page_idx ).Export( page_dest_name );

        if Assigned( Arg_One_Page_Complete )
          then
            Arg_One_Page_Complete( page_idx, fVisio_Doc.Pages.Item( page_idx ).name, page_dest_name );
      end;

    flag_image_exported := True;

    (* https://msdn.microsoft.com/en-us/library/ff767415%28v=office.14%29.aspx *)
    fVisio_Doc.Close;

    Result := tcr_Ok;
  except
    (* ������ ����� ����:
       - vsd ��� ������ � ������������ �����������
       - �� ������� �������������� (��������, ������� ���� �������)

       ����� ������ ��������� Visio, �� ��� ������ �����������
         *)
    on E: EOleException do
      begin
        (* ����������� ��������� *)
        if Assigned( Arg_OLE_Exception_Handler )
          then
            Arg_OLE_Exception_Handler( E.Message );

        (* ������� ����������������, �� ����� ������ ���� ������ *)
        if flag_vsd_opened
          then
            begin
              if flag_image_exported
                then
                  Result := tcr_Some_Ole_Error
                else
                  Result := tcr_Ole_Error_Export
            end
          else
            Result := tcr_Ole_Error_Open_Doc;
      end;
  end;

  fVisio_App.Quit;
end;

{ T_Visio_Wrapper }

function T_Visio_Multipage_Wrapper.Export_File;

procedure Export_One_Page( Arg_idx : integer );
var page_dest_name : string;
    saved_name : string;
begin
  page_dest_name := Arg_Dest_Path + '_page_' + IntToStr( Arg_idx ) + '.png';

  {$ifdef test_wrong_visio_export}
  page_dest_name := 'zzz:/\/\/\' + page_dest_name; 
  {$endif}

  if Arg_Trust_Cash
    then
      begin
        (* �������, ����� ��� ���������, � ���� ��� �����
           �����, ��� �� ���������� Saved2File ������ ������ ������� � ������, �.�.
           ������ ���� ������� ������-�� ����� �� ����� ����������, ���� �� ������� �� ������� �������
           ��� ������ �������� ������
         *)
        saved_name := fDoc_List[ Arg_Doc_IDX ].fPage_List.fPage_List_A[ Arg_idx ].Saved2File;
        
        if ( saved_name <> '' ) and ( FileExists( saved_name ) )
          then
            Exit;
      end;

  (* https://msdn.microsoft.com/en-us/library/ff767035%28v=office.14%29.aspx *)
  (* https://msdn.microsoft.com/en-us/library/ff767110%28v=office.14%29.aspx *)
  (* ���� ��� ��������, ����� Visio �� ����� �������������� ���������� ���� (��������, ��-�� ����������� �������� �������)  *)
  fDoc_List[ Arg_Doc_IDX ].fDoc_Var.Pages.Item( Arg_idx + 1 ).Export( page_dest_name );

  (* ���� ��������� *)
  fDoc_List[ Arg_Doc_IDX ].fPage_List.fPage_List_A[ Arg_idx ].Saved2File := page_dest_name

  {if Assigned( Arg_One_Page_Complete )
    then
      Arg_One_Page_Complete( page_idx, fVisio_Doc.Pages.Item( page_idx ).name, page_dest_name );}
end;

var page_idx : integer;
begin
  Result := False;

  if Is_Doc_IDX_Correct( Arg_Doc_IDX )
    then
      begin
        try
          {$ifdef test_raise_visio_export}
          raise EOleError.Create( 'Visio Export Test Fail' );
          {$endif}

          if ( fDoc_List[ Arg_Doc_IDX ].fOpen )
            then
              begin
                (* ���� ���������� �������� �� �������� - ������������ ��� *)
                if Arg_Page_N = -1
                  then
                    for page_idx := 0 to fDoc_List[ Arg_Doc_IDX ].fDoc_Var.Pages.Count - 1 do
                      Export_One_Page( page_idx )
                  else
                    begin
                      if ( Arg_Page_N >= 0 ) and ( Arg_Page_N < Length( fDoc_List[ Arg_Doc_IDX ].fPage_List.fPage_List_A ) )
                        then
                          Export_One_Page( Arg_Page_N )
                        else
                          Exit;
                    end;
                Result := True;
              end;
        except
          (* ���� ��������
             �������� ������ - ���� �������� �� visio, ��� ������� � ������� �����... *)
          on E: EOleError do
            begin
              Out_Error_MSG := E.Message;
            end;
        end;
      end;
end;

function T_Visio_Multipage_Wrapper.do_Close_App: boolean;
begin
  Result := False;
  
  (* �����, �� �� ��� ������� ���������������? *)
  try
    if not VarIsClear( fVisio_App )
      then
        begin
          fVisio_App.Quit;
          Result := True;
        end;
  except
  end;
end;

function T_Visio_Multipage_Wrapper.do_Close_Doc;
begin
  Result := False;
  if ( not Is_Doc_IDX_Correct( Arg_Doc_IDX ) )
      or
     ( not fDoc_List[ Arg_Doc_IDX ].fOpen )
    then
      Exit;

  try
    fDoc_List[ Arg_Doc_IDX ].fDoc_Var.Close;
    Result := True;
  except
  end
end;

function T_Visio_Multipage_Wrapper.do_Init_App;
var Visio_Dispatch : IDispatch;
    Except_MSG : string;
begin
  Result := False;

  if Try_CreateOleObject( Visio_Com, Visio_Dispatch, Except_MSG )
    then
      begin
        (* ������������� �� IDispatch � Variant, �� �������� ��� ���� � �������� ������������� CreateOleObject *)
        fVisio_App := Visio_Dispatch;
        fVisio_App.Visible := False;
        fVisio_App.AlertResponse := vb_IDNO;
        Result := True;
      end
    else
      begin
        if Assigned( f_OLE_Exception_Handler )
          then
            f_OLE_Exception_Handler( Except_MSG );
      end;
end;

function T_Visio_Multipage_Wrapper.Open_Invisible_File;
var fVisio_Doc, fVisio_Page : Variant;
    i : integer;
    page_count, page_idx : integer;
begin
  if FileExists( Arg_FN )
    then
      begin
        try
          (* ������� �������� *)
          {$ifdef test_wrong_visio_open_doc}
          raise EOleError.Create( 'Visio OpenEx Test Fail' );
          {$endif}

          fVisio_Doc := fVisio_App.Documents.openex( Arg_FN, visOpenRO + visOpenMinimized + visOpenHidden + visOpenMacrosDisabled + visOpenNoWorkspace );

          i := Length( fDoc_List );
          SetLength( fDoc_List, i + 1 );
          fDoc_List[ i ].fDoc_Var := fVisio_Doc;
          fDoc_List[ i ].fOpen := True;

          (* ��������� ������ ������� *)
          page_count := fVisio_Doc.Pages.Count;
          SetLength( fDoc_List[ i ].fPage_List.fPage_List_A, page_count );

          for page_idx := 0 to page_count -1 do
            begin
              fVisio_Page := fVisio_Doc.Pages.Item( page_idx + 1 );
              with fDoc_List[ i ].fPage_List.fPage_List_A[ page_idx ] do
                begin
                  Caption := fVisio_Page.name;
                  ID      := fVisio_Page.ID;
                end;
            end;
            
          Out_Doc_IDX := i;

          Result := tor_Ok;
        except
          on E: EOleError do
            begin
              (* ���� ����� ���� ���������, � ����� � ������������ *)
              Out_MSG := E.Message;
              Result := tor_Ole_Error_Open_Doc;
            end;
        end;
      end
    else
      Result := tor_File_Not_Found;
end;
    
end.
