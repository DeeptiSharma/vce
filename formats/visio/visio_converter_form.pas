unit visio_converter_form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Panel1: TPanel;
    Export_B: TButton;
    Edit1: TEdit;
    Page_List_B: TButton;
    Export_New_Var_B: TButton;
    procedure Export_BClick(Sender: TObject);
    procedure Export_New_Var_BClick(Sender: TObject);
    procedure Page_List_BClick(Sender: TObject);
  private
    { Private declarations }
    procedure fOne_Page_Complete_NotifyEvent( Arg_Page_Num : integer; Arg_Page_Caption, Arg_Page_Res_File : string );
    procedure fOLE_Exception_Handler( Arg_Err_MSG : string );
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses visio_converter, u_multi_page_const;

{$R *.dfm}

procedure TForm1.Export_BClick(Sender: TObject);
begin
  Export_B.Enabled := false;

  case Convert_Visio2Image( Edit1.Text, fOne_Page_Complete_NotifyEvent, fOLE_Exception_Handler ) of
    tcr_None                : ;
    tcr_Ok                  : ShowMessage('ok');
    tcr_File_Not_Found      : ShowMessage( 'File not found!' );
    tcr_Ole_Not_Found       : ShowMessage('MS Visio not found!');
    tcr_Ole_Error_Open_Doc  : ShowMessage('Can''t open the doc!');
    tcr_Ole_Error_Export    : ShowMessage('Can''t export!');
    tcr_Some_Ole_Error      : ShowMessage( 'ole error' );
  end;

  Export_B.Enabled := True;
end;

procedure TForm1.Export_New_Var_BClick(Sender: TObject);
var Visio_Wrapper : T_Visio_Multipage_Wrapper;
    doc_idx : integer;
    Out_MSG : string;
begin
  ( sender as tbutton ).Enabled := False;

  Visio_Wrapper := T_Visio_Multipage_Wrapper.Create;
  Visio_Wrapper.Init_Wrapper( fOne_Page_Complete_NotifyEvent, fOLE_Exception_Handler );
  Visio_Wrapper.Open_Invisible_File( Edit1.Text, doc_idx, Out_MSG );

  Visio_Wrapper.Export_File( doc_idx, Edit1.Text, True, Out_MSG, 3 );

  Visio_Wrapper.Free;

  ( sender as tbutton ).Enabled := True;
end;

procedure TForm1.fOLE_Exception_Handler(Arg_Err_MSG: string);
begin
  //ShowMessage(
  Memo1.Lines.Add(
  Arg_Err_MSG );
end;

procedure TForm1.fOne_Page_Complete_NotifyEvent;
begin
  Memo1.Lines.Add( inttostr( Arg_Page_Num ) + ' ' + Arg_Page_Caption + ' ' +  Arg_Page_Res_File );
end;

procedure TForm1.Page_List_BClick(Sender: TObject);
var Visio_Wrapper : T_Visio_Multipage_Wrapper;
    doc_idx, i : integer;
    Page_List_A : T_Page_List_A;
    out_MSG : string;
begin
  ( sender as tbutton ).Enabled := False;

  Visio_Wrapper := T_Visio_Multipage_Wrapper.Create;
  if Visio_Wrapper.Init_Wrapper( fOne_Page_Complete_NotifyEvent, fOLE_Exception_Handler )
    then
      begin
        case Visio_Wrapper.Open_Invisible_File( Edit1.Text, doc_idx, out_MSG ) of
          tor_Ok :
            begin
              Visio_Wrapper.Export_File( doc_idx, {'y:\'+}Edit1.Text, True,  out_MSG, 0 );
              Visio_Wrapper.Export_File( doc_idx, Edit1.Text, True, out_MSG, 1 );

              if Visio_Wrapper.Get_Pages( 0, Page_List_A )
                then
                  for i := 0 to high( Page_List_A ) do
                    Memo1.Lines.Add( IntToStr( i ) + ' ' + IntToStr( Page_List_A[i].ID ) + ' ' + Page_List_A[i].Caption + '  ' + Page_List_A[i].Saved2File );
            end;
            tor_File_Not_Found : Memo1.Lines.Add( 'file not found' );
            tor_Ole_Error_Open_Doc : Memo1.Lines.Add( 'can''t open file' );
        end;


      end;
  Visio_Wrapper.Free;

  ( sender as tbutton ).Enabled := True;
end;

end.
