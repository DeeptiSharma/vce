object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 348
  ClientWidth = 643
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 73
    Width = 643
    Height = 275
    Align = alClient
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 643
    Height = 73
    Align = alTop
    TabOrder = 1
    object Export_B: TButton
      Left = 16
      Top = 42
      Width = 121
      Height = 25
      Caption = 'Export_Old_Var_B'
      TabOrder = 0
      OnClick = Export_BClick
    end
    object Edit1: TEdit
      Left = 16
      Top = 11
      Width = 577
      Height = 21
      TabOrder = 1
      Text = 'd:\Drawing1.vsd'
    end
    object Page_List_B: TButton
      Left = 304
      Top = 42
      Width = 75
      Height = 25
      Caption = 'Page_List_B'
      TabOrder = 2
      OnClick = Page_List_BClick
    end
    object Export_New_Var_B: TButton
      Left = 160
      Top = 42
      Width = 106
      Height = 25
      Caption = 'Export_New_Var_B'
      TabOrder = 3
      OnClick = Export_New_Var_BClick
    end
  end
end
