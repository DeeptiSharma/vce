unit u_multi_page_const;

interface

(* ��� ��������������� ���������� *)

(* �������� �������� *)
type T_Page_rec = record
       Caption : string;
       //Position : integer; ����������, �� ����� ��������� � �������� � �������
       ID : integer;
       Saved2File : string; (* ���� ��� ���������������� - �� ������ ���� *)
      end;

(* ������ - ������ ������� *)
type T_Page_List_A = array of T_Page_rec;

type T_Page_List = record
       fPage_List_A : T_Page_List_A;
       procedure Set_Nil;
       function Check_Page_IDX_Is_Correct( Arg_Page_IDX : integer ) : boolean;
      end;
      
type T_Paged_Doc_Rec = record
       fDoc_Var : Variant;
       fOpen : boolean;
       fPage_List : T_Page_List;
      end;

type t_open_results = ( tor_Ok, tor_File_Not_Found, tor_Ole_Error_Open_Doc );

(* ���������� ������� ���� �������� ������ *)
type T_One_Page_Complete_NotifyEvent = procedure( Arg_Page_Num : integer; Arg_Page_Caption, Arg_Page_Res_File : string ) of object;

(* ���������� ������� ������ *)
type T_OLE_Exception_Handler = procedure( Arg_Err_MSG : string ) of object;

type T_Base_Multipage_Wrapper = class( TObject )
       protected
        (* ��� ��� ��� ����� ������������� ��������� �������� � ����� ���������� �� ������, �� �������� �� ������� ��������� *)
        fDoc_List : array of T_Paged_Doc_Rec;

        (* ����������� *)
        f_One_Page_Complete : T_One_Page_Complete_NotifyEvent;
        f_OLE_Exception_Handler : T_OLE_Exception_Handler;

        function Is_Doc_IDX_Correct( const Arg_Doc_IDX : integer ) : boolean;

        function do_Init_App : boolean; virtual; abstract;
        function do_Close_App : boolean; virtual; abstract;
        function do_Close_Doc( const Arg_Doc_IDX : integer ) : boolean; virtual; abstract;
       public
        function Init_Wrapper( Arg_One_Page_Complete : T_One_Page_Complete_NotifyEvent;
                               Arg_OLE_Exception_Handler : T_OLE_Exception_Handler ) : boolean;
        function Open_Invisible_File( Arg_FN : string; out Out_Doc_IDX : integer; out Out_MSG : string ) : t_open_results; virtual; abstract;
        function Export_File( const Arg_Doc_IDX : integer; const Arg_Dest_Path : string;
                              const Arg_Trust_Cash : boolean;
                              out Out_Error_MSG : string;
                              const Arg_Page_N : integer = -1 ) : boolean; virtual; abstract;
        function Get_Page_Count( const Arg_Doc_IDX : integer ) : integer;
        function Get_Pages( const Arg_Doc_IDX : integer; out Out_Page_List_A : T_Page_List_A ) : boolean;
        (* �������� �� �������� *)
        function Get_Page_ID( const Arg_Doc_IDX : integer; const Arg_Page_IDX : integer ) : integer;
        destructor Destroy; override;
      end;

implementation

{ T_Page_List }

function T_Page_List.Check_Page_IDX_Is_Correct;
begin
  Result := ( Arg_Page_IDX >= 0 )
            and
            ( Arg_Page_IDX < Length( fPage_List_A ) );
end;

procedure T_Page_List.Set_Nil;
begin
  SetLength( fPage_List_A, 0 );
end;

{ T_Base_Multipage_Wrapper }

destructor T_Base_Multipage_Wrapper.Destroy;
var i : integer;
begin
  for i := 0 to high( fDoc_List ) do
    begin
      do_Close_Doc( i );
      fDoc_List[ i ].fOpen := False;
      fDoc_List[ i ].fPage_List.Set_Nil;
    end;

  SetLength( fDoc_List, 0 );

  do_Close_App;

  inherited;
end;

function T_Base_Multipage_Wrapper.Get_Pages(const Arg_Doc_IDX: integer;
  out Out_Page_List_A: T_Page_List_A): boolean;
begin
  Result := False;

  if Is_Doc_IDX_Correct( Arg_Doc_IDX )
    then
      begin
        if fDoc_List[ Arg_Doc_IDX ].fOpen
          then
            begin
              Out_Page_List_A := fDoc_List[ Arg_Doc_IDX ].fPage_List.fPage_List_A;
              Result := True;
            end;
      end;
end;

function T_Base_Multipage_Wrapper.Get_Page_Count;
begin
  Result := -1;

  if Is_Doc_IDX_Correct( Arg_Doc_IDX )
    then
      begin
        if fDoc_List[ Arg_Doc_IDX ].fOpen
          then
            Result := Length( fDoc_List[ Arg_Doc_IDX ].fPage_List.fPage_List_A );
      end;
end;

function T_Base_Multipage_Wrapper.Get_Page_ID;
begin
  Result := -1;

  if Is_Doc_IDX_Correct( Arg_Doc_IDX )
    then
      begin
        if ( fDoc_List[ Arg_Doc_IDX ].fOpen )
           and
           ( fDoc_List[ Arg_Doc_IDX ].fPage_List.Check_Page_IDX_Is_Correct( Arg_Page_IDX ) )
          then
            begin
              Result := fDoc_List[ Arg_Doc_IDX ].fPage_List.fPage_List_A[ Arg_Page_IDX ].ID;
            end;
      end;
end;

function T_Base_Multipage_Wrapper.Init_Wrapper;
begin
  f_One_Page_Complete := Arg_One_Page_Complete;
  f_OLE_Exception_Handler := Arg_OLE_Exception_Handler;

  Result := do_Init_App;
end;

function T_Base_Multipage_Wrapper.Is_Doc_IDX_Correct;
begin
  Result := ( Arg_Doc_IDX >= 0 ) and ( Arg_Doc_IDX < Length( fDoc_List ) );
end;

end.
