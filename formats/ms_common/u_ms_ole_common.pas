unit u_ms_ole_common;

(* ����� �� ������ � MS OLE *)

interface

{$i test.inc}

const Visio_Com = {$ifdef test_wrong_ole}'failed_name' + {$endif}
                  'Visio.InvisibleApp';
                  // 'Visio.Application';

const Word_App_Classname = {$ifdef test_wrong_ole}'failed_name' + {$endif}
                           'Word.Application';

function Try_CreateOleObject(const ClassName: string; var Arg_Dispatch : IDispatch; out Out_Except_MSG : string ) : boolean;

implementation

uses {$ifdef testing_msg}dialogs,{$endif}
     ActiveX, ComObj, SysUtils;

(* ������� �� CreateOleObject ��� �������� ��������� �������� "������ com ������� ���". ������������ CreateOleObject ������ ������ ����������, ������� ����� ������ *)
function Try_CreateOleObject;
var ClassID: TCLSID;
begin
  Result := False;
  Out_Except_MSG := '';

  {$ifdef testing_msg}showmessage( ClassName );{$endif}

  try
    ClassID := ProgIDToClassID(ClassName);
    OleCheck(CoCreateInstance(ClassID, nil, CLSCTX_INPROC_SERVER or CLSCTX_LOCAL_SERVER, IDispatch, Arg_Dispatch));
    Result := True;
  except
    (* ��. ComObj OleError *)
    on E: EOleSysError do
      begin
        Out_Except_MSG := E.Message;
      end;
  end;
end;

end.
