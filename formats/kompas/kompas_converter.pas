unit kompas_converter;

interface

{$i test.inc}

uses u_multi_page_const, KsTLB;

type T_Kompas_Multipage_Wrapper = class( T_Base_Multipage_Wrapper )
       protected
        Kompas_App : KompasObject;
        function do_Close_Doc( const Arg_Doc_IDX : integer ) : boolean; override;
        function do_Init_App : boolean; override;
        function do_Close_App : boolean; override;
       public
        function Open_Invisible_File( Arg_FN : string; out Out_Doc_IDX : integer; out Out_MSG : string ) : t_open_results; override;
        function Export_File( const Arg_Doc_IDX : integer; const Arg_Dest_Path : string;
                              const Arg_Trust_Cash : boolean;
                              out Out_Error_MSG : string;
                              const Arg_Page_N : integer = -1 ) : boolean; override;
      end;

implementation

uses ActiveX, ComObj, SysUtils;

{ T_Kompas_Multipage_Wrapper }

function T_Kompas_Multipage_Wrapper.do_Close_App: boolean;
begin
  Result := False;
  
  (* �����, �� �� ��� ������� ���������������? *)
  try
    if Kompas_App <> nil
      then
        begin
          Kompas_App.Quit;
          Kompas_App := nil;
          Result := True;
        end;
  except
  end;
end;

function T_Kompas_Multipage_Wrapper.do_Close_Doc(
  const Arg_Doc_IDX: integer): boolean;
begin
  Result := False;
  if ( not Is_Doc_IDX_Correct( Arg_Doc_IDX ) )
      or
     ( not fDoc_List[ Arg_Doc_IDX ].fOpen )
    then
      Exit;

  try
    fDoc_List[ Arg_Doc_IDX ].fDoc_Var.ksCloseDocument;
    Result := True;
  except
  end
end;

function T_Kompas_Multipage_Wrapper.do_Init_App: boolean;
var Except_MSG : string;
    doc:ksDocument2D;
    doc_spec : ksSpcDocument;
    doc_txt : ksDocumentTxt;
    RasterFormatParam : ksRasterFormatParam;

    fn : string;


type T_Kompass_Files = ( tk_2d, tk_2d_frw, tk_SPW, tk_doc );


var pc, i : integer;

    kompas_doc_type : T_Kompass_Files;
    ext : string;
begin
  Result := False;
  if Kompas_App = nil
    then
      begin
         {$IFDEF __LIGHT_VERSION__}
         //Kompas:= KompasObject( CreateOleObject('KompasLT.Application.5') );
         {$ELSE}
        Kompas_App := KompasObject( CreateOleObject('Kompas.Application.5') );
         {$ENDIF}
        if Kompas_App <> nil
          then
            begin
              //Kompas_App.Visible := true;
              Result := True;
            end
          else
            begin
              Except_MSG := '';
              if Assigned( f_OLE_Exception_Handler )
                then
                  f_OLE_Exception_Handler( Except_MSG );
            end;
      end;
end;

function T_Kompas_Multipage_Wrapper.Export_File;

procedure Export_One_Page( Arg_idx : integer );
var page_dest_name : string;
    saved_name : string;
    RasterFormatParam : ksRasterFormatParam;
    ks_Document2D : ksDocument2D;
    ii : IUnknown;
begin
  page_dest_name := Arg_Dest_Path + '_page_' + IntToStr( Arg_idx ) + '.png';

  {$ifdef test_wrong_kompas_export}
  page_dest_name := 'zzz:/\/\/\' + page_dest_name;
  {$endif}

  if Arg_Trust_Cash
    then
      begin
        (* �������, ����� ��� ���������, � ���� ��� �����
           �����, ��� �� ���������� Saved2File ������ ������ ������� � ������, �.�.
           ������ ���� ������� ������-�� ����� �� ����� ����������, ���� �� ������� �� ������� �������
           ��� ������ �������� ������
         *)
        saved_name := fDoc_List[ Arg_Doc_IDX ].fPage_List.fPage_List_A[ Arg_idx ].Saved2File;

        if ( saved_name <> '' ) and ( FileExists( saved_name ) )
          then
            Exit;
      end;

  //fDoc_List[ Arg_Doc_IDX ].fDoc_Var.Pages.Item( Arg_idx + 1 ).Export( page_dest_name );
        ii := IUnknown(fDoc_List[ Arg_Doc_IDX ].fDoc_Var);
        ks_Document2D := ii as ksDocument2D;
        RasterFormatParam := ksRasterFormatParam( ks_Document2D.RasterFormatParam() );
        RasterFormatParam.Format := 3; // png // ������ �� ��������� ������� ����������, ���� ��������� ������
        RasterFormatParam.extResolution := 96;

        RasterFormatParam.colorType := 4;  // 2 ��� 0 ��� ����� ��; 4 - ������������� ��� �������
        RasterFormatParam.ColorBPP := 8;
        RasterFormatParam.MultiPageOutput := False;
        RasterFormatParam.RangeIndex;
        RasterFormatParam.greyScale := False;
        RasterFormatParam.pages := inttostr(Arg_idx + 1);

        //fDoc_List[ Arg_Doc_IDX ].fDoc_Var.Pages.Item( Arg_idx + 1 ).SaveAsToRasterFormat( page_dest_name, RasterFormatParam );
        ks_Document2D.SaveAsToRasterFormat( page_dest_name, RasterFormatParam );


  (* ���� ��������� *)
  fDoc_List[ Arg_Doc_IDX ].fPage_List.fPage_List_A[ Arg_idx ].Saved2File := page_dest_name
end;


var page_idx : integer;

begin
  Result := False;

  if Is_Doc_IDX_Correct( Arg_Doc_IDX )
    then
      begin
        try
          {$ifdef test_raise_kompas_export}
          raise EOleError.Create( 'Kompas Export Test Fail' );
          {$endif}

          if ( fDoc_List[ Arg_Doc_IDX ].fOpen )
            then
              begin
                (* ���� ���������� �������� �� �������� - ������������ ��� *)
                if Arg_Page_N = -1
                  then
                    for page_idx := 0 to fDoc_List[ Arg_Doc_IDX ].fDoc_Var.Pages.Count - 1 do
                      Export_One_Page( page_idx )
                  else
                    begin
                      if ( Arg_Page_N >= 0 ) and ( Arg_Page_N < Length( fDoc_List[ Arg_Doc_IDX ].fPage_List.fPage_List_A ) )
                        then
                          Export_One_Page( Arg_Page_N )
                        else
                          Exit;
                    end;
                Result := True;
              end;
        except
          (* ���� ��������
             �������� ������ - ���� �������� �� ������, ��� ������� � ������� �����... *)
          on E: EOleError do
            begin
              Out_Error_MSG := E.Message;
            end;
        end;
      end;
end;

function T_Kompas_Multipage_Wrapper.Open_Invisible_File(Arg_FN: string;
  out Out_Doc_IDX: integer; out Out_MSG: string): t_open_results;
var //fVisio_Doc, fVisio_Page : Variant;
    i : integer;
    page_count, page_idx : integer;


    doc:ksDocument2D;
    doc_spec : ksSpcDocument;
    doc_txt : ksDocumentTxt;
    RasterFormatParam : ksRasterFormatParam;
type T_Kompass_Files = ( tk_2d, tk_2d_frw, tk_SPW, tk_doc );


var pc : integer;

    kompas_doc_type : T_Kompass_Files;
    ext : string;

begin
  if FileExists( Arg_FN )
    then
      begin
        try
          (* ������� �������� *)
          {$ifdef test_wrong_kompas_open_doc}
          raise EOleError.Create( 'Kompas Open Test Fail' );
          {$endif}

          ext := AnsiLowerCase( ExtractFileExt( Arg_FN ) );
        if ext = '.spw'
          then
            begin
              doc_spec := ksSpcDocument( Kompas_App.SpcDocument );
              kompas_doc_type := tk_SPW;
            end
          else
            begin
              if ext = '.kdw'
                then
                  begin
                    kompas_doc_type := tk_doc;
                    doc_txt := ksDocumentTxt( Kompas_App.DocumentTxt );
                  end
            else
            begin
              doc:= ksDocument2D( Kompas_App.Document2D );
              if ext = '.frw'
                then
                  kompas_doc_type := tk_2d_frw
                else
                  kompas_doc_type := tk_2d;
            end;
            end;


        case
          kompas_doc_type of
            tk_2d, tk_2d_frw  : doc.ksOpenDocument( Arg_FN,true);
            tk_SPW : doc_spec.ksOpenDocument(Arg_FN,1);
            tk_doc : doc_txt.ksOpenDocument(Arg_FN,1);
        end;
      if ( doc <> nil ) or ( doc_spec <> nil ) or ( doc_txt <> nil )
        then
          begin
          i := Length( fDoc_List );
          SetLength( fDoc_List, i + 1 );

          case
          kompas_doc_type of
            tk_2d, tk_2d_frw  : fDoc_List[ i ].fDoc_Var := doc;
            tk_SPW : fDoc_List[ i ].fDoc_Var := doc_spec;
            tk_doc : fDoc_List[ i ].fDoc_Var := doc_txt;
        end;


          fDoc_List[ i ].fOpen := True;


          end;


          (* ��������� ������ ������� *)


        case kompas_doc_type of
            tk_2d, tk_2d_frw : page_count := doc.ksGetDocumentPagesCount();
            tk_SPW : page_count := doc_spec.ksGetSpcDocumentPagesCount();
            tk_doc : page_count := doc_txt.ksGetDocumentPagesCount();
        end;

          SetLength( fDoc_List[ i ].fPage_List.fPage_List_A, page_count );

          for page_idx := 0 to page_count -1 do
            begin
              //fVisio_Page := fVisio_Doc.Pages.Item( page_idx + 1 );
              with fDoc_List[ i ].fPage_List.fPage_List_A[ page_idx ] do
                begin
                  Caption := IntToStr( page_idx );
                  ID      := page_idx;
                end;
            end;
            
          Out_Doc_IDX := i;

          Result := tor_Ok;
        except
          on E: EOleError do
            begin
              (* ���� ����� ���� ���������, � ����� � ������������ *)
              Out_MSG := E.Message;
              Result := tor_Ole_Error_Open_Doc;
            end;
        end;
      end
    else
      Result := tor_File_Not_Found;
end;

end.
