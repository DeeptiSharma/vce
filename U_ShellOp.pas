(* ��������� ������ � Win *)
unit U_ShellOp;

interface

uses U_Msg,
     registry, windows, shellapi, classes, graphics;

const MSWindows_Path_Restricted_Chars = '*?"<>|/'; (* ��� �������, ����������� ������ � ��������� ������. ����������� ��������� ���� /\: ��������� *)
      MSWindows_File_Restricted_Chars = MSWindows_Path_Restricted_Chars + '\:'; (* ��� ��� � ������������� *)

(* ����� ������� ����. ������������ � ��������� Detect_Desktop_Folder. *)
var Desktop_Folder : string;
(* ����� ��� ���������. ������������ � ��������� Detect_Desktop_Folder. *)
var Document_Folder : string;
(* ����� Application Data. ������������ � ��������� Detect_Desktop_Folder. *)
var AppData_Folder : string;

(* ����� � Arg_Str ��������� ������ ������� �� Arg_Char_List *)
function Find_Sub_Char( Arg_Str, Arg_Char_List : string ) : boolean;

function Delete_File_To_TrashBin( Arg_FN : string ) : boolean;

function Copy_File( Arg_Source, Arg_Destination : string ) : boolean;

procedure Show_LastError_MSG;

(* ���������� ����� ������� ���� � ��� ���������. *)
procedure Detect_Desktop_and_MyDoc_Folder;

function Select_Directory( Arg_Cap : cap_Type;
                           const InitialDir: String;
                           ShowStatus: Boolean;
                           out Directory: String;
                           Arg_Root : widestring = '' ): Boolean;

(* �������� ������ � �������� ����� *)
procedure getshellinfo( file_name : string;
                        var arg_name : string;
                        var arg_typ : string;
                        var arg_Large_Icon, arg_Small_Icon : ticon;
                        var arg_attr : integer );

(* ������ ������� ��������� *)
function StartFile( Arg_File_Name : String ) : BOOLean;
function StartExplorer( CmdString : String ) : BOOLean;

(* ������� ��������� � ���������� ������� �� ���� *)
function Show_in_Explorer( Arg_File_Name : String ) : BOOLean;

function Start_Link( Arg_Link : String ) : BOOLean;

function StartProcess( CmdString : String{;
         {var hProcess : DWORD; var hThread : DWORD}) : BOOLean;

function ExecNewProcess(ProgramName{, Args} : String; Wait, Hide: Boolean) : boolean;

(* ������������ ������� � ��������� ������ *)
function Escape_Command_Line( Arg_CL : string ) : string;

function Execute_To_String_Filed( Arg_Program_name, Arg_Extra_Arg, Arg_Work_Folder : String; out Result_Str, Error_Str: string; Arg_Log : boolean ) : boolean;

function Execute_To_String( Program_name, Arg_Work_Folder : String; var STD_Out : string ) : boolean;

function GetDosOutput( CommandLine: string ): string;

function WinExecutableName(const AssociatedFile: string): string;

procedure WinShellOpen(const AssociatedFile: string);
procedure WinShellPrint(const AssociatedFile: string);
procedure WinShellExecute(const Operation, AssociatedFile: string);

(* ���������, �������� �� ���������� ������������������ *)
function Ext_Is_Register( Arg_File_Ext : string ) : boolean;

(* �������� ��������� ����� Windows *)
function Get_System_Temp_Path : string;

procedure Clear_Temp_Folder;

function Get_Win_Path : string;

(* �������� ���� � ����� "��� ���������" *)
function Get_Doc_Path : string;

function Get_File_Date( Arg_FN : string ) : TDateTime;

(* �������� ��� �������� ���������� ����� *)
function Get_User_Name : string;

(* �������� ��� ���������� *)
function Get_Comp_Name : string;


(* file and path checks *)
(* True, ���� ���� ����������. ����� ���������� ���� � ����� *)
function Drive_Exist( Arg_Path : string ) : boolean;

implementation

uses Sysutils, Forms, ShlObj, FileCtrl, ActiveX, U_Const, Dialogs;

const

  FO_MOVE           = $0001;

  FO_COPY           = $0002;

  FO_DELETE         = $0003;

  FO_RENAME         = $0004;

function shelloperate( Oper : integer;
                       Source, Dest : string;
                       Handle : THandle;
                       Arg_Silent : boolean;
                       Arg_Trash_Bin : boolean ) : boolean;

var shfileopstruct : tshfileopstruct;
    fname : string;

    { dest: string;
    ct : integer;}
begin
  // let us create the source-filename
  // all filenames in one string, divided by a #0 and on the end another #0, too
  fname := Source + #0;
  {if b.count > 0 then
    for ct := 0 to pred(b.count) do
      fname := fname+b[ct]+#0;}
  fname := fname+#0;

  // get the destination filename
  //dest := getcurrentdir; // default
  {if Oper = fo_rename then
    Dest := inputbox ('rename file','enter new filename',fname);}

  fillchar(shfileopstruct,sizeof(tshfileopstruct),0);

  with shfileopstruct do begin
    wnd      := handle;       // set this to the calling window's handle
    wfunc    := Oper;         // here set the desired shell-function
    pfrom    := pchar(fname); // these are the source-filenames
    pto      := pchar(dest);  // destination
    //
    if Arg_Silent
      then
        fflags :=    // these are flags for the shelloperation (look to the help)
          fflags + FOF_SILENT + FOF_NOCONFIRMATION;

    if Arg_Trash_Bin
      then
        fFlags := fFlags + FOF_ALLOWUNDO;

    //fanyoperationsaborted   // is true if user cancelled the operation
    //hnamemappings    // filemapping-pointer
    //lpszprogresstitle       // if no dialog-boxes (fflags) then show this string (?)
  end;

  // and now do the operation
  Result := shfileoperation(shfileopstruct) = 0;
end;

function Find_Sub_Char;
var i : integer;
begin
  Result := False;
  
  for i := 1 to length( Arg_Char_List ) - 1 do
    if Pos( Arg_Char_List[i], Arg_Str ) > 0
      then
        begin
          Result := True;
          Exit;
        end;
end;

function Delete_File_To_TrashBin;
begin
  Result :=
    shelloperate( FO_DELETE,
                  Arg_FN,
                  '',
                  Application.Handle,
                  True,
                  True );
end;

function Copy_File;
begin
  Result := shelloperate( fo_copy, Arg_Source, Arg_Destination, Application.Handle, True, False );
end;

procedure Show_LastError_MSG;
begin
//  Show_Err( SysErrorMessage( GetLastError ) );
end;

(* ���������� ����� ������� ���� � ��� ���������. *)
procedure Detect_Desktop_and_MyDoc_Folder;

function GetSpecialFolderLocation( nFolder : Integer ) : string;
var ItemIDList: PItemIDList;
    path : array[0..MAX_PATH-1] of char;
begin
  if Failed(SHGetSpecialFolderLocation(Application.Handle, nFolder, ItemIDList))
    then
    else
      begin
        path := #0;
        SHGetPathFromIDList( ItemIDList, path );
        Result := path;
      end;
end;

begin
  Desktop_Folder  := GetSpecialFolderLocation( CSIDL_DESKTOP );
  Document_Folder := GetSpecialFolderLocation( CSIDL_PERSONAL );
  AppData_Folder  := GetSpecialFolderLocation( CSIDL_APPDATA );
end;

procedure getshellinfo;

function fGet_Shell_Info( var Arg_info : tshfileinfo;
                          Arg_Flag : LongWord ) : boolean;
begin
  result := shgetfileinfo( pchar(file_name),
                    0,
                    Arg_info,
                    sizeof( Arg_info ),
                    shgfi_displayname or
                    shgfi_typename or
                    shgfi_icon or
                    Arg_Flag
                    { or shgfi_attributes} ) <> 0;
end;

var LargeIcon_info, SmallIcon_info : tshfileinfo;
begin
  fillchar( LargeIcon_info, sizeof(tshfileinfo), 0 );
  fillchar( SmallIcon_info, sizeof(tshfileinfo), 0 );

  { TODO -c����������� : ������ ������ �����������. ���� �������������� }
  if fGet_Shell_Info( LargeIcon_info, SHGFI_LARGEICON )
                    and
    fGet_Shell_Info( SmallIcon_info, SHGFI_SMALLICON )
    then
    else
      {ShowMsg('!')};

  with LargeIcon_info do
    begin
      arg_name        := szdisplayname;
      arg_typ         := sztypename;
      arg_Large_Icon.handle := hicon;
      arg_attr        := dwattributes;
    end;

  arg_Small_Icon.Handle := SmallIcon_info.hicon;
end;

function StartFile;

var FileName,
    Directory: PChar;
    Result_Str: array[0..254] of Char;
    Res : integer;

begin
  FileName := PChar( ExtractFileName( Arg_File_Name ) );
  Directory := PChar( ExtractFilePath( Arg_File_Name ) );

  (* ����� ����-����������, ��������� � ������ ����� *)
  Res := FindExecutable( FileName, Directory, Result_Str );
  if Res > 32
    then (* exe ������ *)
      begin
        (* ���� ��������� ���� ����� ������������ (�.�. ����������� � ���� exe) *)
        if AnsiCompareFileName( Result_Str, Arg_File_Name ) = 0
          then 
            Result := StartProcess( Arg_File_Name )
          else
            Result := StartProcess( string( Result_Str ) +  #32 + '"' + Directory + FileName + '"' );
      end
    else (* ������ ��� ������ exe *)
      begin
        ShowMsg( SysErrorMessage(GetLastError) );
        Result := False;
      end;
end;

function StartExplorer;
begin
  Result := StartProcess( 'explorer ' + CmdString );
end;

function Start_Link( Arg_Link : String ) : BOOLean;
begin
  WinShellOpen( Arg_Link );
end;

const
  OFASI_EDIT = $0001;
  OFASI_OPENDESKTOP = $0002;

{$IFDEF UNICODE}
function ILCreateFromPath(pszPath: PChar): PItemIDList stdcall; external shell32
  name 'ILCreateFromPathW';
{$ELSE}
function ILCreateFromPath(pszPath: PChar): PItemIDList stdcall; external shell32
  name 'ILCreateFromPathA';
{$ENDIF}
procedure ILFree(pidl: PItemIDList) stdcall; external shell32;
function SHOpenFolderAndSelectItems(pidlFolder: PItemIDList; cidl: Cardinal;
  apidl: pointer; dwFlags: DWORD): HRESULT; stdcall; external shell32;

function OpenFolderAndSelectFile(const FileName: string): boolean;
var
  IIDL: PItemIDList;
begin
  result := false;
  IIDL := ILCreateFromPath(PChar(FileName));
  if IIDL <> nil then
    try
      result := SHOpenFolderAndSelectItems(IIDL, 0, nil, 0) = S_OK;
    finally
      ILFree(IIDL);
    end;
end;

(* ������� ��������� � ���������� ������� �� ���� *)
function Show_in_Explorer;
//var se_res : hinst;
begin
  (* ������ ������� ������ �� ���������
      http://delphi.about.com/od/adptips2006/qt/openselectfile.htm

     ��� http://stackoverflow.com/questions/4291793/selecting-file-in-windows-explorers-does-not-always-work ���� ����������,
     ��� ������� �� �������� ���� ����� ��� ������ � ����������. � ���� ������ �� Windows 7 �� �������� ���������.

  se_res := ShellExecute( application.handle,
                'OPEN',
                PChar('explorer.exe'),
                PChar('/select, "' + Arg_File_Name+ '"'),
                nil,
                SW_NORMAL);

  Result := se_res = 0;

     �� ���� ��� ����� ������������ �������, � �� �� ��������� ����� �����
     http://stackoverflow.com/questions/15300999/open-windows-explorer-directory-select-a-specific-file-in-delphi
      *)

  Result := OpenFolderAndSelectFile( Arg_File_Name );
end;

function GetDosOutput;
var
  SA: TSecurityAttributes;
  SI: TStartupInfo;
  PI: TProcessInformation;
  StdOutPipeRead, StdOutPipeWrite: THandle;
  WasOK: Boolean;
  Buffer: array[0..255] of AnsiChar;
  BytesRead: Cardinal;
  WorkDir: string;
  Handle: Boolean;
begin
  Result := '';
  with SA do begin
    nLength := SizeOf(SA);
    bInheritHandle := True;
    lpSecurityDescriptor := nil;
  end;
  CreatePipe(StdOutPipeRead, StdOutPipeWrite, @SA, 0);
  try
    with SI do
    begin
      FillChar(SI, SizeOf(SI), 0);
      cb := SizeOf(SI);
      dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
      wShowWindow := SW_HIDE;
      hStdInput := GetStdHandle(STD_INPUT_HANDLE); // don't redirect stdin
      hStdOutput := StdOutPipeWrite;
      hStdError := StdOutPipeWrite;
    end;
    WorkDir := AppData_Folder;
    ShowMsg( CommandLine );
    Handle := CreateProcess(nil, PChar('cmd.exe /C ' + CommandLine),
                            nil, nil, True, 0, nil,
                            PChar(WorkDir), SI, PI);
    CloseHandle(StdOutPipeWrite);
    if Handle then
      try
        repeat
          WasOK := ReadFile(StdOutPipeRead, Buffer, 255, BytesRead, nil);
          if BytesRead > 0 then
          begin
            Buffer[BytesRead] := #0;
            Result := Result + Buffer;
          end;
        until not WasOK or (BytesRead = 0);
        WaitForSingleObject(PI.hProcess, INFINITE);
      finally
        CloseHandle(PI.hThread);
        CloseHandle(PI.hProcess);
      end;
  finally
    CloseHandle(StdOutPipeRead);
  end;
end;

(* ������������ ������� � ��������� ������ *)
function Escape_Command_Line;
var  percent_posiion : integer;
begin
  Result := Arg_CL;
  percent_posiion := Pos( '%', Result );
  if percent_posiion > 0
    then
      repeat
        if Result[ percent_posiion ] = '%'
          then
            begin
              Insert( '%', Result, percent_posiion );
              percent_posiion := percent_posiion + 2;
            end
          else
            inc( percent_posiion );
      until ( percent_posiion >= Length( Result ) );
end;

(* �����-��� *)
function Execute_To_String_Filed;
var s, full_command, res_file, bat_file : string;

    temp_file_name : string;

    (* ����������� ���� *)
    bat_f : text;

    (* ��� ������ ����������� *)
    out_res : tstringlist;
    log_str : string;
    dt_1 : TdateTime;
begin
  Result := False;
  dt_1 := time;

  Result_Str := '';
  Error_Str := '';

  Randomize();

  temp_file_name := Get_Rnd_Temp_File_Name;

  (* ���� �������� ��������� *)
  res_file := Temp_Path + temp_file_name + '.txt';
  (* ������ ������� � ���������������� ������ *)
  full_command := Arg_Program_name + ' > ' + ' "' + res_file + '"';
  (* ������������� � oem-���������
     https://bitbucket.org/yncoder/desktopclient/issues/80 *)
  ansitooem(pansichar( full_command ), pansichar( full_command ) );

  (* ������������ ����������� ���� *)
  bat_file := Temp_Path + temp_file_name + '.bat';
  AssignFile( bat_f, bat_file );
  Rewrite( bat_f );
  Writeln( bat_f, full_command );
  CloseFile( bat_f );

  {$i test.inc}
  {$ifdef testing_msg}
  ShowMessage( bat_file );
  {$endif}

  //ExecNewProcess( bat_file, True, True );

  out_res := TStringList.Create;

  if Execute_To_String( '"' + bat_file + '" ' + Arg_Extra_Arg, Arg_Work_Folder, s )
    then
      try
        out_res.LoadFromFile( res_file );
        Result_Str := out_res.Text;
        Result := True;

        (* ���������� ��������� ����������, ������ ���� �������� �������������� *)
        if Arg_Log
          then
            begin
              log_str := Arg_Program_name + ret + 'Work folder : ' + Arg_Work_Folder + ret + Result_Str + ret + TimeToStr( time - dt_1 );
              Write_Log( log_str, True );
            end;

      except
      end
    else
      begin
        Error_Str := s;
        log_str := 'Error' + ret + Arg_Program_name + ret + 'Work folder : ' + Arg_Work_Folder + ret + Error_Str + ret + TimeToStr( time - dt_1 );
        (* ������ ���������� � ������ � ����� ������ *)
        Write_Log( log_str, True );
      end;

  out_res.Free;

  if not Arg_Log
    then
      begin
        DeleteFile( res_file );
        DeleteFile( bat_file );
      end;
end;

// http://stackoverflow.com/a/9120103/3578861
// http://delphi.about.com/cs/adptips2001/a/bltip0201_2.htm
function Execute_To_String;
const ReadBuffer = 24000;
var Security : TSecurityAttributes;
    ReadPipe, WritePipe : THandle;
    lpFileInformation: TByHandleFileInformation;

    start : TStartUpInfo;
    ProcessInfo : TProcessInformation;
    Buffer : Pchar;
    BytesRead : DWord;
    Apprunning : DWord;

    exitCode : DWORD;

    p_work_folder : pchar;

begin
  {$i test.inc}
  {$ifdef testing_msg}
  ShowMessage( Program_name );
  {$endif}
  
  Result := False;
  
  with Security do
    begin
      nlength := SizeOf(TSecurityAttributes) ;
      binherithandle := true;
      lpsecuritydescriptor := nil;
    end;
  if Createpipe (ReadPipe, WritePipe, @Security, 0)
    then
      begin
        Buffer := AllocMem(ReadBuffer + 1) ;
        FillChar(Start,Sizeof(Start),#0) ;
        start.cb := SizeOf(start) ;
        start.hStdOutput := WritePipe;
        start.hStdInput := ReadPipe;
        (* � ���� �� delphi.about �� ���� ������ ��� ������ ������. �������.
           ������ � ������, ���� ���������� ������� ���������� � �������� � ������ -
           ��� �������� ��������� *)
        start.hStdError := WritePipe;

        start.dwFlags := STARTF_USESTDHANDLES + STARTF_USESHOWWINDOW;
        start.wShowWindow := SW_HIDE;

        if Arg_Work_Folder = ''
          then
            p_work_folder := nil
          else
            p_work_folder := pchar( Arg_Work_Folder );

        if CreateProcess( nil,
                          PChar(Program_name),
                          @Security, @Security, true, NORMAL_PRIORITY_CLASS,
                          nil, p_work_folder, start, ProcessInfo )
          then
            begin
              repeat
                Apprunning := WaitForSingleObject( ProcessInfo.hProcess, 100 );
                Application.ProcessMessages;
              until (Apprunning <> WAIT_TIMEOUT) ;

              exitCode := 0;
              GetExitCodeProcess( ProcessInfo.hProcess, exitCode );

              repeat
                BytesRead := 0;

                (* ��������� ������ ������ - ���� 0, �� �������� ReadFile ���������� *)
                GetFileInformationByHandle( ReadPipe, lpFileInformation );
                if ( lpFileInformation.nFileSizeHigh = 0 ) and ( lpFileInformation.nFileSizeLow = 0 )
                  then
                    STD_Out := ''
                  else
                    begin
                      ReadFile( ReadPipe, Buffer[0], ReadBuffer, BytesRead, nil );
                      Buffer[BytesRead]:= #0;
                      OemToAnsi(Buffer,Buffer) ;
                      STD_Out := String(Buffer);
                    end;
              until (BytesRead < ReadBuffer) ;
            end
          else
            (* � �� ���� ��������� '���������� ��������� �������' *)
            begin
              STD_Out := 'can''t run ' + Program_name;
            end;

        if ExitCode = 0
          then
            Result := True;
            
        {if ExitCode <> 0
          then
            ShowMessage( STD_Out );}

        FreeMem(Buffer) ;
        CloseHandle(ProcessInfo.hProcess) ;
        CloseHandle(ProcessInfo.hThread) ;
        CloseHandle(ReadPipe) ;
        CloseHandle(WritePipe) ;
      end;
end;


// http://stackoverflow.com/a/7102583/3578861
// https://msdn.microsoft.com/en-us/library/ms682512%28v=vs.85%29.aspx
function ExecNewProcess;
var
 StartInfo : TStartupInfo;
 ProcInfo : TProcessInformation;
 CreateOK : Boolean;

begin
  Result := False;

  (* fill with known state *)
  FillChar(StartInfo,SizeOf(TStartupInfo),#0);
  FillChar(ProcInfo,SizeOf(TProcessInformation),#0);
  StartInfo.cb := SizeOf(TStartupInfo);

  if Hide
    then
      StartInfo.wShowWindow := SW_HIDE;

  CreateOK := CreateProcess( nil, PChar(ProgramName), nil, nil, False,
                             CREATE_NEW_PROCESS_GROUP+NORMAL_PRIORITY_CLASS,
                             nil, nil, StartInfo, ProcInfo);

  (* check to see if successful *)
  if CreateOK
    then
      begin
        // may or may not be needed. Usually wait for child processes
        if Wait
          then
            WaitForSingleObject(ProcInfo.hProcess, INFINITE);
        Result := True;
      end
    else
      Show_Err( 'Unable to run ' + ProgramName );

 CloseHandle(ProcInfo.hProcess);
 CloseHandle(ProcInfo.hThread);
end;

function StartProcess( CmdString : String{;
         {var hProcess : DWORD; var hThread : DWORD}) : BOOLean;
var CL_PChar: Array [0..255] of char;
    i, j: integer;
    si: TStartupInfo;
    Pri: TProcessInformation;
    //var hProcess : DWORD; var hThread : DWORD;
begin
  //Write_Log( CmdString );

  //hProcess := MainInstance;
  //hThread  := MainInstance;

  si.lpReserved:=nil;                si.lpDesktop:=nil;
  si.lpTitle:=nil;                   si.dwX:=0;
  si.dwY:=0;                         si.dwXSize:=0;
  si.dwYSize:=0;                     si.dwXCountChars:=0;
  si.dwYCountChars:=0;               si.dwFillAttribute:=0;
  si.dwFlags:=STARTF_USESTDHANDLES;  si.wShowWindow:=SW_SHOW;//SW_SHOWMINNOACTIVE;
  si.cbReserved2:=0;                 si.lpReserved2:=nil;
  si.hStdInput:=0;                   si.hStdOutput:=0;
  si.hStdError:=0;                   si.cb := sizeof(TStartupInfo);

  j:=Length(CmdString);  //  String -> PChar
  for i:=0 to (j-1)  do CL_PChar[i]:=CmdString[i+1];
  CL_PChar[j]:=#0;

  if CreateProcess( nil, CL_PChar, nil, nil, TRUE,
                    CREATE_DEFAULT_ERROR_MODE,
                    //CREATE_NO_WINDOW + CREATE_DEFAULT_ERROR_MODE,
                    nil, nil, si, Pri )
  //if CreateProcess( nil, CL_PChar, nil, nil, TRUE, CREATE_NO_WINDOW + CREATE_DEFAULT_ERROR_MODE, nil, nil, si, Pri )
    then begin
       //hProcess:=Pri.hProcess;
       //hThread:=Pri.hThread;

       {repeat
       until WaitForSingleObject( Pri.hProcess, 1000 ) <> WAIT_TIMEOUT;}
       (* Final *)

       Result:=True;
    end
    else begin
       Result:=False;
    end;
end;

const
  cStrBufSize = 80;

{---------------------------------------------------------------}

function WinExecutableName(const AssociatedFile: string): string;
//HINSTANCE FindExecutable(
//    LPCTSTR lpFile,      // ��������� �� ������ � ������ �����
//    LPCTSTR lpDirectory, // ��������� �� ������ � ����������� �� ���������
//    LPTSTR lpResult      // ��������� �� ����� ��� ������, ������������ ����������� ������
//   );
begin
  SetLength(result, cStrBufSize); //ucshell
  FindExecutable(pchar(AssociatedFile), '', pchar(result));
  SetLength(result, strlen(pchar(result)));
end;

//

procedure WinShellExecute(const Operation, AssociatedFile: string);
var
  a1: string;
  i : integer;
begin
  a1 := Operation;
  if a1 = '' then
    a1 := 'open';
    i :=
  ShellExecute(
    application.handle //hWnd: HWND
    , pchar(a1) //Operation: PChar
    , pchar(AssociatedFile) //FileName: PChar
    , '' //Parameters: PChar
    , '' //Directory: PChar
    , SW_SHOWNORMAL //ShowCmd: Integer
    );

    if i = SE_ERR_NOASSOC
      then
        (* ��� ������ ���������� �������� ���� - � �� ��������.
        �� �� �Ѩ ����� ��������!!!! *)
        StartFile( AssociatedFile );
    //GetLastErrorString(0); //ucdialog
end;

procedure WinShellPrint(const AssociatedFile: string);
begin
  WinShellExecute('print', AssociatedFile);
end;

procedure WinShellOpen(const AssociatedFile: string);
begin
  WinShellExecute('open', AssociatedFile);
end;


(* ���������, �������� �� ���������� ������������������ *)
function Ext_Is_Register( Arg_File_Ext : string ) : boolean;
var File_Root : string;
    s : string;
    Reg: TRegistry;
begin
  Result := False;

  if Arg_File_Ext <> ''
    then
      begin
        Reg := TRegistry.Create;
        try
          Reg.RootKey := HKEY_CLASSES_ROOT;
          Result := Reg.KeyExists( Arg_File_Ext );
          Reg.CloseKey;
        finally
          Reg.Free;
        end;
      end;
  //if Result then ShowMessage( Arg_File_Ext + ' exist' ) else ShowMessage( Arg_File_Ext + ' NOT exist' );
end;

function Get_System_Temp_Path : string;
var s : array[0..255] of char;
    c : cardinal;
begin
  //� ���� � � ���� ������ ������???

  c := 255;
  GetTempPath( c, @s );
  Result := s;
end;

procedure Clear_Temp_Folder;
begin
  (* ������� �� ������, ������� ������ *)
  shelloperate( FO_DELETE, Temp_Path, '', 0, true, false );
end;

function Get_Win_Path : string;
var s : array[0..255] of char;
    c : cardinal;
begin
  c := 255;
  GetWindowsDirectory( @s, c );
  Result := s;
end;

(* �������� ���� � ����� "��� ���������" *)
function Get_Doc_Path : string;
{var s : array[0..255] of char;
    c : cardinal;}
begin
  {c := 255;
  G etWindowsDirectory( @s, c );
  Result := s;}
end;

function Get_File_Date;
var SearchRec : TSearchRec;
    FileAttrs: Integer;
begin
  FileAttrs :=  faAnyFile; //faDirectory;
  Result := 0;

  if FindFirst( Arg_FN, FileAttrs, SearchRec ) = 0
    then
      begin
        Result := FileDateToDateTime( SearchRec.Time );
      end;

  FindClose( SearchRec );
end;


function BrowseCallbackProc(hwnd: HWND; uMsg: UINT; lParam, lpData: LPARAM): Integer; stdcall;

// callback function used in SelectDirectory to set the status text and choose an initial dir

var
  Path: array[0..MAX_PATH] of Char;
  X, Y: Integer;
  R: TRect;

begin
  case uMsg of
    BFFM_INITIALIZED:
      begin
        // Initialization has been done, now set our initial directory which is passed in lpData
        // (and set btw. the status text too).
        // Note: There's no need to cast lpData to a PChar since the following call needs a
        //       LPARAM parameter anyway.
        SendMessage(hwnd, BFFM_SETSELECTION, 1, lpData);
        SendMessage(hwnd, BFFM_SETSTATUSTEXT, 0, lpData);

        // place the dialog screen centered
        GetWindowRect(hwnd, R);
        X := (Screen.Width - (R.Right - R.Left)) div 2;
        Y := (Screen.Height - (R.Bottom - R.Top)) div 2;
        SetWindowPos(hwnd, 0, X, Y, 0, 0, SWP_NOSIZE or SWP_NOZORDER); 
      end;
    BFFM_SELCHANGED:
      begin
        // Set the status window to the currently selected path.
        if SHGetPathFromIDList(Pointer(lParam), Path) then SendMessage(hwnd, BFFM_SETSTATUSTEXT, 0, Integer(@Path));
      end;
  end;
  Result := 0;
end;

//----------------------------------------------------------------------------------------------------------------------

function Select_Directory;

// Another browse-for-folder function with the ability to select an intial directory
// (other SelectDirectory functions are in FileCtrl.pas).

var
  BrowseInfo: TBrowseInfo;
  Buffer: PChar;
  RootItemIDList,
  ItemIDList: PItemIDList;
  ShellMalloc: IMalloc;
  IDesktopFolder: IShellFolder;
  Eaten, Flags: LongWord;
  Windows: Pointer;
  Path: String;

begin
  Result := False;
  Directory := '';
  Path := InitialDir;
  if (Length(Path) > 0) and (Path[Length(Path)] = '\')
    then
      Delete(Path, Length(Path), 1);

  FillChar(BrowseInfo, SizeOf(BrowseInfo), 0);
  if ( ShGetMalloc(ShellMalloc) = S_OK ) and ( ShellMalloc <> nil )
    then
  begin
    Buffer := ShellMalloc.Alloc(MAX_PATH);
    try
      RootItemIDList := nil;

      if Arg_Root <> ''
        then
          begin
            SHGetDesktopFolder(IDesktopFolder);
            IDesktopFolder.ParseDisplayName(Application.Handle, nil, PWideChar(Arg_Root), Eaten, RootItemIDList, Flags);
          end;
            
      with BrowseInfo do
      begin
        hwndOwner := Application.Handle;
        pidlRoot := RootItemIDList;
        pszDisplayName := Buffer;
        lpszTitle := PChar( Cap( Arg_Cap ) );
        ulFlags := BIF_RETURNONLYFSDIRS;
        if ShowStatus then ulFlags := ulFlags or BIF_STATUSTEXT;
        lParam := Integer(PChar(Path));
        lpfn := BrowseCallbackProc;
      end;

      // make the browser dialog modal
      Windows := DisableTaskWindows(Application.Handle);
      try
        ItemIDList := ShBrowseForFolder(BrowseInfo);
      finally
        EnableTaskWindows(Windows);
      end;

      Result :=  ItemIDList <> nil;
      if Result then
      begin
        ShGetPathFromIDList(ItemIDList, Buffer);
        ShellMalloc.Free(ItemIDList);
        Directory := Buffer;
      end;
    finally
      ShellMalloc.Free(Buffer);
    end;
  end;
end;

(* �������� ��� �������� ���������� ����� *)
function Get_User_Name;
var lpBuffer: array[0..255] of char;
    nSize: DWORD;
begin
  ZeroMemory( @lpBuffer, 255 );
  GetUserName( @lpBuffer, nSize );
  Result := Copy( lpBuffer, 1, nSize );
end;

(* �������� ��� ���������� *)
function Get_Comp_Name;
var lpBuffer: array[0..255] of char;
    nSize: DWORD;
begin
  ZeroMemory( @lpBuffer, 255 );
  GetComputerName( @lpBuffer, nSize );
  Result := Copy( lpBuffer, 1, nSize );
end;

{
(* �������� �������� ���������� ��������� *)
function Get_EnvironmentStrings( Arg_VarName : string ) : string;

(* �������� �������� ���������� ��������� *)
function Get_EnvironmentStrings;
var Buffer2: PChar;
    Sz: Integer;
    s1, s : string;
begin
  s1 := '%' + Arg_VarName + '%' + #0;

  // bug in size detection! sometimes we get an additional 2 bytes at the end...
  Sz := ExpandEnvironmentStrings( pchar( s1 ), nil, 0 );
  GetMem( Buffer2, Sz );
  ExpandEnvironmentStrings( pchar( s1 ), Buffer2, Sz);
  s := Buffer2;
  FreeMem( Buffer2 );

  Result := s;
end;
}

(* True, ���� ���� ����������. ����� ���������� ���� � ����� *)
function Drive_Exist;
begin
  result := DirectoryExists( ExtractFileDrive( Arg_Path ) );
end;

end.
