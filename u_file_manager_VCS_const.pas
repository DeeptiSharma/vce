unit u_file_manager_VCS_const;

interface

(* ��� �������������� ������� *)
type t_revision_id_type = integer;

const revision_unknown : t_revision_id_type = 0;

type T_Auth_Params = record
       f_username,
       f_password : string;
      end;

(* �������� � ����������� *)
type T_Repo_Info = record
       UUID : string;
       Rev : t_revision_id_type;
      end;

(* ������ � ������� ��� ������� *)
type T_Commits_Log_Rec = record
       f_revision_id : t_revision_id_type;
       f_author, f_datetime, f_comment : string;
       f_file_list : string;
      end;

type T_Commits_Log_A = array of T_Commits_Log_Rec;

(* ��������� ������ � ����� *)
type T_File_VCS_States = ( file_vcs_state_unknown, (* ��������� ���������� �� �������. � ����� ������ ���� �� ������ *)
                           file_vcs_state_Normal, (* �.�. ������������� � ������������� ������� *)
                           file_vcs_state_Added,
                           file_vcs_state_Deleted,
                           file_vcs_state_Modified,
                           file_vcs_state_Ignored,
                           file_vcs_state_Unversioned,
                           file_vcs_state_Missed,
                           file_vcs_state_Conflicted );

(* ������ ���������, ��� ������� ����� ����� ���� � ����� *)
const Enter_Folder_states : set of T_File_VCS_States = [ file_vcs_state_Added, file_vcs_state_Normal ];

(* ������ ���������, ��� ������� ����� ����� �������� Add *)
const Addable_states : set of T_File_VCS_States = [ file_vcs_state_Unversioned ];

(* ������ ���������, ��� ������� ����� ����� �������� Revert *)
const Revertable_states : set of T_File_VCS_States = [ file_vcs_state_Modified, file_vcs_state_Missed ];

(* ������ ���������, �� ������� ��������� �������� Commit *)
const Commitable_states : set of T_File_VCS_States = [ file_vcs_state_Added, file_vcs_state_Modified ];

(* ������ ���������, �� ������� ��������� �������� Resolve *)
const Resolvable_states : set of T_File_VCS_States = [ file_vcs_state_Conflicted ];

(* ������ ���������, �� ������� ��������� �������� Diff *)
const Diffable_states : set of T_File_VCS_States = [ file_vcs_state_Modified {, file_vcs_state_Missed ���������, �.�. ��� ������ ����������  } ];

(* ������ ���������, �� ������� ��������� �������� Log *)
const Logable_states : set of T_File_VCS_States = [ file_vcs_state_Normal, file_vcs_state_Modified, file_vcs_state_Missed, file_vcs_state_Conflicted ];

(* ���� ����������� �������� *)
(* ���� ����������� �������� ����������� ������� *)
type T_Connect_Results = ( tcr_WrongURL, tcr_Fail, tcr_Ok );
(* ���� ����������� ���������� *)
type T_Update_Results = ( tur_Fail, tur_Ok, tur_Conflict );

type T_Get_FileList_BeforeCommit_results = ( tgfl_bc_None, tgfl_bc_Error, tgfl_bc_Commit_Yes, tgfl_bc_Forbid_Conflict );

(* �������, ��� ��� ������������ ��� ������������. �� ������ � ���, �� ��� ������� � ������� ����������� vcs *)
const T_File_VCS_State_Titles : array[T_File_VCS_States] of string =
                        ( '-',
                        
                          'normal',
                          'added',
                          'deleted',
                          'modified',
                          'ignored',
                          'unversioned',
                          'missed',
                          'conflicted'

                         );

(* ������ � ������� � ����� *)
type T_FileRec = record
       f_file_name : string;
       f_state : T_File_VCS_States; (* ��������� � ������� *)
       f_folder_flag : boolean;
       f_revision : t_revision_id_type;
      end;
     T_Files_Array = array of T_FileRec;

(* ������ � ������� � ����� ��� ������� *)
type T_Commited_FileRec = record
       f_file_name : string;
       f_state : T_File_VCS_States;
      end;
     T_Commited_Files_Array = array of T_Commited_FileRec;

procedure Add2Files_Array( var Arg_Files_Array : T_Files_Array; const Arg_New_FileRec : T_FileRec );

(* ��������� ������ *)
type T_VCS_Project_State = ( ps_None,
                             ps_Browse,
                             ps_Checkout );

(* ������ �� ���������� ������� VCS �����-����������� *)
type T_VCS_Busy_Notify = procedure( Arg_Busy : boolean ) of object;

implementation

procedure Add2Files_Array;
var l,
    position, shift : integer;

begin
  { https://bitbucket.org/yncoder/desktopclient/issues/68' }
  l := Length( Arg_Files_Array );
  SetLength( Arg_Files_Array, l + 1 );
  position := l;

  if Arg_New_FileRec.f_folder_flag
    then
      begin
        while position > 0 do
          begin
            if not Arg_Files_Array[ position - 1 ].f_folder_flag
              then
                dec( position )
              else
                begin
                  if Arg_New_FileRec.f_file_name < Arg_Files_Array[ position - 1 ].f_file_name
                    then
                      dec( position )
                    else
                      break;
                end;
          end;
      end;

  if position <> l
    then
      begin
        for shift := l - 1 downto position do
          Arg_Files_Array[ shift + 1 ] := Arg_Files_Array[ shift ];
      end;
  Arg_Files_Array[ position ] := Arg_New_FileRec;
end;

end.
