unit u_test_multipage_converter_form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses u_multipage_converter;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var Visio_Multipage_Converter : T_Visio_Multipage_Converter;
begin
  Visio_Multipage_Converter := T_Visio_Multipage_Converter.Create;
  Visio_Multipage_Converter.Prepare_Converter( '', '' );
  Visio_Multipage_Converter.Done_Converter;
  Visio_Multipage_Converter.Free;
end;

end.
