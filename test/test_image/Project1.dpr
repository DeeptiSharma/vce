program Project1;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  vcs_diff_frame in '..\..\vcs_client\vcs_diff_frame.pas' {View_diff_frame: TFrame},
  vcs_view_image_frame in '..\..\vcs_client\vcs_view_image_frame.pas' {View_Image_Frame: TFrame},
  U_ShellOp in '..\..\U_ShellOp.pas',
  U_Msg in '..\..\U_Msg.pas',
  U_Const in '..\..\U_Const.pas',
  u_image_utils in '..\..\vcs_client\u_image_utils.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
