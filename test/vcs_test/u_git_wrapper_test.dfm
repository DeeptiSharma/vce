object Project_List_F: TProject_List_F
  Left = 0
  Top = 0
  Caption = 'Project_List_F'
  ClientHeight = 355
  ClientWidth = 611
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Projects_LV: TListView
    Left = 0
    Top = 0
    Width = 611
    Height = 297
    Align = alClient
    Columns = <
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
      end>
    ReadOnly = True
    TabOrder = 0
    ViewStyle = vsReport
    OnDblClick = View_Project_BClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 297
    Width = 611
    Height = 58
    Align = alBottom
    TabOrder = 1
    object Close_B: TButton
      Left = 24
      Top = 19
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 0
      OnClick = Close_BClick
    end
    object View_Project_B: TButton
      Left = 280
      Top = 6
      Width = 121
      Height = 34
      Caption = 'View project'
      TabOrder = 1
      OnClick = View_Project_BClick
    end
  end
end
