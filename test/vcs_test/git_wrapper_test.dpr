program git_wrapper_test;

uses
  Forms,
  u_git_wrapper_test in 'u_git_wrapper_test.pas' {Project_List_F},
  u_file_manager_frame in '..\..\u_file_manager_frame.pas' {File_Manager_Frame: TFrame},
  u_file_manager in '..\..\u_file_manager.pas',
  U_Msg in '..\..\U_Msg.pas',
  U_Const in '..\..\U_Const.pas',
  u_file_manager_VCS in '..\..\u_file_manager_VCS.pas',
  u_file_manager_VCS_Base in '..\..\u_file_manager_VCS_Base.pas',
  U_ShellOp in '..\..\U_ShellOp.pas',
  u_file_manager_VCS_const in '..\..\u_file_manager_VCS_const.pas',
  U_ProjectView in '..\..\U_ProjectView.pas' {Project_Form},
  U_API_Wrapper_Const in '..\..\U_API_Wrapper_Const.pas',
  u_file_manager_VCS_log in '..\..\u_file_manager_VCS_log.pas' {VCS_Log_F},
  u_file_manager_VCS_commit_dialog in '..\..\u_file_manager_VCS_commit_dialog.pas' {Commit_Dialog_Form};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TProject_List_F, Project_List_F);
  Application.Run;
end.
